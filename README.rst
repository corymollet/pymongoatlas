PyMongoAtlas
############

PyMongoAtlas is a Python client library for the `MongoDB Atlas Administration API <https://www.mongodb.com/docs/atlas/api/atlas-admin-api/>`_.


.. note::

    This library is still under heavy development and is neither feature-complete nor battle-tested with every resource offered by the Atlas Administration API. It should NOT be used in mission-critical workloads yet.

Installation
============

TODO Until this is released on PyPI...

Quickstart
==========

You will need an Atlas API key to authenticate with your account. Follow the `MongoDB docs <https://www.mongodb.com/docs/atlas/configure-api-access/>`_ to get started. You should carefully consider which permissions to grant to your API key. Almost all API operations require different levels of privilege. You should only grant the bare minimum permissions needed for various discrete workloads and, if feasible, restrict API keys to specific IP address ranges. A compromised key with Org Owner permissions could wreak serious havoc (think delete databases, their backups, and then the org itself!)

Once you have your credentials (public and private keys), you can instantiate your client:

.. code-block:: python

    from pymongoatlas import AtlasClient

    client = AtlasClient(public_key="my-public-key", private_key="my-private-key")
    print(client.get_system_status())


You could also set your keys as environment variables, ``MONGODBATLAS_PUBLIC_KEY`` and/org ``MONGODBATLAS_PRIVATE_KEY`` instead passing them in when constructing the client.

Usage
=====

The main ``AtlasClient`` has various "sub-clients" that roughly correspond to different `resources offered by the Atlas Administration API <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/>`_. These sub-clients generally have methods that correspond to typical CRUD operations and similar REST API verbs. For example, to list all projects in your organization:

.. code-block:: python

    print(client.projects.list())


Entities
--------

PyMongoAtlas uses `Pydantic <https://docs.pydantic.dev/latest/>`_ under the hood to validate data to and from the API. The line above will return a list of ``pymongoatlas.entities.projects.Project``, which is a `Pydantic Model <https://docs.pydantic.dev/latest/concepts/models/>`_ with fields populated from the JSON API. For example, to print the names and creation dates for all of your projects:

.. code-block:: python

    print([(x.name, x.created) for x in client.projects.list()])


Entity References
^^^^^^^^^^^^^^^^^

Entities are frequently referenced when invoking operations against other entities. Almost all operations require either an ``Organization`` or ``Project`` reference when they are invoked, since those are the two highest-level logical groupings of resources in MongoDB Atlas. When calling sub-client methods that require these kind of references, you can pass in either the full entity (i.e. the results of a different method call) or the string identifier of that resource. For example, to list all clusters in a project:


.. code-block:: python

    myproject = client.projects.get_by_name("my-project")
    print(client.clusters.list(project=myproject))

    # you could also pass in the string id of the project
    print(client.clusters.list(project="32b6e34b3d91647abb20e7b8"))

Example Application
===================

This is a simple command-line utility that uses `click <https://click.palletsprojects.com/en/8.1.x/>`_ and `rich <https://rich.readthedocs.io/en/stable/>`_ to display nicely formatted console output for common workflows in my day job that originally led me to develop this library. You could use this application to display information about current VPC peering connections between your Atlas projects and other cloud providers such as AWS:

.. code-block:: shell

    python peering.py --list-connections

This application is also capable of changing the Atlas CIDR block of your project's network container(s):

.. code-block:: shell

    python peering.py \
        --project my-project \
        --container <container id> \
        --set-cidr 192.168.224.0/21


``peering.py``

.. code-block:: python


    import json
    from datetime import datetime
    from enum import Enum

    import click
    from bson.errors import InvalidId
    from bson.objectid import ObjectId
    from pymongoatlas import AtlasClient
    from pymongoatlas.entities.projects import Project, ProjectIdentifier
    from rich.console import Console
    from rich.table import Table


    console = Console()


    def render_row(*renderables) -> list[str]:
        row = []
        for item in renderables:
            if item is None:
                row.append(":no_entry_sign:")
            elif isinstance(item, bool):
                if item:
                    row.append("[green]:heavy_check_mark:[/green]")
                else:
                    row.append("[bold red]:x:[/bold red]")
            elif isinstance(item, datetime):
                row.append(item.strftime("%Y-%m-%d %H:%M:%T %Z"))
            elif isinstance(item, dict):
                row.append(json.dumps(item))
            elif isinstance(item, Enum):
                row.append(item.value)
            else:
                row.append(str(item))
        return row


    def is_object_id(value: str) -> bool:
        try:
            ObjectId(value.strip())
            return True
        except (InvalidId, TypeError):
            return False


    class PeeringWorkflow:
        def __init__(self):
            self.client = AtlasClient()

        def check_project_identifier(self, identifier: str) -> Project:
            if not is_object_id(identifier):
                return self.client.projects.get_by_name(identifier)
            return self.client.projects.get(identifier)

        def connections_as_table(self, project: ProjectIdentifier) -> Table:
            table = Table("Connection ID", "Status", "VPC ID", "Route Table CIDR Block")
            for connection in self.client.networking.list_peering_connections(project):
                table.add_row(*render_row(connection.connection_id, connection.status, connection.vpc_id, connection.route_table_cidr_block))
            return table

        def containers_as_table(self, project: ProjectIdentifier) -> Table:
            table = Table(
                "Project ID",
                "Container ID",
                "Provider",
                "Region",
                "VPC ID",
                "Atlas CIDR Block",
                "Provisioned?"
            )
            for container in self.client.networking.list_peering_containers(project):
                table.add_row(
                    *render_row(
                        project.id,
                        container.id,
                        container.provider_name,
                        container.region_name,
                        container.vpc_id,
                        container.atlas_cidr_block,
                        container.provisioned,
                    )
                )
            return table

        def ip_access_lists_as_table(self, project: ProjectIdentifier) -> Table:
            table = Table("CIDR Block", "Comment")
            for entry in self.client.networking.list_project_ip_access_lists(project):
                table.add_row(*render_row(entry.cidr_block, entry.comment))
            return table


    @click.command
    @click.option("--list-projects", is_flag=True)
    @click.option("--list-containers", help="List networking containers for the given project")
    @click.option("--list-connections", help="List peering connections for the given project")
    @click.option("--list-ip-access-lists")
    @click.option("--set-cidr", help="Set a new CIDR block for the specified network container (project must have no clusters or active peering connections).")
    @click.option("--container", metavar="Network Container ID")
    @click.option("--project")
    def main(list_containers, list_projects, list_connections, list_ip_access_lists, set_cidr, container, project):
        workflow = PeeringWorkflow()
        if list_projects:
            console.print(workflow.client.projects.list())
        if list_containers:
            project = workflow.check_project_identifier(list_containers)
            console.print(workflow.containers_as_table(project))
        if list_connections:
            project = workflow.check_project_identifier(list_connections)
            console.print(workflow.connections_as_table(project))
        if list_ip_access_lists:
            project = workflow.check_project_identifier(list_ip_access_lists)
            console.print(workflow.ip_access_lists_as_table(project))
        if set_cidr:
            project = workflow.check_project_identifier(project)
            container = workflow.client.networking.get_peering_container(project=project, container_id=container)
            container.atlas_cidr_block = set_cidr
            console.print(workflow.client.networking.update_peering_container(project=project, container=container))


    if __name__ == "__main__":
        main()


