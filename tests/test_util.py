from datetime import UTC, datetime, timedelta

import pytest

from pymongoatlas import util


@pytest.fixture
def period_data_full():
    data = {
        "period": "PT10M",
        "end": datetime.now(tz=UTC),
    }
    data["start"] = data["end"] - timedelta(minutes=10)
    return data


@pytest.fixture
def start_end_data():
    data = {"end": datetime.now(tz=UTC)}
    data["start"] = data["end"] - timedelta(minutes=30)
    return data


@pytest.fixture
def period_str():
    return "PT10M"


@pytest.fixture
def start_end():
    data = {"end": datetime.now(tz=UTC)}
    data["start"] = data["end"] - timedelta(minutes=30)
    return data


def test_get_time_period_params(period_str):
    params = util.get_time_period_params(period=period_str)
    assert params == {"period": period_str}


def test_get_time_period_start_end_params(start_end_data):
    params = util.get_time_period_params(**start_end_data)
    assert params == {"start": start_end_data["start"].isoformat(), "end": start_end_data["end"].isoformat()}


def test_get_time_period_raises_if_period_and_other_values_passed(period_data_full):
    with pytest.raises(util.InvalidTimePeriodParams):
        util.get_time_period_params(**period_data_full)


def test_get_time_period_raises_if_period_passed_with_start_or_end():
    data = {"start": datetime.now(tz=UTC), "period": "PT10M"}
    with pytest.raises(util.InvalidTimePeriodParams):
        util.get_time_period_params(**data)
    del data["start"]
    data["end"] = datetime.now(tz=UTC)
    with pytest.raises(util.InvalidTimePeriodParams):
        util.get_time_period_params(**data)


def test_labels_to_dict():
    labels = [
        {
            "key": "foo",
            "value": "bar",
        },
        {
            "key": "baz",
            "value": "biz",
        },
    ]
    assert util.labels_to_dict(labels) == {"foo": "bar", "baz": "biz"}


def dict_to_labels():
    d = {"foo": "bar", "baz": "biz"}
    assert util.dict_to_labels(d) == [{"key": "foo", "value": "bar"}, {"key": "baz", "value": "biz"}]


def test_remove_port():
    host = "host123.mongodb.net:27017"
    assert util.remove_port(host) == "host123.mongodb.net"


def test_remove_none_values():
    data = {"A": 1, "B": False, "C": None, "D": [], "E": {"F": {}, "G": "foo", "H": 0, "I": {"J": None}}}

    assert util.remove_none_values(data) == {"A": 1, "B": False, "D": [], "E": {"F": {}, "G": "foo", "H": 0, "I": {}}}
