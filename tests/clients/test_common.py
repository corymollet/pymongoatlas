import json
import logging
from pathlib import Path

import pytest
from pytest_httpserver import HTTPServer
from requests.exceptions import HTTPError

from pymongoatlas.clients import AtlasClient

ENDPOINT = "/projects"


@pytest.fixture
def cluster_response():
    with Path("tests/fixtures/clusters/cluster.json").open() as infile:
        return json.load(infile)


@pytest.fixture
def atlas_client():
    client = AtlasClient("public_key", "private_key")
    client.client.base_url = ""
    return client


def test_atlas_request_raises_http_error_on_http_error(httpserver: HTTPServer, atlas_client, caplog):
    httpserver.expect_request(ENDPOINT, method="GET").respond_with_json({"foo": "bar"})
    with pytest.raises(HTTPError):
        assert atlas_client.client.get(httpserver.url_for("/foo"))


def get_pymongoatlas_logger(caplog, atlas_client):
    return next(x for x in caplog.records if x.name == atlas_client.client.logger.name)


def test_atlas_request_logs_error_on_http_error(httpserver: HTTPServer, atlas_client, caplog):
    error_endpoint = "/error"
    httpserver.expect_request(ENDPOINT, method="GET").respond_with_json({"foo": "bar"})
    with pytest.raises(HTTPError):
        assert atlas_client.client.get(httpserver.url_for(error_endpoint))
    assert error_endpoint in caplog.text
    record = get_pymongoatlas_logger(caplog, atlas_client)
    assert record.levelno == logging.ERROR


def test_atlas_request_debug_logs_url(httpserver: HTTPServer, atlas_client, caplog):
    caplog.set_level(logging.DEBUG)
    httpserver.expect_request(ENDPOINT, method="GET").respond_with_json({"foo": "bar"})
    assert atlas_client.client.get(httpserver.url_for(ENDPOINT))
    assert ENDPOINT in caplog.text


def test_atlas_request_only_logs_at_configured_level(httpserver: HTTPServer, atlas_client, caplog):
    caplog.set_level(logging.WARNING)
    httpserver.expect_request(ENDPOINT, method="GET").respond_with_json({"foo": "bar"})
    assert atlas_client.client.get(httpserver.url_for(ENDPOINT))
    assert ENDPOINT not in caplog.text
