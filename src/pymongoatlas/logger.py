import logging
import sys
from enum import Enum, auto

from pythonjsonlogger import jsonlogger


class LogFormatter(Enum):
    JSON = auto()
    SIMPLE = auto()


def setup_logger(name: str | None = None, log_formatter: LogFormatter = LogFormatter.SIMPLE, level=logging.INFO) -> logging.Logger:
    if name is not None:
        logger = logging.getLogger(name)
    else:
        logger = logging.getLogger(__name__)
    logger.setLevel(level)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(level)
    handler = HANDLER_MAP[log_formatter](handler)

    logger.addHandler(handler)

    return logger


def _setup_json_logger(handler):
    formatter = jsonlogger.JsonFormatter()
    handler.setFormatter(formatter)
    return handler


def _setup_simple_logger(handler):
    formatter = logging.Formatter("%(name)s: %(levelname)s %(message)s")
    handler.setFormatter(formatter)
    return handler


HANDLER_MAP = {LogFormatter.JSON: _setup_json_logger, LogFormatter.SIMPLE: _setup_simple_logger}


def get_log_formatter_type(logger: logging.Logger) -> LogFormatter:
    formatter = logger.handlers[0].formatter
    if isinstance(formatter, jsonlogger.JsonFormatter):
        return LogFormatter.JSON
    return LogFormatter.SIMPLE
