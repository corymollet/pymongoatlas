import json
import sys

from pymongoatlas import AtlasClient

if __name__ == "__main__":
    client = AtlasClient()
    sys.stdout.write(json.dumps(client.get_system_status()))
