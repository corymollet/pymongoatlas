from datetime import datetime
from typing import TypeAlias

from pydantic import Field

from pymongoatlas.entities.auth import AtlasOrgRole, GroupRole, get_atlas_role
from pymongoatlas.entities.common import AtlasEntity


class Organization(AtlasEntity):
    id: str = ""
    is_deleted: bool = False
    name: str

    def __str__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id


OrganizationIdentifier: TypeAlias = Organization | str


class OrganizationInvite(AtlasEntity):
    created_at: datetime | None = Field(default=None, exclude=True)
    expires_at: datetime | None = Field(default=None, exclude=True)
    group_role_assignments: list[GroupRole] = Field(default_factory=list)
    id: str = Field(default="", exclude=True)
    inviter_username: str | None = Field(default=None, exclude=True)
    org_id: str = Field(exclude=True)
    org_name: str = Field(exclude=True)
    roles: list[AtlasOrgRole] = Field(default_factory=list)
    team_ids: list[str] = Field(default_factory=list)
    username: str | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            createdAt=datetime.fromisoformat(response.pop("createdAt")),
            expiresAt=datetime.fromisoformat(response.pop("expiresAt")),
            groupRoleAssignments=[GroupRole.from_api(x) for x in response.pop("groupRoleAssignments", [])],
            roles=[get_atlas_role(x) for x in response.pop("roles", [])],
            **response,
        )

    def __str__(self):
        return self.id


OrganizationInviteIdentifier: TypeAlias = OrganizationInvite | str


class OrganizationSettings(AtlasEntity):
    api_access_list_required: bool
    multi_factor_auth_required: bool
    restrict_employee_access: bool
