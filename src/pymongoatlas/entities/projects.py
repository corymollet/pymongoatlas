import json
from datetime import datetime
from enum import Enum
from typing import TypeAlias

from pydantic import Field

from pymongoatlas.entities.common import AtlasEntity, Tag


class RegionUsageRestriction(Enum):
    NONE = "NONE"
    GOV_REGIONS_ONLY = "GOV_REGIONS_ONLY"
    COMMERCIAL_FEDRAMP_REGIONS_ONLY = "COMMERCIAL_FEDRAMP_REGIONS_ONLY"


class Project(AtlasEntity):
    id: str | None = None
    org_id: str
    name: str
    created: datetime | None = None
    cluster_count: int = 0
    region_usage_restrictions: RegionUsageRestriction = RegionUsageRestriction.NONE
    tags: list[Tag] = Field(default_factory=list)

    @classmethod
    def from_api(cls, response: dict):
        region_usage_restrictions = response.pop("regionUsageRestrictions", None)
        if region_usage_restrictions is not None:
            region_usage_restrictions = RegionUsageRestriction(region_usage_restrictions)
        else:
            region_usage_restrictions = RegionUsageRestriction.NONE
        return cls(
            created=datetime.fromisoformat(response.pop("created")),
            regionUsageRestrictions=region_usage_restrictions,
            tags=[Tag.from_api(x) for x in response.pop("tags", [])],
            **response,
        )

    def __str__(self):
        return self.id

    def __repr__(self):
        return f"<Project: {self.name} ({self.id})>"

    def __eq__(self, other):
        return self.id == other.id

    def to_api(self, with_default_alert_settings: bool = True) -> str:
        return json.dumps(
            {
                "name": self.name,
                "orgId": self.org_id,
                "tags": self.tags,
                "withDefaultAlertSettings": with_default_alert_settings,
            }
        )


ProjectIdentifier: TypeAlias = Project | str


class ProjectLimitName(Enum):
    # Number of clusters in the project
    DEPLOYMENT_CLUSTERS = "atlas.project.deployment.clusters"

    # Number of nodes per Private Link region in this project
    DEPLOYMENT_NODES_PER_PRIVATE_LINK_REGION = "atlas.project.deployment.nodesPerPrivateLinkRegion"

    # Number of custom roles in the project
    SECURITY_DATABASE_ACCESS_CUSTOM_ROLES = "atlas.project.security.databaseAccess.customRoles"

    # Number of database users in this project
    SECURITY_DATABASE_ACCESS_USERS = "atlas.project.security.databaseAccess.users"

    # Number of cross-region network access entries in this project
    SECURITY_NETWORK_ACCESS_CROSS_REGION_ENTRIES = "atlas.project.security.networkAccess.crossRegionEntries"

    # Number of network access entries
    SECURITY_NETWORK_ACCESS_ENTRIES = "atlas.project.security.networkAccess.entries"

    # Number of bytes processed during a single Data Federation query
    DATA_FEDERATION_BYTES_PROCESSED_QUERY = "dataFederation.bytesProcessed.query"

    # Number of bytes processed across all Data Federation tenants for the current day
    DATA_FEDERATION_BYTES_PROCESSED_DAILY = "dataFederation.bytesProcessed.daily"

    # Number of private service connections per Region Group
    DEPLOYMENT_PRIVATE_SERVICE_CONNECTIONS_PER_REGION_GROUP = "atlas.project.deployment.privateServiceConnectionsPerRegionGroup"

    # Subnet mask for GCP PSC Networks. Has lower limit of 20
    DEPLOYMENT_PRIVATE_SERVICE_CONNECTIONS_SUBNET_MASK = "atlas.project.deployment.privateServiceConnectionsSubnetMask"


class ProjectLimit(AtlasEntity):
    name: ProjectLimitName
    value: int
    default_limit: int
    maximum_limit: int
    current_usage: int | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(name=ProjectLimitName(response.pop("name")), **response)

    def __str__(self):
        return self.name.value


ProjectLimitIdentifier: TypeAlias = ProjectLimit | str


class ProjectSettings(AtlasEntity):
    is_collect_database_specific_statistics_enabled: bool
    is_data_explorer_enabled: bool
    is_extended_storage_sizes_enabled: bool
    is_performance_advisor_enabled: bool
    is_realtime_performance_panel_enabled: bool
    is_schema_advisor_enabled: bool


class MaintenanceWindow(AtlasEntity):
    auto_defer_once_enabled: bool = False
    day_of_week: int = Field(
        ge=1,
        le=7,
    )
    hour_of_day: int = Field(
        ge=0,
        le=23,
    )
    start_asap: bool = False
