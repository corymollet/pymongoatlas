from typing import TypeAlias

from pydantic import Field

from pymongoatlas.entities.auth import AtlasOrgRole, AtlasProjectRole, get_atlas_role
from pymongoatlas.entities.common import AtlasEntity


class Team(AtlasEntity):
    id: str = Field(default="", exclude=True)
    name: str
    usernames: list[str] = Field(default_factory=list)

    def __str__(self):
        return self.id


TeamTypeAlias: TypeAlias = Team | str


class TeamRole(AtlasEntity):
    team_id: str
    roles: list[AtlasOrgRole | AtlasProjectRole] = Field(default_factory=list)

    @classmethod
    def from_api(cls, response: dict) -> "TeamRole":
        return cls(team_id=response["teamId"], roles=[get_atlas_role(x) for x in response.get("roleNames", [])])

    def __str__(self):
        return self.team_id


TeamRoleTypeAlias: TypeAlias = TeamRole | str
