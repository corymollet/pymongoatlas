import json
from datetime import datetime, timezone
from math import ceil
from typing import Any
from urllib.parse import quote

from pydantic import BaseModel, Field, IPvAnyAddress, field_serializer

from pymongoatlas.entities.common import AtlasEntity


class AccessLog(AtlasEntity):
    """
    Atlas parses the MongoDB database logs to collect a list of authentication requests made against your clusters through the following methods:

    - ``mongosh``
    - Compass
    - Drivers

    Authentication requests made with API Keys through the Atlas Administration API are not logged.

    Atlas retains authentication request logs for 7 days.

    <https://www.mongodb.com/docs/atlas/access-tracking/>_
    """

    auth_result: bool
    auth_source: str
    failure_reason: str | None = None
    hostname: str
    ip_address: str
    log_line: dict[str, Any]
    timestamp: datetime
    username: str

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            logLine=json.loads(response.pop("logLine")),
            timestamp=datetime.strptime(response.pop("timestamp"), "%a %b %d %H:%M:%S %Z %Y").replace(tzinfo=timezone.utc),
            **response,
        )


class AccessLogQueryParams(BaseModel):
    start: datetime | None = None
    end: datetime | None = None
    auth_result: bool | None = Field(default=None, serialization_alias="authResult")
    limit: int | None = Field(default=None, serialization_alias="nLogs")
    ip_address: IPvAnyAddress | None = Field(default=None, serialization_alias="ipAddress")

    @field_serializer("start", "end", when_used="unless-none")
    def serialize_start_end(self, value: datetime) -> int:
        return ceil(value.timestamp())

    @field_serializer("ip_address")
    def serialize_ip_address(self, value: str) -> str:
        return quote(str(value))
