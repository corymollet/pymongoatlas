"""
https://www.mongodb.com/docs/atlas/alerts/
"""

from datetime import datetime
from enum import Enum
from typing import ClassVar, Literal, TypeAlias

from pydantic import AnyUrl, EmailStr, Field

from pymongoatlas.entities.auth import AtlasOrgRole, AtlasProjectRole
from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.entities.events import EventType
from pymongoatlas.entities.monitoring import MeasurementName
from pymongoatlas.util import optional_isoformat


class NotificationTypeNotImplemented(Exception):
    pass


class NotificationType(Enum):
    DATADOG = "DATADOG"
    EMAIL = "EMAIL"
    GROUP = "GROUP"
    MICROSOFT_TEAMS = "MICROSOFT_TEAMS"
    OPS_GENIE = "OPS_GENIE"
    ORG = "ORG"
    PAGER_DUTY = "PAGER_DUTY"
    SLACK = "SLACK"
    SMS = "SMS"
    TEAM = "TEAM"
    USER = "USER"
    WEBHOOK = "WEBHOOK"


class Notification(AtlasEntity):
    delay_min: int | None
    notifier_id: str | None
    type_name: NotificationType
    interval_min: int | None

    _mapping: ClassVar[dict] = {}

    @classmethod
    def from_api(cls, response: dict):
        try:
            type_name = NotificationType(response["typeName"])
        except AttributeError as exc:
            raise NotificationTypeNotImplemented(f"Notification type {response['typeName']} is not currently implemented") from exc

        base = {
            "delayMin": response.get("delayMin"),
            "notifierId": response.get("notifierId"),
            "typeName": type_name,
            "intervalMin": response.get("intervalMin"),
        }

        return cls._mapping[type_name].from_api(response, **base)


class DatadogRegion(Enum):
    US1 = "US1"
    US3 = "US3"
    US5 = "US5"
    EU1 = "EU1"


class DatadogNotification(Notification):
    datadog_api_key: str
    datadog_region: DatadogRegion = DatadogRegion.US1

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "DatadogNotification":
        return cls(datadog_api_key=response["datadogApiKey"], datadog_region=response["datadogRegion"], **kwargs)


class EmailNotification(Notification):
    email_address: EmailStr

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "EmailNotification":
        return cls(email_address=response["emailAddress"], **kwargs)


class GroupNotification(Notification):
    roles: list[AtlasProjectRole] = Field(default_factory=list)
    email_enabled: bool = False
    sms_enabled: bool = False

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "GroupNotification":
        return cls(
            email_enabled=response["emailEnabled"],
            sms_enabled=response["smsEnabled"],
            roles=[AtlasProjectRole(x) for x in response.get("roles", [])],
            **kwargs,
        )


class MicrosoftTeamsNotification(Notification):
    microsoft_teams_webhook_url: AnyUrl

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "MicrosoftTeamsNotification":
        return cls(microsoft_teams_webhook_url=response["microsoftTeamsWebhookUrl"], **kwargs)


class OpsGenieNotification(Notification):
    ops_genie_api_key: str
    ops_genie_region: str = "US"

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "OpsGenieNotification":
        return cls(ops_genie_api_key=response["opsGenieApiKey"], ops_genie_region=response["opsGenieRegion"], **kwargs)


class OrgNotification(Notification):
    roles: list[AtlasOrgRole]
    email_enabled: bool = False
    sms_enabled: bool = False

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "OrgNotification":
        return OrgNotification(
            email_enabled=response["emailEnabled"],
            sms_enabled=response["smsEnabled"],
            roles=[AtlasOrgRole(x) for x in response["roles"]],
            **kwargs,
        )


class PagerDutyNotification(Notification):
    region: str = "US"
    service_key: str

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "PagerDutyNotification":
        return cls(region=response["region"], service_key=response["serviceKey"], **kwargs)


class SlackNotification(Notification):
    api_token: str
    channel_name: str

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "SlackNotification":
        return cls(api_token=response["apiToken"], channel_name=response["channelName"], **kwargs)


class SMSNotification(Notification):
    mobile_number: str

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "SMSNotification":
        return cls(mobile_number=response["mobileNumber"], **kwargs)


class TeamNotification(Notification):
    team_id: str
    team_name: str

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "TeamNotification":
        return cls(team_id=response["teamId"], team_name=response["teamName"], **kwargs)


class UserNotification(Notification):
    username: str

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "UserNotification":
        return cls(username=response["username"], **kwargs)


class WebhookNotification(Notification):
    webhook_secret: str
    webhook_url: AnyUrl

    @classmethod
    def from_api(cls, response: dict, **kwargs) -> "WebhookNotification":
        return cls(webhook_secret=response["webhookSecret"], webhook_url=response["webhookUrl"], **kwargs)


Notification._mapping = {
    NotificationType.DATADOG: DatadogNotification,
    NotificationType.EMAIL: EmailNotification,
    NotificationType.GROUP: GroupNotification,
    NotificationType.MICROSOFT_TEAMS: MicrosoftTeamsNotification,
    NotificationType.OPS_GENIE: OpsGenieNotification,
    NotificationType.ORG: OrgNotification,
    NotificationType.PAGER_DUTY: PagerDutyNotification,
    NotificationType.SLACK: SlackNotification,
    NotificationType.SMS: SMSNotification,
    NotificationType.TEAM: TeamNotification,
    NotificationType.USER: UserNotification,
    NotificationType.WEBHOOK: WebhookNotification,
}


class AlertStatus(Enum):
    CANCELLED = "CANCELLED"
    CLOSED = "CLOSED"
    OPEN = "OPEN"
    TRACKING = "TRACKING"


class MatcherFieldName(Enum):
    CLUSTER_NAME = "CLUSTER_NAME"
    HOSTNAME = "HOSTNAME"
    PORT = "PORT"
    HOSTNAME_AND_PORT = "HOSTNAME_AND_PORT"
    APPLICATION_ID = "APPLICATION_ID"
    REPLICA_SET_NAME = "REPLICA_SET_NAME"
    TYPE_NAME = "TYPE_NAME"
    SHARD_NAME = "SHARD_NAME"


class Operator(Enum):
    EQUALS = "EQUALS"
    LESS_THAN = "LESS_THAN"
    GREATER_THAN = "GREATER_THAN"
    CONTAINS = "CONTAINS"
    STARTS_WITH = "STARTS_WITH"
    ENDS_WITH = "ENDS_WITH"
    NOT_EQUALS = "NOT_EQUALS"
    NOT_CONTAINS = "NOT_CONTAINS"
    REGEX = "REGEX"


class Matcher(AtlasEntity):
    field_name: MatcherFieldName
    operator: Operator
    value: str

    @classmethod
    def from_api(cls, response: dict):
        return cls(fieldName=MatcherFieldName(response.pop("fieldName")), operator=Operator(response.pop("operator")), **response)


class TimeMetricUnit(Enum):
    DAYS = "DAYS"
    HOURS = "HOURS"
    MILLION_MINUTES = "MILLION_MINUTES"
    MILLISECONDS = "MILLISECONDS"
    MINUTES = "MINUTES"
    NANOSECONDS = "NANOSECONDS"
    SECONDS = "SECONDS"


class AppServicesMetricThresholdName(Enum):
    REALM_AUTH_LOGIN_FAIL = "REALM_AUTH_LOGIN_FAIL"
    REALM_ENDPOINTS_COMPUTE_MS = "REALM_ENDPOINTS_COMPUTE_MS"
    REALM_ENDPOINTS_EGRESS_BYTES = "REALM_ENDPOINTS_EGRESS_BYTES"
    REALM_ENDPOINTS_FAILED_REQUESTS = "REALM_ENDPOINTS_FAILED_REQUESTS"
    REALM_ENDPOINTS_RESPONSE_MS = "REALM_ENDPOINTS_RESPONSE_MS"
    REALM_GQL_COMPUTE_MS = "REALM_GQL_COMPUTE_MS"
    REALM_GQL_EGRESS_BYTES = "REALM_GQL_EGRESS_BYTES"
    REALM_GQL_FAILED_REQUESTS = "REALM_GQL_FAILED_REQUESTS"
    REALM_GQL_RESPONSE_MS = "REALM_GQL_RESPONSE_MS"
    REALM_OVERALL_COMPUTE_MS = "REALM_OVERALL_COMPUTE_MS"
    REALM_OVERALL_EGRESS_BYTES = "REALM_OVERALL_EGRESS_BYTES"
    REALM_SDKFNS_FAILED_REQUESTS = "REALM_SDKFNS_FAILED_REQUESTS"
    REALM_SDK_FNS_RESPONSE_MS = "REALM_SDK_FNS_RESPONSE_MS"
    REALM_SDK_FUNCTIONS_COMPUTE_MS = "REALM_SDK_FUNCTIONS_COMPUTE_MS"
    REALM_SDK_FUNCTIONS_EGRESS_BYTES = "REALM_SDK_FUNCTIONS_EGRESS_BYTES"
    REALM_SDK_MQL_COMPUTE_MS = "REALM_SDK_MQL_COMPUTE_MS"
    REALM_SDK_MQL_EGRESS_BYTES = "REALM_SDK_MQL_EGRESS_BYTES"
    REALM_SDK_MQL_RESPONSE_MS = "REALM_SDK_MQL_RESPONSE_MS"
    REALM_SYNC_CLIENT_BOOTSTRAP_MS = "REALM_SYNC_CLIENT_BOOTSTRAP_MS"
    REALM_SYNC_CLIENT_CHANGESETS_INVALID = "REALM_SYNC_CLIENT_CHANGESETS_INVALID"
    REALM_SYNC_CLIENT_UPLOADS_FAILED = "REALM_SYNC_CLIENT_UPLOADS_FAILED"
    REALM_SYNC_CLIENT_UPLOADS_INVALID = "REALM_SYNC_CLIENT_UPLOADS_INVALID"
    REALM_SYNC_CURRENT_OPLOG_LAG_MS_SUM = "REALM_SYNC_CURRENT_OPLOG_LAG_MS_SUM"
    REALM_SYNC_EGRESS_BYTES = "REALM_SYNC_EGRESS_BYTES"
    REALM_SYNC_FAILED_REQUESTS = "REALM_SYNC_FAILED_REQUESTS"
    REALM_SYNC_NUM_UNSYNCABLE_DOCS_PERCENT = "REALM_SYNC_NUM_UNSYNCABLE_DOCS_PERCENT"
    REALM_SYNC_SESSIONS_ENDED = "REALM_SYNC_SESSIONS_ENDED"
    REALM_TRIGGERS_COMPUTE_MS = "REALM_TRIGGERS_COMPUTE_MS"
    REALM_TRIGGERS_CURRENT_OPLOG_LAG_MS_SUM = "REALM_TRIGGERS_CURRENT_OPLOG_LAG_MS_SUM"
    REALM_TRIGGERS_EGRESS_BYTES = "REALM_TRIGGERS_EGRESS_BYTES"
    REALM_TRIGGERS_FAILED_REQUESTS = "REALM_TRIGGERS_FAILED_REQUESTS"
    REALM_TRIGGERS_RESPONSE_MS = "REALM_TRIGGERS_RESPONSE_MS"


class AppServicesMetricThreshold(AtlasEntity):
    metric_name: AppServicesMetricThresholdName
    operator: Operator
    threshold: float
    units: TimeMetricUnit = TimeMetricUnit.HOURS

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            metricName=AppServicesMetricThresholdName(response.pop("metricName")),
            operator=Operator(response.pop("operator")),
            units=TimeMetricUnit(response.pop("units")),
            **response,
        )


class AlertConfiguration(AtlasEntity):
    created: datetime | None = None
    enabled: bool = False
    id: str = ""
    matchers: list[Matcher] = Field(default_factory=list)
    updated: datetime | None = None
    notifications: list[Notification] = Field(
        default_factory=list,
    )
    event_type: EventType

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            created=datetime.fromisoformat(response.pop("created")),
            eventType=EventType(response.pop("eventTypeName")),
            updated=datetime.fromisoformat(response.pop("updated")),
            notifications=[Notification.from_api(x) for x in response.pop("notifications")],
            **response,
        )


class GreaterThanRawThreshold(AtlasEntity):
    operator: Operator = Operator.GREATER_THAN
    threshold: int
    units: Literal["RAW"]

    @classmethod
    def from_api(cls, response: dict):
        return cls(threshold=response["threshold"]["threshold"])


class TimeThreshold(AtlasEntity):
    operator: Operator = Operator.GREATER_THAN
    threshold: int
    units: TimeMetricUnit = TimeMetricUnit.HOURS

    @classmethod
    def from_api(cls, response: dict):
        return cls(threshold=response["threshold"]["threshold"], units=TimeMetricUnit(response["threshold"]["units"]))


class AppServicesMetricAlertConfiguration(AlertConfiguration):
    metric_threshold: AppServicesMetricThreshold

    @classmethod
    def from_api(cls, response: dict):
        alert_config = super().from_api(response)
        alert_config.metric_threshold = AppServicesMetricThreshold.from_api(response["metricThreshold"])
        return alert_config


class BillingThresholdAlertConfiguration(AlertConfiguration):
    threshold: GreaterThanRawThreshold

    @classmethod
    def from_api(cls, response: dict):
        alert_config = super().from_api(response)
        alert_config.threshold = GreaterThanRawThreshold.from_api(response["threshold"])
        return alert_config


class CpsBackupThresholdAlertConfiguration(AlertConfiguration):
    threshold: TimeThreshold

    @classmethod
    def from_api(cls, response: dict):
        alert_config = super().from_api(response)
        alert_config.threshold = TimeThreshold.from_api(response["threshold"])
        return alert_config


class EncryptionKeyAlertConfiguration(AlertConfiguration):
    threshold: TimeThreshold

    @classmethod
    def from_api(cls, response: dict):
        alert_config = super().from_api(response)
        alert_config.threshold = TimeThreshold.from_api(response["threshold"])
        return alert_config


class HostMetricThreshold(AtlasEntity):
    metric_name: MeasurementName
    operator: Operator
    threshold: float
    units: str = "RAW"

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            metricName=MeasurementName(response.pop("metricName")),
            operator=Operator(response.pop("operator")),
            threshold=response.pop("threshold"),
            **response,
        )


class HostMetricAlertConfiguration(AlertConfiguration):
    metric_threshold: HostMetricThreshold

    @classmethod
    def from_api(cls, response: dict):
        alert_config = super().from_api(response)
        alert_config.metric_threshold = HostMetricThreshold.from_api(response["metricThreshold"])
        return alert_config


class NDSX509UserAuthAlertConfiguration(AlertConfiguration):
    threshold: TimeThreshold

    @classmethod
    def from_api(cls, response: dict):
        alert_config = super().from_api(response)
        alert_config.threshold = TimeThreshold.from_api(response["threshold"])
        return alert_config


class Alert(AtlasEntity):
    acknowledged_until: datetime | None = None
    acknowledgement_comment: str | None = None
    acknowledging_username: str | None = None
    alert_config_id: str
    cluster_name: str | None = None
    created: datetime
    current_value: dict[str, str | float] = Field(default_factory=dict)
    event_type: EventType
    hostname_and_port: str | None = None
    id: str
    last_notified: datetime | None = None
    metric_name: MeasurementName | None = None
    replica_set_name: str | None = None
    resolved: datetime | None = None
    status: AlertStatus
    updated: datetime

    @classmethod
    def from_api(cls, response: dict):
        metric_name = response.pop("metricName", None)
        if metric_name is not None:
            metric_name = MeasurementName(metric_name)
        return cls(
            acknowledgedUntil=optional_isoformat(response.pop("acknowledgedUntil", None)),
            created=datetime.fromisoformat(response.pop("created")),
            eventType=EventType(response.pop("eventTypeName")),
            lastNotified=optional_isoformat(response.pop("lastNotified", None)),
            metricName=metric_name,
            resolved=optional_isoformat(response.pop("resolved", None)),
            status=AlertStatus(response.pop("status")),
            updated=datetime.fromisoformat(response.pop("updated")),
            **response,
        )

    def __str__(self):
        return self.id


AlertIdentifier: TypeAlias = Alert | str
