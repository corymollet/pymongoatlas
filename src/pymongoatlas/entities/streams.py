import json
from enum import Enum
from typing import Annotated, Optional, TypeAlias, Union

from pydantic import BaseModel, Field

from pymongoatlas.entities.auth import StandardRoleName
from pymongoatlas.entities.cloud_providers import DataProcessingRegion
from pymongoatlas.entities.common import AtlasEntity


class StreamConnectionType(Enum):
    CLUSTER = "Cluster"
    KAFKA = "Kafka"


class DBRoleType(Enum):
    BUILT_IN = "BUILT_IN"
    CUSTOM = "CUSTOM"


class DBRoleToExecute(AtlasEntity):
    role: StandardRoleName | str
    type: DBRoleType = DBRoleType.BUILT_IN

    @classmethod
    def from_api(cls, response: dict) -> "DBRoleToExecute":
        role_type = DBRoleType(response["type"])
        role = response["role"]
        if role_type == DBRoleType.BUILT_IN:
            role = StandardRoleName(response["role"])
        return cls(role=role, type=role_type)

    def to_api(self) -> str:
        payload = {"type": self.type.value}
        if isinstance(self.role, StandardRoleName):
            payload["role"] = self.role.value
        else:
            payload["role"] = self.role
        return json.dumps(payload)


class ClusterStreamConnection(AtlasEntity):
    name: str
    type: StreamConnectionType = StreamConnectionType.CLUSTER
    cluster_name: str
    db_role_to_execute: DBRoleToExecute

    @classmethod
    def from_api(cls, response: dict) -> "ClusterStreamConnection":
        return cls(dbRoleToExecute=DBRoleToExecute.from_api(response.pop("dbRoleToExecute")), **response)

    def __str__(self):
        return self.name


class KafkaAuthenticationMechanism(Enum):
    PLAIN = "PLAIN"
    SCRAM_256 = "SCRAM-256"
    SCRAM_512 = "SCRAM-512"


class KafkaAuthentication(AtlasEntity):
    mechanism: KafkaAuthenticationMechanism
    username: str

    @classmethod
    def from_api(cls, response: dict) -> "KafkaAuthentication":
        return cls(mechanism=KafkaAuthenticationMechanism(response.pop("mechanism")), **response)


class KafkaTransportProtocol(Enum):
    PLAINTEXT = "PLAINTEXT"
    SSL = "SSL"


class KafkaSecurity(AtlasEntity):
    broker_public_certificate: str | None = None
    protocol: KafkaTransportProtocol = KafkaTransportProtocol.PLAINTEXT

    @classmethod
    def from_api(cls, response: dict) -> "KafkaSecurity":
        return cls(protocol=KafkaTransportProtocol(response.pop("protocol")), **response)


class KafkaStreamConnection(AtlasEntity):
    name: str
    type: StreamConnectionType = StreamConnectionType.KAFKA
    authentication: KafkaAuthentication
    bootstrap_servers: str
    config: dict[str, str] = Field(
        default_factory=dict,
        description="A map of Kafka key-value pairs for optional configuration. This is a flat object, and keys can have '.' characters.",
    )
    security: KafkaSecurity

    @classmethod
    def from_api(cls, response: dict) -> "KafkaStreamConnection":
        return cls(
            authentication=KafkaAuthentication.from_api(response.pop("authentication")),
            security=KafkaSecurity.from_api(response.pop("security")),
            **response,
        )

    def __str__(self):
        return self.name


class StreamInstanceTier(Enum):
    SP10 = "SP10"
    SP30 = "SP30"


class StreamConfig(AtlasEntity):
    tier: StreamInstanceTier

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[StreamConfig]":
        if not response:
            return
        return cls(tier=StreamInstanceTier(response["tier"]))


StreamConnection = Annotated[BaseModel, Union[ClusterStreamConnection | KafkaStreamConnection]]


def process_connections(connections: list[dict]) -> list[StreamConnection]:
    connection_entities = []
    for connection in connections:
        connection_type = StreamConnectionType(connection["type"])
        if connection_type == StreamConnectionType.CLUSTER:
            connection_entities.append(ClusterStreamConnection.from_api(connection))
        elif connection_type == StreamConnectionType.KAFKA:
            connection_entities.append(KafkaStreamConnection.from_api(connection))
    return connection_entities


class StreamInstance(AtlasEntity):
    id: str = Field(default="", exclude=True)
    connections: list[StreamConnection] = Field(default_factory=list, exclude=True)
    data_processing_region: DataProcessingRegion
    group_id: str = Field(default="", exclude=True)
    hostnames: list[str] = Field(default_factory=list, exclude=True)
    name: str
    stream_config: StreamConfig | None = None

    @classmethod
    def from_api(cls, response: dict) -> "StreamInstance":
        return cls(
            id=response.pop("_id"),
            data_processing_region=DataProcessingRegion.from_api(response.pop("dataProcessingRegion")),
            connections=process_connections(response.pop("connections", [])),
            stream_config=StreamConfig.from_api(response.pop("streamConfig")),
            **response,
        )

    def __str__(self):
        return self.name


StreamInstanceIdentifier: TypeAlias = StreamInstance | str
