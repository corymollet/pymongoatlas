from enum import Enum
from typing import TypeAlias

from pydantic import Field

from pymongoatlas.entities.common import AtlasEntity


class SearchIndexStatus(Enum):
    IN_PROGRESS = "IN_PROGRESS"
    STEADY = "STEADY"
    FAILED = "FAILED"
    MIGRATING = "MIGRATING"
    STALE = "STALE"
    PAUSED = "PAUSED"


class SearchIndexType(Enum):
    SEARCH = "search"
    VECTOR_SEARCH = "vectorSearch"


class SearchIndexAnalyzer(Enum):
    LUCENE_STANDARD = "lucene.standard"
    LUCENE_SIMPLE = "lucene.simple"
    LUCENE_WHITESPACE = "lucene.whitespace"
    LUCENE_KEYWORD = "lucene.keyword"
    LUCENE_ARABIC = "lucene.arabic"
    LUCENE_ARMENIAN = "lucene.armenian"
    LUCENE_BASQUE = "lucene.basque"
    LUCENE_BENGALI = "lucene.bengali"
    LUCENE_BRAZILIAN = "lucene.brazilian"
    LUCENE_BULGARIAN = "lucene.bulgarian"
    LUCENE_CATALAN = "lucene.catalan"
    LUCENE_CHINESE = "lucene.chinese"
    LUCENE_CJK = "lucene.cjk"
    LUCENE_CZECH = "lucene.czech"
    LUCENE_DANISH = "lucene.danish"
    LUCENE_DUTCH = "lucene.dutch"
    LUCENE_ENGLISH = "lucene.english"
    LUCENE_FINNISH = "lucene.finnish"
    LUCENE_FRENCH = "lucene.french"
    LUCENE_GALICIAN = "lucene.galician"
    LUCENE_GERMAN = "lucene.german"
    LUCENE_GREEK = "lucene.greek"
    LUCENE_HINDI = "lucene.hindi"
    LUCENE_HUNGARIAN = "lucene.hungarian"
    LUCENE_INDONESIAN = "lucene.indonesian"
    LUCENE_IRISH = "lucene.irish"
    LUCENE_ITALIAN = "lucene.italian"
    LUCENE_JAPANESE = "lucene.japanese"
    LUCENE_KOREAN = "lucene.korean"
    LUCENE_KUROMOJI = "lucene.kuromoji"
    LUCENE_LATVIAN = "lucene.latvian"
    LUCENE_LITHUANIAN = "lucene.lithuanian"
    LUCENE_MORFOLOGIK = "lucene.morfologik"
    LUCENE_NORI = "lucene.nori"
    LUCENE_NORWEGIAN = "lucene.norwegian"
    LUCENE_PERSIAN = "lucene.persian"
    LUCENE_PORTUGUESE = "lucene.portuguese"
    LUCENE_ROMANIAN = "lucene.romanian"
    LUCENE_RUSSIAN = "lucene.russian"
    LUCENE_SMARTCN = "lucene.smartcn"
    LUCENE_SORANI = "lucene.sorani"
    LUCENE_SPANISH = "lucene.spanish"
    LUCENE_SWEDISH = "lucene.swedish"
    LUCENE_THAI = "lucene.thai"
    LUCENE_TURKISH = "lucene.turkish"
    LUCENE_UKRAINIAN = "lucene.ukrainian"


class CharFilterType(Enum):
    HTML_STRIP = "htmlStrip"
    ICU_NORMALIZE = "icuNormalize"
    MAPPING = "mapping"
    PERSIAN = "persian"


class CharFilter(AtlasEntity):
    type: CharFilterType = Field(description="Human-readable label that identifies this character filter type.")
    ignored_tags: list[str] = Field(
        default_factory=list, description="The HTML tags that you want to exclude from filtering. Only applicable if type = 'HTML_STRIP'."
    )
    mappings: dict[str, str] = Field(
        default_factory=dict,
        description="Comma-separated list of mappings. A mapping indicates that one character or group of characters should be substituted for another, using the following format: <original> : <replacement>. Only applicable if type = 'MAPPING'",
    )

    @classmethod
    def from_api(cls, response: dict) -> "CharFilter":
        return cls(type=CharFilterType(response.pop("type")), **response)


class TokenizerType(Enum):
    EDGE_GRAM = "edgeGram"
    KEYWORD = "keyword"
    N_GRAM = "nGram"
    REGEX_CAPTURE_GROUP = "regexCaptureGroup"
    REGEX_SPLIT = "regexSplit"
    STANDARD = "standard"
    UAX_URL_EMAIL = "uaxUrlEmail"
    WHITESPACE = "whitespace"


class Tokenizer(AtlasEntity):
    max_gram: int = Field(description="Characters to include in the longest token that Atlas Search creates.")
    min_gram: int = Field(description="Characters to include in the shortest token that Atlas Search creates.")
    type: TokenizerType = Field(description="Human-readable label that identifies this tokenizer type.")

    @classmethod
    def from_api(cls, response: dict) -> "Tokenizer":
        return cls(type=TokenizerType(response.pop("type")), **response)


class TokenFilterType(Enum):
    ASCII_FOLDING = "asciiFolding"
    DAITCH_MOKOTOFF_SOUNDEX = "daitchMokotoffSoundex"
    EDGE_GRAM = "edgeGram"
    ICU_FOLDING = "icuFolding"
    ICU_NORMALIZER = "icuNormalizer"
    LENGTH = "length"
    LOWERCASE = "lowercase"
    N_GRAM = "nGram"
    REGEX = "regex"
    REVERSE = "reverse"
    SHINGLE = "shingle"
    SNOWBALL_STEMMING = "snowballStemming"
    STOPWORD = "stopword"
    TRIM = "trim"


class IncludeOmitOption(Enum):
    INCLUDE = "include"
    OMIT = "omit"


class TokenFilter(AtlasEntity):
    type: TokenFilterType
    original_tokens: IncludeOmitOption

    @classmethod
    def from_api(cls, response: dict) -> "TokenFilter":
        return cls(type=TokenFilterType(response["type"]), original_tokens=IncludeOmitOption(response["originalTokens"]))


class AsciiFoldingTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.ASCII_FOLDING
    original_tokens: IncludeOmitOption = Field(
        default=IncludeOmitOption.OMIT,
        description="Value that indicates whether to include or omit the original tokens in the output of the token filter. Choose include if you want to support queries on both the original tokens as well as the converted forms. Choose omit if you want to query only on the converted forms of the original tokens.",
    )


class DaitchMokotoffTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.DAITCH_MOKOTOFF_SOUNDEX
    original_tokens: IncludeOmitOption = Field(
        default=IncludeOmitOption.OMIT,
        description="Value that indicates whether to include or omit the original tokens in the output of the token filter. Choose include if you want to support queries on both the original tokens as well as the converted forms. Choose omit if you want to query only on the converted forms of the original tokens.",
    )


class EdgeGramTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.EDGE_GRAM
    max_gram: int = Field(
        description="Value that specifies the maximum length of generated n-grams. This value must be greater than or equal to min_gram."
    )
    min_gram: int = Field(
        description="Value that specifies the minimum length of generated n-grams. This value must be less than or equal to max_gram."
    )
    term_not_in_bounds: IncludeOmitOption = Field(
        default=IncludeOmitOption.OMIT,
        description="Value that indicates whether to index tokens shorter than min_gram or longer than max_gram.",
    )


class ICUFoldingTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.ICU_FOLDING


class ICUNormalizationForm(Enum):
    NFD = "nfd"
    NFC = "nfc"
    NFKD = "nfkd"
    NFKC = "nfkc"


class ICUNormalizerTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.ICU_NORMALIZER
    normalization_form: ICUNormalizationForm = Field(default=ICUNormalizationForm.NFC, description="Normalization form to apply.")


class LengthTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.LENGTH
    max: int = Field(
        default=255, description="Number that specifies the maximum length of a token. Value must be greater than or equal to min."
    )
    min: int = Field(
        default=0, description="Number that specifies the minimum length of a token. This value must be less than or equal to max."
    )


class LowercaseTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.LOWERCASE


class NGramTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.N_GRAM
    max_gram: int = Field(
        description="Value that specifies the maximum length of generated n-grams. This value must be greater than or equal to min_gram."
    )
    min_gram: int = Field(
        description="Value that specifies the minimum length of generated n-grams. This value must be less than or equal to max_gram."
    )
    term_not_in_bounds: IncludeOmitOption = Field(
        default=IncludeOmitOption.OMIT,
        description="Value that indicates whether to index tokens shorter than min_gram or longer than max_gram.",
    )


class RegexMatches(Enum):
    ALL = "all"
    FIRST = "first"


class RegexTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.REGEX
    matches: RegexMatches = Field(
        description="Value that indicates whether to replace only the first matching pattern or all matching patterns."
    )
    pattern: str = Field(description="Regular expression pattern to apply to each token.")
    replacement: str = Field(description="Replacement string to substitute wherever a matching pattern occurs.")


class StemmerName(Enum):
    ARABIC = "arabic"
    ARMENIAN = "armenian"
    BASQUE = "basque"
    CATALAN = "catalan"
    DANISH = "danish"
    DUTCH = "dutch"
    ENGLISH = "english"
    FINNISH = "finnish"
    FRENCH = "french"
    GERMAN = "german"
    GERMAN2 = "german2"
    HUNGARIAN = "hungarian"
    IRISH = "irish"
    ITALIAN = "italian"
    KP = "kp"
    LITHUANIAN = "lithuanian"
    LOVINS = "lovins"
    NORWEGIAN = "norwegian"
    PORTER = "porter"
    PORTUGUESE = "portuguese"
    ROMANIAN = "romanian"
    RUSSIAN = "russian"
    SPANISH = "spanish"
    SWEDISH = "swedish"
    TURKISH = "turkish"


class ReverseTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.REVERSE


class ShingleTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.SHINGLE
    max_shingle_size: int = Field(
        description="Value that specifies the maximum number of tokens per shingle. This value must be greater than or equal to min_shingle_size."
    )
    min_shingle_size: int = Field(
        description="Value that specifies the minimum number of tokens per shingle. This value must be less than or equal to max_shingle_size."
    )


class SnowballStemmerTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.SNOWBALL_STEMMING
    stemmer_name: StemmerName = Field(description="Snowball-generated stemmer to use.")


class StopwordTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.STOPWORD
    ignore_case: bool = Field(
        default=True, description="Flag that indicates whether to ignore the case of stop words when filtering the tokens to remove."
    )
    tokens: list[str] = Field(description="The stop words that correspond to the tokens to remove. Value must be one or more stop words.")


class TrimTokenFilter(TokenFilter):
    type: TokenFilterType = TokenFilterType.TRIM


class Mapping(AtlasEntity):
    # https://www.mongodb.com/docs/atlas/atlas-search/define-field-mappings/#define-field-mappings
    dynamic: bool = Field(
        default=False, description="Flag that indicates whether the index uses dynamic or static mappings. Required if fields is omitted."
    )
    fields: dict = Field(
        default_factory=dict,
        description="One or more field specifications for the Atlas Search index. Required if mappings.dynamic is omitted or set to false.",
    )


class CustomAnalyzer(AtlasEntity):
    char_filters: list[CharFilter] = Field(
        default_factory=list, description="Filters that examine text one character at a time and perform filtering operations."
    )
    name: str
    token_filters: list[TokenFilter] = Field(default_factory=list, exclude=True)
    tokenizer: Tokenizer = Field(exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "CustomAnalyzer":
        return cls(
            charFilters=[CharFilter.from_api(x) for x in response.pop("charFilters", [])],
            tokenFilters=[TokenFilter.from_api(x) for x in response.pop("tokenFilters", [])],
            tokenizer=Tokenizer.from_api(response.pop("tokenizer")),
            **response,
        )


class SynonymSource(AtlasEntity):
    collection: str


class SynonymMappingDefinition(AtlasEntity):
    analyzer: SearchIndexAnalyzer = Field(
        default=SearchIndexAnalyzer.LUCENE_STANDARD,
        description="Specific pre-defined method chosen to apply to the synonyms to be searched.",
    )
    name: str
    source: SynonymSource

    @classmethod
    def from_api(cls, response: dict) -> "SynonymMappingDefinition":
        return cls(
            analyzer=SearchIndexAnalyzer(response.pop("analyzer")), source=SynonymSource.from_api(response.pop("source")), **response
        )


class AtlasSearchIndex(AtlasEntity):
    collection_name: str
    database: str
    id: str = Field(default="", exclude=True)
    name: str
    status: SearchIndexStatus | None = Field(default=None, exclude=True)
    type: SearchIndexType = Field(default=SearchIndexType.SEARCH)
    analyzer: SearchIndexAnalyzer = Field(
        default=SearchIndexAnalyzer.LUCENE_SIMPLE,
    )
    analyzers: list[CustomAnalyzer] = Field(
        default_factory=list,
    )
    mappings: list[Mapping] = Field(
        default_factory=list,
    )
    search_analyzer: SearchIndexAnalyzer = Field(
        default=SearchIndexAnalyzer.LUCENE_STANDARD,
    )
    synonyms: list[SynonymMappingDefinition] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict) -> "AtlasSearchIndex":
        return cls(
            status=SearchIndexStatus(response.pop("status")),
            type=SearchIndexType(response.pop("type")),
            analyzer=SearchIndexAnalyzer(response.pop("analyzer")),
            analyzers=[CustomAnalyzer.from_api(x) for x in response.pop("analyzers", [])],
            mappings=[Mapping.from_api(x) for x in response.pop("mappings", [])],
            search_analyzer=SearchIndexAnalyzer(response.pop("searchAnalyzer")),
            synonyms=[SynonymMappingDefinition.from_api(x) for x in response.pop("synonyms", [])],
            **response,
        )

    def __str__(self):
        return self.id


AtlasSearchIndexIdentifier: TypeAlias = AtlasSearchIndex | str


class SearchDeploymentInstanceSize(Enum):
    S20_LOWCPU_NVME = "S20_HIGHCPU_NVME"
    S30_HIGHCPU_NVME = "S30_HIGHCPU_NVME"
    S40_HIGHCPU_NVME = "S40_HIGHCPU_NVME"
    S50_HIGHCPU_NVME = "S50_HIGHCPU_NVME"
    S60_HIGHCPU_NVME = "S60_HIGHCPU_NVME"
    S70_HIGHCPU_NVME = "S70_HIGHCPU_NVME"
    S80_HIGHCPU_NVME = "S80_HIGHCPU_NVME"
    S30_LOWCPU_NVME = "S30_LOWCPU_NVME"
    S40_LOWCPU_NVME = "S40_LOWCPU_NVME"
    S50_LOWCPU_NVME = "S50_LOWCPU_NVME"
    S60_LOWCPU_NVME = "S60_LOWCPU_NVME"
    S80_LOWCPU_NVME = "S80_LOWCPU_NVME"
    S90_LOWCPU_NVME = "S90_LOWCPU_NVME"
    S100_LOWCPU_NVME = "S100_LOWCPU_NVME"
    S110_LOWCPU_NVME = "S110_LOWCPU_NVME"


class SearchDeploymentState(Enum):
    IDLE = "IDLE"
    PAUSED = "PAUSED"
    UPDATING = "UPDATING"


class SearchDeploymentSpec(AtlasEntity):
    instance_size: SearchDeploymentInstanceSize
    node_count: int

    @classmethod
    def from_api(cls, response: dict) -> "SearchDeploymentSpec":
        return cls(instance_size=SearchDeploymentInstanceSize(response.pop("instanceSize")), **response)


class SearchDeploymentNode(AtlasEntity):
    specs: list[SearchDeploymentSpec]
    state: SearchDeploymentState = Field(exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "SearchDeploymentNode":
        return cls(specs=[SearchDeploymentSpec.from_api(x) for x in response["specs"]], state=SearchDeploymentState(response["stateName"]))
