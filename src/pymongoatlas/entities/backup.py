from datetime import datetime
from enum import Enum
from typing import Optional, TypeAlias

from pydantic import AnyUrl, BaseModel, EmailStr, Field, NonNegativeInt, PositiveInt

from pymongoatlas.entities.cloud_providers import CloudProviderName, DataProcessingRegion
from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.util import labels_to_dict, optional_isoformat


class DeliveryType(Enum):
    AUTOMATED = "automated"
    DOWNLOAD = "download"
    POINT_IN_TIME = "pointInTime"


class BackupFrequency(Enum):
    HOURLY = "HOURLY"
    DAILY = "DAILY"
    WEEKLY = "WEEKLY"
    MONTHLY = "MONTHLY"
    ONDEMAND = "ONDEMAND"


class BackupRetentionUnit(Enum):
    DAYS = "DAYS"
    WEEKS = "WEEKS"
    MONTHS = "MONTHS"


class BackupPolicyState(Enum):
    ACTIVE = "ACTIVE"
    DISABLING = "DISABLING"
    ENABLING = "ENABLING"
    UPDATING = "UPDATING"


class DiskBackupApiPolicyItem(AtlasEntity):
    frequency_interval: NonNegativeInt
    frequency_type: BackupFrequency = (BackupFrequency.ONDEMAND,)
    id: str = Field(default="", exclude=True)
    retention_unit: BackupRetentionUnit
    retention_value: NonNegativeInt

    @classmethod
    def from_api(cls, response: dict | None) -> "DiskBackupApiPolicyItem | None":
        if not response:
            return
        return cls(
            frequencyType=BackupFrequency(response.pop("frequencyType").upper()),
            retentionUnit=BackupRetentionUnit(response.pop("retentionUnit").upper()),
            **response,
        )


class DiskBackupSnapshotAWSExportBucket(AtlasEntity):
    id: str = Field(default="", exclude=True)
    bucket_name: str
    cloud_provider: CloudProviderName = Field(default=CloudProviderName.AWS)
    iam_role_id: str

    @classmethod
    def from_api(cls, response: dict) -> "DiskBackupSnapshotAWSExportBucket":
        return cls(id=response.pop("_id"), cloudProvider=CloudProviderName(response.pop("cloudProvider")), **response)


class BackupCompliancePolicy(AtlasEntity):
    authorized_email: EmailStr | None = None
    authorized_user_first_name: str | None = None
    authorized_user_last_name: str | None = None
    copy_protection_enabled: bool = False
    encryption_at_rest_enabled: bool = False
    ondemand_policy_item: DiskBackupApiPolicyItem | None = None
    pit_enabled: bool = False
    restore_window_days: PositiveInt | None = None
    scheduled_policy_items: list[DiskBackupApiPolicyItem] = Field(
        default_factory=list,
    )
    state: BackupPolicyState | None = Field(default=None, exclude=True)
    updated_date: datetime | None = Field(default=None, exclude=True)
    updated_user: str | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "BackupCompliancePolicy":
        ondemand_policy_item = response.pop("onDemandPolicyItem", None)
        state = response.pop("state", None)
        policy = cls(
            scheduledPolicyItems=[DiskBackupApiPolicyItem.from_api(x) for x in response.pop("scheduledPolicyItems", [])],
            updatedDate=optional_isoformat(response.get("updatedDate", None)),
            updatedUser=response.get("updatedUser", None),
            **response,
        )
        if ondemand_policy_item:
            policy.ondemand_policy_item = DiskBackupApiPolicyItem.from_api(ondemand_policy_item)
        if state:
            policy.state = BackupPolicyState(state)
        return policy


class DiskBackupBaseRestoreMember(AtlasEntity):
    replica_set_name: str


class ExportStatus(AtlasEntity):
    exported_collections: NonNegativeInt
    total_collections: NonNegativeInt


class ExportState(Enum):
    CANCELLED = "Cancelled"
    FAILED = "Failed"
    IN_PROGRESS = "InProgress"
    QUEUED = "Queued"
    SUCCESSFUL = "Successful"


class DiskBackupExportJob(AtlasEntity):
    components: list[DiskBackupBaseRestoreMember] = Field(default_factory=list, exclude=True)
    created_at: datetime | None = Field(default=None, exclude=True)
    custom_data: dict[str, str] = Field(
        default_factory=dict,
    )
    delivery_url: list[AnyUrl] = Field(
        default_factory=list,
    )
    export_bucket_id: str
    export_status: ExportStatus | None = Field(default=None, exclude=True)
    finished_at: datetime | None = Field(default=None, exclude=True)
    id: str = Field(default="", exclude=True)
    prefix: str | None = Field(default=None, exclude=True)
    snapshot_id: str
    state: ExportState | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "DiskBackupExportJob":
        return cls(
            components=[DiskBackupBaseRestoreMember.from_api(x) for x in response.pop("components")],
            createdAt=datetime.fromisoformat(response.pop("createdAt")),
            customData=labels_to_dict(response.pop("customData", [])),
            exportStatus=ExportStatus.from_api(response.pop("exportStatus")),
            finishedAt=optional_isoformat(response.pop("finishedAt", None)),
            state=ExportState(response.pop("state")),
            **response,
        )


class BSONTimestamp(BaseModel):
    date: datetime
    time: int
    increment: int

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[BSONTimestamp]":
        if not response or not response.get("time"):
            return
        return cls(date=datetime.fromisoformat(response["date"]), time=response["time"], increment=response["increment"])


class DiskBackupSnapshotRestoreJob(AtlasEntity):
    cancelled: bool = Field(default=False, exclude=True)
    delivery_type: DeliveryType
    delivery_url: list[AnyUrl] = Field(default_factory=list, exclude=True)
    desired_timestamp: BSONTimestamp | None = Field(default=None, exclude=True)
    expired: bool = Field(default=False, exclude=True)
    expires_at: datetime | None = Field(default=None, exclude=True)
    failed: bool = Field(default=False, exclude=True)
    finished_at: datetime | None = Field(default=None, exclude=True)
    id: str = Field(default="", exclude=True)
    oplog_inc: PositiveInt | None = None
    oplog_ts: PositiveInt | None = None
    point_in_time_utc_seconds: int | None = None
    snapshot_id: str
    target_cluster_name: str | None = None
    target_group_id: str | None = None
    timestamp: datetime | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            deliveryType=DeliveryType(response.pop("deliveryType")),
            desiredTimestamp=BSONTimestamp.from_api(response.pop("desiredTimestamp", None)),
            finishedAt=optional_isoformat(response.pop("finishedAt", None)),
            timestamp=datetime.fromisoformat(response.pop("timestamp")),
            **response,
        )

    def __str__(self):
        return self.id


DiskBackupSnapshotRestoreJobIdentifier: TypeAlias = DiskBackupSnapshotRestoreJob | str


class DiskBackupCopySetting(AtlasEntity):
    cloud_provider_name: CloudProviderName
    frequencies: list[BackupFrequency]
    region_name: str
    replication_spec_id: str
    should_copy_oplogs: bool = False

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            cloudProviderName=CloudProviderName(response.pop("cloudProvider")),
            frequencies=[BackupFrequency(x) for x in response.pop("frequencies")],
            **response,
        )


class ExtraRetentionSetting(AtlasEntity):
    frequency_type: BackupFrequency
    retention_days: int

    @classmethod
    def from_api(cls, response: dict):
        return cls(frequencyType=BackupFrequency(response.pop("frequencyType")), **response)


class AdvancedDiskBackupSnapshotSchedulePolicy(AtlasEntity):
    id: str
    policy_items: list[DiskBackupApiPolicyItem]

    @classmethod
    def from_api(cls, response: dict):
        return cls(policyItems=[DiskBackupApiPolicyItem.from_api(x) for x in response.pop("policyItems", [])], **response)


class CloudBackupSchedule(AtlasEntity):
    auto_export_enabled: bool = False
    copy_settings: list[DiskBackupCopySetting]
    export: dict | None = None
    extra_retention_settings: list[ExtraRetentionSetting]
    next_snapshot: datetime | None = None
    policies: list[AdvancedDiskBackupSnapshotSchedulePolicy]
    reference_hour_of_day: int
    reference_minute_of_hour: int
    restore_window_days: int
    use_org_and_group_names_in_prefix: bool = False

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            copySettings=[DiskBackupCopySetting.from_api(x) for x in response.pop("copySettings", [])],
            extraRetentionSettings=[ExtraRetentionSetting.from_api(x) for x in response.pop("extraRetentionSettings", [])],
            nextSnapshot=optional_isoformat(response.pop("nextSnapshot", None)),
            policies=[AdvancedDiskBackupSnapshotSchedulePolicy.from_api(x) for x in response.pop("policies", [])],
            **response,
        )


class ReplicaSetSnapshot(AtlasEntity):
    cloud_provider: CloudProviderName | None = None
    copy_regions: list[str] = Field(default_factory=list)
    created_at: datetime
    description: str | None = None
    expires_at: datetime
    frequency_type: BackupFrequency
    id: str = ""
    master_key_uuid: str | None = None
    mongod_version: str | None = None
    policy_items: list[str] = Field(default_factory=list)
    snapshot_type: str
    status: str
    storage_size_bytes: int
    type: str

    def __str__(self):
        return self.id

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            cloudProvider=CloudProviderName(response.pop("cloudProvider", None)),
            createdAt=datetime.fromisoformat(response.pop("createdAt")),
            expiresAt=datetime.fromisoformat(response.pop("expiresAt")),
            frequencyType=BackupFrequency(response.pop("frequencyType").upper()),
            **response,
        )


class CollectionType(Enum):
    STANDARD = "STANDARD"
    TIMESERIES = "TIMESERIES"


class DateFormat(Enum):
    ISODATE = "ISODATE"
    EPOCH_SECONDS = "EPOCH_SECONDS"
    EPOCH_MILLIS = "EPOCH_MILLIS"
    EPOCH_NANOSECONDS = "EPOCH_NANOSECONDS"


class CriteriaType(Enum):
    CUSTOM = "CUSTOM"
    DATE = "DATE"


class DateView(AtlasEntity):
    date_field: str
    date_format: DateFormat = DateFormat.ISODATE
    expire_after_days: int = 7
    type: CriteriaType = Field(default=CriteriaType.DATE, frozen=True)

    @classmethod
    def from_api(cls, response: dict):
        return cls(dateFormat=DateFormat(response.pop("dateFormat")), **response)


class CustomView(AtlasEntity):
    query: str
    type: CriteriaType = Field(default=CriteriaType.CUSTOM, frozen=True)


class DataExpirationRuleView(AtlasEntity):
    expire_after_days: int = Field(
        ge=7,
        le=9215,
    )


class PartitionFieldType(Enum):
    DATE = "date"
    INT = "int"
    LONG = "long"
    OBJECT_ID = "objectId"
    STRING = "string"
    UUID = "uuid"


class OnlineArchivePartition(AtlasEntity):
    field_name: str
    field_type: PartitionFieldType = Field(default=PartitionFieldType.STRING, exclude=True)
    order: int = 0

    @classmethod
    def from_api(cls, response: dict):
        return cls(fieldType=PartitionFieldType(response.pop("fieldType")), **response)


class OnlineArchiveScheduleType(Enum):
    DAILY = "DAILY"
    DEFAULT = "DEFAULT"
    MONTHLY = "MONTHLY"
    WEEKLY = "WEEKLY"


class DailyOnlineArchiveSchedule(AtlasEntity):
    end_hour: int = Field(ge=0, le=23)
    end_minute: int = Field(ge=0, le=59)
    start_hour: int = Field(ge=0, le=23)
    start_minute: int = Field(ge=0, le=59)
    type: OnlineArchiveScheduleType = Field(default=OnlineArchiveScheduleType.DAILY, frozen=True)


class WeeklyOnlineArchiveSchedule(DailyOnlineArchiveSchedule):
    type: OnlineArchiveScheduleType = Field(default=OnlineArchiveScheduleType.WEEKLY, frozen=True)
    day_of_week: int = Field(
        ge=1,
        le=7,
    )


class MonthlyOnlineArchiveSchedule(DailyOnlineArchiveSchedule):
    type: OnlineArchiveScheduleType = Field(default=OnlineArchiveScheduleType.MONTHLY, frozen=True)
    day_of_month: int = Field(ge=1, le=31)


class OnlineArchiveState(Enum):
    PENDING = "PENDING"
    ACTIVE = "ACTIVE"
    IDLE = "IDLE"
    PAUSING = "PAUSING"
    PAUSED = "PAUSED"
    DELETED = "DELETED"
    ORPHANED = "ORPHANED"


class BackupOnlineArchive(AtlasEntity):
    id: str = Field(default="", exclude=True)
    cluster_name: str | None = Field(default=None, exclude=True)
    coll_name: str
    collection_type: CollectionType = Field(default=CollectionType.STANDARD, exclude=True)
    criteria: DateView | CustomView
    data_expiration_rule: DataExpirationRuleView | None = None
    data_process_region: DataProcessingRegion | None = None
    dataset_name: str | None = Field(default=None, exclude=True)
    db_name: str
    partition_fields: list[OnlineArchivePartition] = Field(
        default_factory=list,
    )
    paused: bool = Field(
        default=False,
    )
    schedule: DailyOnlineArchiveSchedule | WeeklyOnlineArchiveSchedule | MonthlyOnlineArchiveSchedule | None = None
    state: OnlineArchiveState = Field(default=OnlineArchiveState.PENDING, exclude=True)

    @classmethod
    def from_api(cls, response: dict):
        criteria = response.pop("criteria", {}) or {}
        criteria_type = CriteriaType(response.pop("type"))
        if criteria_type == CriteriaType.DATE:
            criteria = DateView.from_api(criteria)
        elif criteria_type == CriteriaType.CUSTOM:
            criteria = CustomView.from_api(criteria)
        schedule = response.pop("schedule", {}) or {}
        schedule_type = OnlineArchiveScheduleType(schedule.pop("type", OnlineArchiveScheduleType.DEFAULT.value))
        if schedule_type == OnlineArchiveScheduleType.DAILY:
            schedule = DailyOnlineArchiveSchedule.from_api(**schedule)
        elif schedule_type == OnlineArchiveScheduleType.WEEKLY:
            schedule = WeeklyOnlineArchiveSchedule.from_api(**schedule)
        elif schedule_type == OnlineArchiveScheduleType.MONTHLY:
            schedule = MonthlyOnlineArchiveSchedule.from_api(**schedule)
        else:
            schedule = None
        archive = cls(
            id=response.pop("_id"),
            collectionType=CollectionType(response.pop("collectionType")),
            criteria=criteria,
            dataExpirationRule=DataExpirationRuleView.from_api(response.pop("dataExpirationRule", None)),
            dataProcessRegion=DataProcessingRegion.from_api(response.pop("dataProcessRegion", None)),
            partitionFields=[OnlineArchivePartition.from_api(x) for x in response.pop("partitionFields", [])],
            schedule=schedule,
            state=OnlineArchiveState(response.pop("state")),
            **response,
        )
        return archive

    def __str__(self):
        return self.id


BackupOnlineArchiveIdentifier: TypeAlias = BackupOnlineArchive | str


class BackupTenantSnapshotStatus(Enum):
    COMPLETED = "COMPLETED"
    FAILED = "FAILED"
    PENDING = "PENDING"
    QUEUED = "QUEUED"
    RUNNING = "RUNNING"


class BackupTenantSnapshot(AtlasEntity):
    expiration: datetime | None = None
    finish_time: datetime | None = None
    id: str = ""
    mongodb_version: str
    scheduled_time: datetime | None = None
    start_time: datetime | None = None
    status: BackupTenantSnapshotStatus

    @classmethod
    def from_api(cls, response: dict) -> "BackupTenantSnapshot":
        return cls(
            expiration=optional_isoformat(response.pop("expiration", None)),
            finish_time=optional_isoformat(response.pop("finishTime", None)),
            scheduled_time=optional_isoformat(response.pop("scheduledTime", None)),
            start_time=optional_isoformat(response.pop("startTime", None)),
            status=BackupTenantSnapshotStatus(response.pop("status")),
            **response,
        )

    def __str__(self):
        return self.id


class BackupTenantSnapshotRestoreDeliveryType(Enum):
    DOWNLOAD = "DOWNLOAD"
    RESTORE = "RESTORE"


class BackupTenantSnapshotRestore(AtlasEntity):
    cluster_name: str
    delivery_type: BackupTenantSnapshotRestoreDeliveryType
    expiration_date: datetime | None = None
    id: str = ""
    project_id: str | None = None
    restore_finished_date: datetime | None = None
    restore_scheduled_date: datetime | None = None
    snapshot_finished_date: datetime | None = None
    snapshot_id: str
    snapshot_url: str | None = None
    status: BackupTenantSnapshotStatus
    target_deployment_item_name: str
    target_project_id: str | None = None

    @classmethod
    def from_api(cls, response: dict) -> "BackupTenantSnapshotRestore":
        return cls(
            deliveryType=BackupTenantSnapshotRestoreDeliveryType(response.pop("deliveryType")),
            expirationDate=optional_isoformat(response.pop("expirationDate", None)),
            restoreFinisheDate=optional_isoformat(response.pop("restoreFinishedDate", None)),
            restoreScheduledDate=optional_isoformat(response.pop("restoreScheduledDate", None)),
            snapshotFinishedDate=optional_isoformat(response.pop("snapshotFinishedDate", None)),
            status=BackupTenantSnapshotStatus(response.pop("status")),
            **response,
        )

    def __str__(self):
        return self.id


class ConfigServerType(Enum):
    DEDICATED = "DEDICATED"
    EMBEDDED = "EMBEDDED"


class ShardedClusterSnapshotFrequencyType(Enum):
    HOURLY = "hourly"
    DAILY = "daily"
    WEEKLY = "weekly"
    MONTHLY = "monthly"
    YEARLY = "yearly"


class DiskBackupShardedClusterSnapshotMember(AtlasEntity):
    cloud_provider: CloudProviderName
    id: str
    replica_set_name: str

    @classmethod
    def from_api(cls, response: dict) -> "DiskBackupShardedClusterSnapshotMember":
        return cls(cloudProvider=CloudProviderName(response.pop("cloudProvider")), **response)


class ShardedSnapshotType(Enum):
    ON_DEMAND = "onDemand"
    SCHEDULED = "scheduled"


class ShardedSnapshotStatus(Enum):
    COMPLETED = "completed"
    FAILED = "failed"
    IN_PROGRESS = "inProgress"
    QUEUED = "queued"


class ShardedClusterSnapshotType(Enum):
    REPLICA_SET = "replicaSet"
    SHARDED_CLUSTER = "shardedCluster"


class ShardedClusterSnapshot(AtlasEntity):
    config_server_type: ConfigServerType
    created_at: datetime | None = None
    description: str | None = None
    expires_at: datetime | None = None
    frequency_type: ShardedClusterSnapshotFrequencyType
    id: str = ""
    master_key_uuid: str | None = None
    members: list[DiskBackupShardedClusterSnapshotMember] = Field(
        default_factory=list,
    )
    mongod_version: str
    policy_items: list[str] = Field(
        default_factory=list,
    )
    snapshot_ids: list[str] = Field(
        default_factory=list,
    )
    snapshot_type: ShardedSnapshotType
    status: ShardedSnapshotStatus | None = None
    storage_size_bytes: int | None = None
    type: ShardedClusterSnapshotType

    @classmethod
    def from_api(cls, response: dict) -> "ShardedClusterSnapshot":
        return cls(
            configServerType=ConfigServerType(response.pop("configServerType")),
            createdAt=datetime.fromisoformat(response.pop("createdAt")),
            expiresAt=optional_isoformat(response.pop("expiresAt", None)),
            frequencyType=ShardedClusterSnapshotFrequencyType(response.pop("frequencyType")),
            members=[DiskBackupShardedClusterSnapshotMember.from_api(x) for x in response.pop("members", [])],
            snapshotType=ShardedSnapshotType(response.pop("snapshotType")),
            status=ShardedSnapshotStatus(response.pop("status")),
            type=ShardedClusterSnapshotType(response.pop("type")),
            **response,
        )

    def __str__(self):
        return self.id


DiskBackupSnapshot: TypeAlias = ReplicaSetSnapshot | ShardedClusterSnapshot
DiskBackupSnapshotIdentifier: TypeAlias = DiskBackupSnapshot | str
