import json
from enum import Enum

from pydantic import BaseModel, Field


class AuditConfigType(Enum):
    FILTER_BUILDER = "FILTER_BUILDER"
    FILTER_JSON = "FILTER_JSON"
    NONE = "NONE"


class AuditingConfig(BaseModel):
    audit_authorization_success: bool = Field(
        default=False,
        description="Flag that indicates whether someone set auditing to track successful authentications. This only applies to the 'atype' : 'authCheck' audit filter. Setting this parameter to true degrades cluster performance.",
        json_schema_extra={
            "documentation": "https://docs.mongodb.com/manual/reference/audit-message/#audit-event-actions-details-and-results"
        },
    )
    audit_filter: dict = Field(
        default_factory=dict,
        description=r"JSON document that specifies which events to record. Escape any characters that may prevent parsing, such as single or double quotes, using a backslash (\).",
        json_schema_extra={"documentation": "https://www.mongodb.com/docs/atlas/database-auditing/#configure-a-custom-auditing-filter"},
    )
    configuration_type: AuditConfigType = Field(description="Human-readable label that displays how to configure the audit filter.")
    enabled: bool = Field(
        default=False, description="Flag that indicates whether someone enabled database auditing for the specified project."
    )

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            audit_authorization_success=response.get("auditAuthorizationSuccess", False),
            audit_filter=json.loads(response.get("auditFilter", "{}")),
            configuration_type=AuditConfigType(response["configurationType"]),
            enabled=response["enabled"],
        )

    def to_api(self) -> dict:
        return {"auditAuthorizationSuccess": self.audit_authorization_success, "auditFilter": self.audit_filter, "enabled": self.enabled}
