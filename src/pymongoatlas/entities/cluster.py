import typing
from datetime import datetime
from enum import Enum
from urllib.parse import urlparse

from pydantic import Field, NonNegativeInt, PositiveInt
from typing_extensions import Annotated

from pymongoatlas.entities.cloud_providers import CloudProviderName, CloudProviderRegion, get_cloud_provider_region
from pymongoatlas.entities.common import AtlasEntity, Tag
from pymongoatlas.entities.monitoring import ProcessType
from pymongoatlas.entities.networking import ServerlessPrivateEndpoint
from pymongoatlas.util import optional_isoformat, remove_port

if typing.TYPE_CHECKING:
    from pymongoatlas.entities.monitoring import Process


class ChangeStatus(Enum):
    APPLIED = "APPLIED"
    PENDING = "PENDING"


class Region(AtlasEntity):
    name: str
    default: bool = False


class InstanceSizeName(Enum):
    M0 = "M0"
    M2 = "M2"
    M5 = "M5"
    M10 = "M10"
    M20 = "M20"
    M30 = "M30"
    M40 = "M40"
    M50 = "M50"
    M60 = "M60"
    M90 = "M90"
    M80 = "M80"
    M100 = "M100"
    M140 = "M140"
    M200 = "M200"
    M250 = "M250"
    M300 = "M300"
    M400 = "M400"
    R40 = "R40"
    R50 = "R50"
    R60 = "R60"
    R80 = "R80"
    R200 = "R200"
    R300 = "R300"
    R400 = "R400"
    R600 = "R600"
    R700 = "R700"
    M40_NVME = "M40_NVME"
    M50_NVME = "M50_NVME"
    M60_NVME = "M60_NVME"
    M80_NVME = "M80_NVME"
    M200_NVME = "M200_NVME"
    M300_NVME = "M300_NVME"
    M400_NVME = "M400_NVME"
    M600_NVME = "M600_NVME"


CommonInstanceSize = Annotated[
    InstanceSizeName,
    typing.Union[
        InstanceSizeName.M10,
        InstanceSizeName.M20,
        InstanceSizeName.M30,
        InstanceSizeName.M40,
        InstanceSizeName.M50,
        InstanceSizeName.M60,
        InstanceSizeName.M80,
        InstanceSizeName.M200,
        InstanceSizeName.R40,
        InstanceSizeName.R50,
        InstanceSizeName.R60,
        InstanceSizeName.R80,
        InstanceSizeName.R200,
        InstanceSizeName.R300,
        InstanceSizeName.R400,
    ],
]


AWSInstanceSize = Annotated[
    InstanceSizeName,
    typing.Union[
        CommonInstanceSize,
        InstanceSizeName.M100,
        InstanceSizeName.M140,
        InstanceSizeName.M300,
        InstanceSizeName.R700,
        InstanceSizeName.M40_NVME,
        InstanceSizeName.M50_NVME,
        InstanceSizeName.M60_NVME,
        InstanceSizeName.M80_NVME,
        InstanceSizeName.M200_NVME,
        InstanceSizeName.M400_NVME,
    ],
]


AzureInstanceSize = Annotated[
    InstanceSizeName,
    typing.Union[
        CommonInstanceSize,
        InstanceSizeName.M90,
        InstanceSizeName.M60_NVME,
        InstanceSizeName.M80_NVME,
        InstanceSizeName.M200_NVME,
        InstanceSizeName.M300_NVME,
        InstanceSizeName.M400_NVME,
        InstanceSizeName.M600_NVME,
    ],
]

GCPInstanceSize = Annotated[
    InstanceSizeName,
    typing.Union[
        CommonInstanceSize,
        InstanceSizeName.M140,
        InstanceSizeName.M250,
        InstanceSizeName.M300,
        InstanceSizeName.M400,
        InstanceSizeName.R600,
    ],
]


TenantInstanceSize = Annotated[InstanceSizeName, typing.Union[InstanceSizeName.M0, InstanceSizeName.M2, InstanceSizeName.M5]]


class InstanceSize(AtlasEntity):
    name: InstanceSizeName
    available_regions: list[Region]

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            name=InstanceSizeName(response["name"]), availableRegions=[Region.from_api(x) for x in response.get("availableRegions", [])]
        )

    def get_default_region(self) -> Region:
        return next(x for x in self.available_regions if x.default)


class EBSVolumeType(Enum):
    """
    Type of storage you want to attach to your AWS-provisioned cluster.

    STANDARD volume types can't exceed the default input/output operations per second (IOPS) rate for the selected
    volume size.

    PROVISIONED volume types must fall within the allowable IOPS range for the selected volume size. You must set this
    value to (PROVISIONED) for NVMe clusters.
    """

    STANDARD = "STANDARD"
    PROVISIONED = "PROVISIONED"


class AWSHardwareSpec(AtlasEntity):
    disk_iops: int | None = None
    ebs_volume_type: EBSVolumeType = EBSVolumeType.STANDARD
    instance_size: AWSInstanceSize
    node_count: NonNegativeInt

    @classmethod
    def from_api(cls, response: dict) -> "AWSHardwareSpec":
        return cls(
            ebsVolumeType=EBSVolumeType(response.pop("ebsVolumeType")),
            instanceSize=InstanceSizeName(response.pop("instanceSize")),
            **response,
        )


class AzureHardwareSpec(AtlasEntity):
    instance_size: AzureInstanceSize
    node_count: NonNegativeInt

    @classmethod
    def from_api(cls, response: dict) -> "AzureHardwareSpec":
        return cls(instanceSize=InstanceSizeName(response.pop("instanceSize")), **response)


class GCPHardwareSpec(AtlasEntity):
    instance_size: GCPInstanceSize
    node_count: NonNegativeInt

    @classmethod
    def from_api(cls, response: dict) -> "GCPHardwareSpec":
        return cls(instanceSize=InstanceSizeName(response.pop("instanceSize")), **response)


class TenantHardwareSpec(AtlasEntity):
    instance_size: TenantInstanceSize

    @classmethod
    def from_api(cls, response: dict) -> "TenantHardwareSpec":
        return cls(instanceSize=InstanceSizeName(response["instanceSize"]))


HardwareSpec: typing.TypeAlias = typing.Union[
    AWSHardwareSpec,
    AzureHardwareSpec,
    GCPHardwareSpec,
    TenantHardwareSpec,
]

ReadOnlySpec: typing.TypeAlias = typing.Union[
    AWSHardwareSpec,
    AzureHardwareSpec,
    GCPHardwareSpec,
]


def process_specs(cloud_provider: CloudProviderName, specs: dict) -> HardwareSpec:
    provider_to_spec_mapper = {
        CloudProviderName.AWS: AWSHardwareSpec.from_api,
        CloudProviderName.AZURE: AzureHardwareSpec.from_api,
        CloudProviderName.GCP: GCPHardwareSpec.from_api,
        CloudProviderName.TENANT: TenantHardwareSpec.from_api,
    }

    return provider_to_spec_mapper[cloud_provider](specs)


class AutoScalingComputeSettings(AtlasEntity):
    enabled: bool
    max_instance_size: AWSInstanceSize | AzureInstanceSize | GCPInstanceSize | None = None
    min_instance_size: AWSInstanceSize | AzureInstanceSize | GCPInstanceSize | None = None
    scale_down_enabled: bool | None = None

    @classmethod
    def from_api(cls, response: dict) -> "AutoScalingComputeSettings":
        entity = cls(enabled=response["enabled"], scaleDownEnabled=response.get("scaleDownEnabled"))
        min_instance_size = response.get("minInstanceSize")
        if min_instance_size:
            entity.min_instance_size = InstanceSizeName(min_instance_size)
        max_instance_size = response.get("maxInstanceSize")
        if max_instance_size:
            entity.max_instance_size = InstanceSizeName(max_instance_size)
        return entity


class AutoScalingDiskSettings(AtlasEntity):
    enabled: bool


class AutoScalingSettings(AtlasEntity):
    compute: AutoScalingComputeSettings
    disk: AutoScalingDiskSettings

    @classmethod
    def from_api(cls, response: dict | None) -> "typing.Optional[AutoScalingSettings]":
        if not response:
            return
        return cls(
            compute=AutoScalingComputeSettings.from_api(response["compute"]), disk=AutoScalingDiskSettings.from_api(response["diskGB"])
        )


class RegionConfig(AtlasEntity):
    analytics_specs: ReadOnlySpec | None = None
    auto_scaling: AutoScalingSettings | None = None
    analytics_auto_scaling: AutoScalingSettings | None = None
    electable_specs: HardwareSpec | None = None
    priority: int = Field(ge=0, le=7)
    provider_name: CloudProviderName
    read_only_specs: ReadOnlySpec | None = None
    region_name: CloudProviderRegion

    @classmethod
    def from_api(cls, response: dict):
        cloud_provider = CloudProviderName(response.pop("providerName"))
        return cls(
            analyticsSpecs=process_specs(cloud_provider=cloud_provider, specs=response.pop("analyticsSpecs", [])),
            autoScaling=AutoScalingSettings.from_api(response.pop("autoScaling", None)),
            analyticsAutoScaling=AutoScalingSettings.from_api(response.pop("analyticsAutoScaling", None)),
            electableSpecs=process_specs(cloud_provider=cloud_provider, specs=response.pop("electableSpecs", [])),
            providerName=cloud_provider,
            readOnlySpecs=process_specs(cloud_provider=cloud_provider, specs=response.pop("readOnlySpecs", [])),
            regionName=get_cloud_provider_region(cloud_provider, response.pop("regionName")),
            **response,
        )


class ReplicationSpec(AtlasEntity):
    id: str = Field(default="", exclude=True)
    num_shards: PositiveInt
    region_configs: list[RegionConfig]
    zone_name: str | None = Field(
        default=None,
    )

    @classmethod
    def from_api(cls, response: dict):
        return cls(regionConfigs=[RegionConfig.from_api(x) for x in response.pop("regionConfigs", [])], **response)


class ClusterState(Enum):
    CREATING = "CREATING"
    DELETING = "DELETING"
    IDLE = "IDLE"
    REPAIRING = "REPAIRING"
    UPDATING = "UPDATING"


class VersionReleaseSystem(Enum):
    CONTINUOUS = "CONTINUOUS"
    LTS = "LTS"


class BIConnectorReadPreference(Enum):
    PRIMARY = "PRIMARY"
    SECONDARY = "SECONDARY"
    ANALYTICS = "ANALYTICS"


class BIConnector(AtlasEntity):
    enabled: bool
    read_preference: BIConnectorReadPreference

    @classmethod
    def from_api(cls, response: dict | None) -> "typing.Optional[BIConnector]":
        if not response:
            return
        return cls(readPreference=BIConnectorReadPreference(response.pop("readPreference").upper()), **response)


class ClusterType(Enum):
    REPLICASET = "REPLICASET"
    SHARDED = "SHARDED"
    GEOSHARDED = "GEOSHARDED"


class DiskWarmingMode(Enum):
    """
    https://docs.atlas.mongodb.com/reference/replica-set-tags/#reduce-secondary-disk-warming-impact
    """

    FULLY_WARMED = "FULLY_WARMED"
    VISIBLE_EARLIER = "VISIBLE_EARLIER"


class EncryptionAtRestProvider(Enum):
    AWS = "AWS"
    AZURE = "AZURE"
    GCP = "GCP"
    NONE = "NONE"


class Cluster(AtlasEntity):
    accept_data_risks_and_force_replica_set_reconfig: datetime | None = Field(
        default=None,
    )
    backup_enabled: bool = False
    bi_connector: BIConnector | None = Field(
        default=None,
    )
    cluster_type: ClusterType
    connection_strings: dict = Field(default_factory=dict)
    created: datetime | None = Field(default=None, exclude=True)
    disk_size_gb: float = Field(
        ge=10,
        le=4096,
        default=10,
    )
    disk_warming_mode: DiskWarmingMode = DiskWarmingMode.FULLY_WARMED
    encryption_at_rest_provider: EncryptionAtRestProvider
    global_cluster_self_managed_sharding: bool | None = Field(
        default=None,
    )
    id: str = Field(default="", exclude=True)
    mongo_db_major_version: str = Field(alias="mongoDBMajorVersion")
    mongo_db_version: str = Field(default="", alias="mongoDBVersion", exclude=True)
    name: str = Field(pattern=r"^[a-zA-Z0-9][a-zA-Z0-9-]*${1,64}")
    paused: bool = False
    pit_enabled: bool = Field(
        default=False,
    )
    replication_specs: list[ReplicationSpec] = Field(default_factory=list)
    root_cert_type: str = Field(default="ISRGROOTX1", exclude=True)
    state: ClusterState | None = Field(default=None, exclude=True)
    tags: list[Tag] = Field(default_factory=list)
    termination_protection_enabled: bool = False
    version_release_system: VersionReleaseSystem = VersionReleaseSystem.LTS

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            acceptDataRisksAndForceReplicaSetReconfig=optional_isoformat(response.pop("acceptDataRisksAndForceReplicaSetReconfig", None)),
            biConnector=BIConnector.from_api(response.pop("biConnector", None)),
            clusterType=ClusterType(response.pop("clusterType")),
            created=datetime.fromisoformat(response.pop("createDate")),
            encryptionAtRestProvider=EncryptionAtRestProvider(response.pop("encryptionAtRestProvider")),
            replicationSpecs=[ReplicationSpec.from_api(x) for x in response.pop("replicationSpecs", [])],
            state=ClusterState(response["stateName"]),
            tags=[Tag.from_api(x) for x in response.pop("tags", [])],
            versionReleaseSystem=VersionReleaseSystem(response.pop("versionReleaseSystem")),
            **response,
        )

    def __str__(self):
        return self.name

    def get_user_aliases(self) -> list[str]:
        parsed = urlparse(self.connection_strings.pop("standard"))
        hostnames = parsed.netloc.split(",")
        return [remove_port(x) for x in hostnames]

    def get_processes(self, project_processes: list["Process"], process_type: ProcessType | None = None) -> list["Process"]:
        """
        Get the processes mapped to this cluster.

        :param project_processes: The list of processes in the Project of this Cluster.
        :param process_type: Optional process type filter
        """
        aliases = self.get_user_aliases()
        processes = [x for x in project_processes if x.user_alias in aliases]
        if process_type is not None:
            processes = [x for x in processes if x.type_name == process_type]
        return processes

    def get_primary_process(self, project_processes: list["Process"]) -> "Process":
        """
        Get the processes mapped to this cluster.

        :param project_processes: The list of processes in the Project of this Cluster.
        """
        processes = self.pop_processes(project_processes=project_processes, process_type=ProcessType.REPLICA_PRIMARY)
        return processes[0]


ClusterIdentifier: typing.TypeAlias = Cluster | str


class SampleDatasetLoadState(Enum):
    COMPLETED = "COMPLETED"
    FAILED = "FAILED"
    WORKING = "WORKING"


class SampleDatasetLoadStatus(AtlasEntity):
    id: str
    complete_date: datetime | None
    create_date: datetime
    error_message: str | None
    state: SampleDatasetLoadState

    @classmethod
    def from_api(cls, response: dict) -> "SampleDatasetLoadStatus":
        return cls(
            id=response.pop("_id"),
            completeDate=optional_isoformat(response.pop("completeDate")),
            createDate=datetime.fromisoformat(response.pop("createDate")),
            state=SampleDatasetLoadState(response.pop("state")),
            **response,
        )

    def __str__(self):
        return self.id


SampleDatasetLoadStatusIdentifier: typing.TypeAlias = SampleDatasetLoadStatus | str


class OutageSimulationState(Enum):
    COMPLETE = "COMPLETE"
    RECOVERING = "RECOVERING"
    RECOVERY_REQUESTED = "RECOVERY_REQUESTED"
    SIMULATING = "SIMULATING"
    STARTING = "STARTING"
    START_REQUESTED = "START_REQUESTED"


class OutageSimulationFilter(AtlasEntity):
    cloud_provider: CloudProviderName
    region_name: str
    type: str = "REGION"

    @classmethod
    def from_api(cls, response: dict) -> "OutageSimulationFilter":
        return cls(cloudProvider=CloudProviderName(response.pop("cloudProvider")), **response)


class OutageSimulation(AtlasEntity):
    id: str
    outage_filters: list[OutageSimulationFilter]
    start_request_date: datetime | None = None
    state: OutageSimulationState

    @classmethod
    def from_api(cls, response: dict) -> "OutageSimulation":
        return cls(
            outageFilters=[OutageSimulationFilter.from_api(x) for x in response.pop("outageFilters", [])],
            startRequestDate=optional_isoformat(response.pop("startRequestDate")),
            state=OutageSimulationState(response.pop("state")),
            **response,
        )


class ServerlessInstanceConnectionString(AtlasEntity):
    private_endpoint: list[ServerlessPrivateEndpoint]
    standard_srv: str

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessInstanceConnectionString":
        return cls(privateEndpoint=[ServerlessPrivateEndpoint.from_api(x) for x in response.pop("privateEndpoint", [])], **response)


class ServerlessInstanceCloudProviderSettings(AtlasEntity):
    backing_provider_name: CloudProviderName
    provider_name: CloudProviderName = Field(default=CloudProviderName.SERVERLESS, exclude=True)
    region_name: str

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessInstanceCloudProviderSettings":
        return cls(
            backingProviderName=CloudProviderName(response.pop("backingProviderName")),
            providerName=CloudProviderName(response.pop("providerName")),
            **response,
        )

    def to_api(self) -> dict:
        return {"backingProviderName": self.backing_provider_name.value, "regionName": self.region_name}


class ServerlessBackupOptions(AtlasEntity):
    serverless_continuous_backup_enabled: bool


class ServerlessInstance(AtlasEntity):
    connection_strings: ServerlessInstanceConnectionString | None = None
    create_date: datetime | None = Field(default=None, exclude=True)
    group_id: str = Field(default="", exclude=True)
    id: str = Field(default="", exclude=True)
    mongodb_version: str = Field(default="", exclude=True)
    name: str
    provider_settings: ServerlessInstanceCloudProviderSettings
    serverless_backup_options: ServerlessBackupOptions | None = None
    state: ClusterState = Field(default=ClusterState.IDLE, exclude=True)
    tags: dict = Field(default_factory=dict)
    termination_protection_enabled: bool = False

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessInstance":
        return cls(
            connectionStrings=ServerlessInstanceConnectionString.from_api(response.pop("connectionStrings")),
            createDate=datetime.fromisoformat(response.pop("createDate")),
            providerSettings=ServerlessInstanceCloudProviderSettings.from_api(response.pop("providerSettings")),
            serverlessBackupOptions=ServerlessBackupOptions.from_api(response.pop("serverlessBackupOptions")),
            state=ClusterState(response.pop("stateName")),
            tags={x["key"]: x["value"] for x in response.pop("tags", [])},
            **response,
        )

    def __str__(self):
        return self.name


class CloudProvider(AtlasEntity):
    name: CloudProviderName
    instance_sizes: list[InstanceSize]

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            name=CloudProviderName(response.pop("provider")),
            instanceSizes=[InstanceSize.from_api(x) for x in response.pop("instanceSizes", [])],
        )


class CustomZoneMapping(AtlasEntity):
    location: str
    zone: str


class ManagedNamespace(AtlasEntity):
    collection: str
    custom_shard_key: str
    db: str


class GlobalClusterManagedNamespace(AtlasEntity):
    custom_zone_mappings: list[CustomZoneMapping] = Field(
        default_factory=list,
    )
    managed_namespaces: list[ManagedNamespace]
    self_managed_sharding: bool = Field(
        default=False,
    )

    @classmethod
    def from_api(cls, response: dict) -> "GlobalClusterManagedNamespace":
        entity = cls(
            managedNamespaces=[ManagedNamespace.from_api(x) for x in response.pop("managedNamespaces", [])],
            selfManagedSharding=response.pop("selfManagedSharding"),
        )
        mappings = response.pop("customZoneMappings", {}).items()
        if mappings:
            entity.custom_zone_mappings = [CustomZoneMapping(zone=x[0], location=x[1]) for x in mappings]
        return entity
