from datetime import datetime
from enum import Enum
from typing import Annotated, Optional, TypeAlias, Union

from pydantic import BaseModel, Field

from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.util import optional_isoformat


class AWSRegion(Enum):
    US_GOV_WEST_1 = "US_GOV_WEST_1"
    US_GOV_EAST_1 = "US_GOV_EAST_1"
    US_EAST_1 = "US_EAST_1"
    US_EAST_2 = "US_EAST_2"
    US_WEST_1 = "US_WEST_1"
    US_WEST_2 = "US_WEST_2"
    CA_CENTRAL_1 = "CA_CENTRAL_1"
    EU_NORTH_1 = "EU_NORTH_1"
    EU_WEST_1 = "EU_WEST_1"
    EU_WEST_2 = "EU_WEST_2"
    EU_WEST_3 = "EU_WEST_3"
    EU_CENTRAL_1 = "EU_CENTRAL_1"
    AP_EAST_1 = "AP_EAST_1"
    AP_NORTHEAST_1 = "AP_NORTHEAST_1"
    AP_NORTHEAST_2 = "AP_NORTHEAST_2"
    AP_NORTHEAST_3 = "AP_NORTHEAST_3"
    AP_SOUTHEAST_1 = "AP_SOUTHEAST_1"
    AP_SOUTHEAST_2 = "AP_SOUTHEAST_2"
    AP_SOUTHEAST_3 = "AP_SOUTHEAST_3"
    AP_SOUTHEAST_4 = "AP_SOUTHEAST_4"
    AP_SOUTH_1 = "AP_SOUTH_1"
    AP_SOUTH_2 = "AP_SOUTH_2"
    SA_EAST_1 = "SA_EAST_1"
    CN_NORTH_1 = "CN_NORTH_1"
    CN_NORTHWEST_1 = "CN_NORTHWEST_1"
    ME_SOUTH_1 = "ME_SOUTH_1"
    ME_CENTRAL_1 = "ME_CENTRAL_1"
    AF_SOUTH_1 = "AF_SOUTH_1"
    EU_SOUTH_1 = "EU_SOUTH_1"
    EU_SOUTH_2 = "EU_SOUTH_2"
    IL_CENTRAL_1 = "IL_CENTRAL_1"
    CA_WEST_1 = "CA_WEST_1"
    GLOBAL = "GLOBAL"


class AzureRegion(Enum):
    US_CENTRAL = "US_CENTRAL"
    US_EAST = "US_EAST"
    US_EAST_2 = "US_EAST_2"
    US_NORTH_CENTRAL = "US_NORTH_CENTRAL"
    US_WEST = "US_WEST"
    US_SOUTH_CENTRAL = "US_SOUTH_CENTRAL"
    EUROPE_NORTH = "EUROPE_NORTH"
    EUROPE_WEST = "EUROPE_WEST"
    US_WEST_CENTRAL = "US_WEST_CENTRAL"
    US_WEST_2 = "US_WEST_2"
    US_WEST_3 = "US_WEST_3"
    CANADA_EAST = "CANADA_EAST"
    CANADA_CENTRAL = "CANADA_CENTRAL"
    BRAZIL_SOUTH = "BRAZIL_SOUTH"
    BRAZIL_SOUTHEAST = "BRAZIL_SOUTHEAST"
    AUSTRALIA_CENTRAL = "AUSTRALIA_CENTRAL"
    AUSTRALIA_CENTRAL_2 = "AUSTRALIA_CENTRAL_2"
    AUSTRALIA_EAST = "AUSTRALIA_EAST"
    AUSTRALIA_SOUTH_EAST = "AUSTRALIA_SOUTH_EAST"
    GERMANY_CENTRAL = "GERMANY_CENTRAL"
    GERMANY_NORTH_EAST = "GERMANY_NORTH_EAST"
    GERMANY_WEST_CENTRAL = "GERMANY_WEST_CENTRAL"
    GERMANY_NORTH = "GERMANY_NORTH"
    SWEDEN_CENTRAL = "SWEDEN_CENTRAL"
    SWEDEN_SOUTH = "SWEDEN_SOUTH"
    SWITZERLAND_NORTH = "SWITZERLAND_NORTH"
    SWITZERLAND_WEST = "SWITZERLAND_WEST"
    UK_SOUTH = "UK_SOUTH"
    UK_WEST = "UK_WEST"
    NORWAY_EAST = "NORWAY_EAST"
    NORWAY_WEST = "NORWAY_WEST"
    INDIA_CENTRAL = "INDIA_CENTRAL"
    INDIA_SOUTH = "INDIA_SOUTH"
    INDIA_WEST = "INDIA_WEST"
    CHINA_EAST = "CHINA_EAST"
    CHINA_NORTH = "CHINA_NORTH"
    ASIA_EAST = "ASIA_EAST"
    JAPAN_EAST = "JAPAN_EAST"
    ASIA_SOUTH_EAST = "ASIA_SOUTH_EAST"
    KOREA_CENTRAL = "KOREA_CENTRAL"
    KOREA_SOUTH = "KOREA_SOUTH"
    FRANCE_CENTRAL = "FRANCE_CENTRAL"
    SOUTH_AFRICA_NORTH = "SOUTH_AFRICA_NORTH"
    SOUTH_AFRICA_WEST = "SOUTH_AFRICA_WEST"
    UAE_CENTRAL = "UAE_CENTRAL"
    UAE_NORTH = "UAE_NORTH"
    QATAR_CENTRAL = "QATAR_CENTRAL"


class GCPRegion(Enum):
    EASTERN_US = "EASTERN_US"
    GOV_EASTERN_US = "GOV_EASTERN_US"
    US_EAST_4 = "US_EAST_4"
    GOV_US_EAST_4 = "GOV_US_EAST_4"
    US_EAST_5 = "US_EAST_5"
    GOV_US_EAST_5 = "GOV_US_EAST_5"
    US_WEST_2 = "US_WEST_2"
    GOV_US_WEST_2 = "GOV_US_WEST_2"
    US_WEST_3 = "US_WEST_3"
    GOV_US_WEST_3 = "GOV_US_WEST_3"
    US_WEST_4 = "US_WEST_4"
    GOV_US_WEST_4 = "GOV_US_WEST_4"
    US_SOUTH_1 = "US_SOUTH_1"
    GOV_US_SOUTH_1 = "GOV_US_SOUTH_1"
    CENTRAL_US = "CENTRAL_US"
    GOV_CENTRAL_US = "GOV_CENTRAL_US"
    WESTERN_US = "WESTERN_US"
    GOV_WESTERN_US = "GOV_WESTERN_US"
    NORTH_AMERICA_NORTHEAST_1 = "NORTH_AMERICA_NORTHEAST_1"
    NORTH_AMERICA_NORTHEAST_2 = "NORTH_AMERICA_NORTHEAST_2"
    SOUTH_AMERICA_EAST_1 = "SOUTH_AMERICA_EAST_1"
    SOUTH_AMERICA_WEST_1 = "SOUTH_AMERICA_WEST_1"
    WESTERN_EUROPE = "WESTERN_EUROPE"
    EUROPE_NORTH_1 = "EUROPE_NORTH_1"
    EUROPE_WEST_2 = "EUROPE_WEST_2"
    EUROPE_WEST_3 = "EUROPE_WEST_3"
    EUROPE_WEST_4 = "EUROPE_WEST_4"
    EUROPE_WEST_6 = "EUROPE_WEST_6"
    EUROPE_WEST_8 = "EUROPE_WEST_8"
    EUROPE_WEST_9 = "EUROPE_WEST_9"
    EUROPE_WEST_10 = "EUROPE_WEST_10"
    EUROPE_WEST_12 = "EUROPE_WEST_12"
    EUROPE_SOUTHWEST_1 = "EUROPE_SOUTHWEST_1"
    EUROPE_CENTRAL_2 = "EUROPE_CENTRAL_2"
    MIDDLE_EAST_CENTRAL_1 = "MIDDLE_EAST_CENTRAL_1"
    MIDDLE_EAST_CENTRAL_2 = "MIDDLE_EAST_CENTRAL_2"
    MIDDLE_EAST_WEST_1 = "MIDDLE_EAST_WEST_1"
    AUSTRALIA_SOUTHEAST_1 = "AUSTRALIA_SOUTHEAST_1"
    AUSTRALIA_SOUTHEAST_2 = "AUSTRALIA_SOUTHEAST_2"
    EASTERN_ASIA_PACIFIC = "EASTERN_ASIA_PACIFIC"
    NORTHEASTERN_ASIA_PACIFIC = "NORTHEASTERN_ASIA_PACIFIC"
    SOUTHEASTERN_ASIA_PACIFIC = "SOUTHEASTERN_ASIA_PACIFIC"
    ASIA_EAST_2 = "ASIA_EAST_2"
    ASIA_NORTHEAST_2 = "ASIA_NORTHEAST_2"
    ASIA_NORTHEAST_3 = "ASIA_NORTHEAST_3"
    ASIA_SOUTH_1 = "ASIA_SOUTH_1"
    ASIA_SOUTH_2 = "ASIA_SOUTH_2"
    ASIA_SOUTHEAST_2 = "ASIA_SOUTHEAST_2"


class CloudProviderName(Enum):
    AWS = "AWS"
    AZURE = "AZURE"
    GCP = "GCP"
    TENANT = "TENANT"
    SERVERLESS = "SERVERLESS"
    NONE = None


CloudProviderIdentifier: TypeAlias = CloudProviderName | str


def check_cloud_provider(name: CloudProviderIdentifier) -> CloudProviderName:
    if name is None:
        raise ValueError("Must specify a valid cloud provider")
    return CloudProviderName(name)


CloudProviderRegion = Annotated[Enum, Union[AWSRegion, AzureRegion, GCPRegion]]


def get_cloud_provider_region(cloud_provider: CloudProviderName, region_name: str) -> CloudProviderRegion:
    mapping = {CloudProviderName.AWS: AWSRegion, CloudProviderName.AZURE: AzureRegion, CloudProviderName.GCP: GCPRegion}
    return mapping[cloud_provider](region_name)


class DataProcessingRegion(AtlasEntity):
    cloud_provider: CloudProviderName
    region: CloudProviderRegion

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[DataProcessingRegion]":
        if not response:
            return
        entity = cls(cloudProvider=CloudProviderName(response.pop("cloudProvider")))
        region = response.pop("region")
        if entity.cloud_provider == CloudProviderName.AWS:
            entity.region = AWSRegion(region)
        elif entity.cloud_provider == CloudProviderName.AZURE:
            entity.region = AzureRegion(region)
        return entity


class FeatureType(Enum):
    ATLAS_DATA_LAKE = "ATLAS_DATA_LAKE"
    ENCRYPTION_AT_REST = "ENCRYPTION_AT_REST"
    EXPORT_SNAPSHOT = "EXPORT_SNAPSHOT"
    PUSH_BASED_LOG_EXPORT = "PUSH_BASED_LOG_EXPORT"


class DataLakeFeatureUsage(AtlasEntity):
    group_id: str
    name: str


class ExportSnapshotFeatureUsage(AtlasEntity):
    export_bucket_id: str
    group_id: str


class PushBasedLogExportFeatureUsage(AtlasEntity):
    bucket_name: str
    group_id: str


FeatureID = Annotated[BaseModel, Union[DataLakeFeatureUsage | ExportSnapshotFeatureUsage | PushBasedLogExportFeatureUsage | None]]


class FeatureUsage(AtlasEntity):
    feature_type: FeatureType
    feature_id: FeatureID

    @classmethod
    def from_api(cls, response: dict) -> "FeatureUsage":
        id_mapper = {
            FeatureType.ATLAS_DATA_LAKE: DataLakeFeatureUsage.from_api,
            FeatureType.ENCRYPTION_AT_REST: lambda feature: None,
            FeatureType.EXPORT_SNAPSHOT: ExportSnapshotFeatureUsage.from_api,
            FeatureType.PUSH_BASED_LOG_EXPORT: PushBasedLogExportFeatureUsage.from_api,
        }
        feature_type = FeatureType(response.pop("featureType"))
        return cls(featureType=feature_type, featureId=id_mapper[feature_type](response.pop("featureId")))


class IAMProvider(Enum):
    AWS = "AWS"
    AZURE = "AZURE"


class AWSIAMRole(AtlasEntity):
    atlas_aws_account_arn: str | None = Field(default=None, exclude=True)
    atlas_assumed_role_external_id: str | None = Field(default=None, exclude=True)
    authorize_date: datetime | None = Field(default=None, exclude=True)
    created_date: datetime | None = Field(default=None, exclude=True)
    feature_usages: list[FeatureUsage] = Field(default_factory=list, exclude=True)
    iam_assumed_role_arn: str = Field(
        min_length=20,
        max_length=2048,
    )
    role_id: str | None = Field(default=None, exclude=True)
    provider_name: IAMProvider = Field(default=IAMProvider.AWS, frozen=True)

    @classmethod
    def from_api(cls, response: dict) -> "AWSIAMRole":
        return cls(
            authorizeDate=optional_isoformat(response.pop("authorizeDate", None)),
            createdDate=datetime.fromisoformat(response.pop("createdDate")),
            featureUsages=[FeatureUsage.from_api(x) for x in response.pop("featureUsages", [])],
            **response,
        )

    def __str__(self):
        return self.role_id


UUID_PATTERN = r"^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$"


class AzureIAMRole(AtlasEntity):
    id: str = Field(default="", exclude=True)
    atlas_azure_app_id: str = Field(pattern=UUID_PATTERN)
    created_date: datetime | None = Field(default=None, exclude=True)
    feature_usages: list[FeatureUsage] = Field(default_factory=list, exclude=True)
    last_updated_date: datetime | None = Field(default=None, exclude=True)
    service_principal_id: str = Field(
        pattern=UUID_PATTERN,
    )
    tenant_id: str = Field(
        pattern=UUID_PATTERN,
    )
    provider_name: IAMProvider = Field(default=IAMProvider.AZURE, frozen=True)

    @classmethod
    def from_api(cls, response: dict) -> "AzureIAMRole":
        return cls(
            createdDate=datetime.fromisoformat(response.pop("createdDate")),
            featureUsages=[FeatureUsage.from_api(x) for x in response.pop("featureUsages", [])],
            lastUpdatedDate=optional_isoformat(response.pop("lastUpdatedDate", None)),
            **response,
        )

    def __str__(self):
        return self.id


IAMRole: TypeAlias = AWSIAMRole | AzureIAMRole
IAMRoleIdentifier: TypeAlias = IAMRole | str
