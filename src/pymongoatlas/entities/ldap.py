from enum import Enum
from typing import Optional, TypeAlias

from pydantic import Field

from pymongoatlas.entities.common import AtlasEntity


class LDAPConfigStatus(Enum):
    FAIL = "FAIL"
    PENDING = "PENDING"
    SUCCESS = "SUCCESS"


class LDAPVerificationTestStatus(Enum):
    FAIL = "FAIL"
    OK = "OK"


class LDAPValidationType(Enum):
    AUTHENTICATE = "AUTHENTICATE"
    AUTHORIZATION_ENABLED = "AUTHORIZATION_ENABLED"
    CONNECT = "CONNECT"
    PARSE_AUTHZ_QUERY = "PARSE_AUTHZ_QUERY"
    QUERY_SERVER = "QUERY_SERVER"
    SERVER_SPECIFIED = "SERVER_SPECIFIED"
    TEMPLATE = "TEMPLATE"


class CustomerX509(AtlasEntity):
    cas: str


class UserDNMapping(AtlasEntity):
    ldap_query: str | None = None
    match: str
    substitution: str | None = None


class LDAPSecuritySettings(AtlasEntity):
    authentication_enabled: bool = True
    authorization_enabled: bool = True
    authz_query_template: str = Field(
        default="{USER}?memberOf?base",
    )
    bind_username: str | None = Field(
        default=None,
        pattern=r"^(?:(?P<cn>CN=(?P<name>[^,]*)),)?(?:(?P<path>(?:(?:CN|OU)=[^,]+,?)+),)?(?P<domain>(?:DC=[^,]+,?)+)$",
    )
    bind_password: str | None = Field(default=None)
    ca_certificate: str | None = None
    hostname: str = Field(
        pattern=r"^([0-9]{1,3}\.){3}[0-9]{1,3}|([0-9a-f]{1,4}:){7}([0-9a-f]{1,4})|(([a-z0-9]+\.){1,10}[a-z]+)?$",
    )
    port: int = Field(default=636)
    user_to_dn_mapping: list[UserDNMapping] = Field(
        alias="userToDNMapping",
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[LDAPSecuritySettings]":
        if not response:
            return
        return cls(userToDNMapping=[UserDNMapping.from_api(x) for x in response.pop("userToDNMapping", []) or []], **response)


class LDAPConfig(AtlasEntity):
    customer_x509: CustomerX509 | None = None
    ldap: LDAPSecuritySettings | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            customerX509=CustomerX509.from_api(response.get("customerX509")), ldap=LDAPSecuritySettings.from_api(response.get("ldap"))
        )


class LDAPConnectivityValidation(AtlasEntity):
    status: LDAPVerificationTestStatus
    validation_type: LDAPValidationType

    @classmethod
    def from_api(cls, response: dict) -> "LDAPConnectivityValidation":
        return cls(status=LDAPVerificationTestStatus(response["status"]), validationType=LDAPValidationType(response["validationType"]))


class LDAPConfigVerification(AtlasEntity):
    request: LDAPSecuritySettings
    status: LDAPConfigStatus
    validations: list[LDAPConnectivityValidation]
    request_id: str

    @classmethod
    def from_api(cls, response: dict) -> "LDAPConfigVerification":
        return cls(
            request=LDAPSecuritySettings.from_api(response.pop("request")),
            status=LDAPConfigStatus(response.pop("status")),
            validations=[LDAPConnectivityValidation.from_api(x) for x in response.pop("validations", [])],
            **response,
        )

    def __str__(self):
        return self.request_id


LDAPConfigVerificationIdentifier: TypeAlias = LDAPConfigVerification | str
