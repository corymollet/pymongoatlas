from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING, TypeAlias
from urllib.parse import quote

from pydantic import Field, IPvAnyAddress

from pymongoatlas.entities.cloud_providers import CloudProviderName
from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.util import optional_isoformat

if TYPE_CHECKING:
    from pymongoatlas.entities.cluster import Cluster


class PeeringContainer(AtlasEntity):
    atlas_cidr_block: str
    id: str = Field(default="", exclude=True)
    provider_name: CloudProviderName
    provisioned: bool = Field(default=False, exclude=True)
    region_name: str
    vpc_id: str | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict):
        return cls(providerName=CloudProviderName(response.pop("providerName")), **response)

    def __str__(self):
        return self.id


PeeringContainerIdentifier: TypeAlias = PeeringContainer | str


class PeeringConnectionAWSErrorState(Enum):
    EXPIRED = "EXPIRED"
    INVALID_ARGUMENT = "INVALID_ARGUMENT"
    REJECTED = "REJECTED"
    NULL = None


class PeeringConnectionAWSStatus(Enum):
    AVAILABLE = "AVAILABLE"
    FAILED = "FAILED"
    FINALIZING = "FINALIZING"
    INITIATING = "INITIATING"
    PENDING_ACCEPTANCE = "PENDING_ACCEPTANCE"
    TERMINATING = "TERMINATING"
    NULL = None


class PeeringConnectionAWS(AtlasEntity):
    accepter_region_name: str | None = None
    aws_account_id: str
    container_id: str
    route_table_cidr_block: str
    vpc_id: str
    id: str = Field(default="", exclude=True)
    connection_id: str | None = Field(default=None, exclude=True)
    error_state: PeeringConnectionAWSErrorState | None = Field(default=None, exclude=True)
    status: PeeringConnectionAWSStatus | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "PeeringConnectionAWS":
        return cls(
            errorState=PeeringConnectionAWSErrorState(response.pop("errorStateName", None)),
            status=PeeringConnectionAWSStatus(response.pop("statusName", None)),
            **response,
        )

    def __str__(self):
        return self.id


class PeeringConnectionAzureStatus(Enum):
    ADDING_PEER = "ADDING_PEER"
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    DELETION_FAILED = "DELETION_FAILED"
    FAILED = "FAILED"


class PeeringConnectionAzure(AtlasEntity):
    azure_directory_id: str
    azure_subscription_id: str
    container_id: str
    resource_group_name: str
    vnet_name: str
    id: str = Field(default="", exclude=True)
    error_state: str | None = Field(default=None, exclude=True)
    status: PeeringConnectionAzureStatus | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "PeeringConnectionAzure":
        return cls(status=PeeringConnectionAzureStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id


class PeeringConnectionGCPStatus(Enum):
    ADDING_PEER = "ADDING_PEER"
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    FAILED = "FAILED"
    WAITING_FOR_USER = "WAITING_FOR_USER"


class PeeringConnectionGCP(AtlasEntity):
    container_id: str
    gcp_project_id: str
    network_name: str
    id: str = Field(default="", exclude=True)
    error_message: str | None = Field(default=None, exclude=True)
    status: PeeringConnectionGCPStatus | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "PeeringConnectionGCP":
        return cls(status=PeeringConnectionGCPStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id


PeeringConnection: TypeAlias = PeeringConnectionAWS | PeeringConnectionAzure | PeeringConnectionGCP
PeeringConnectionIdentifier: TypeAlias = PeeringConnectionAWS | PeeringConnectionAzure | PeeringConnectionGCP | str


class IPAccessListStatus(Enum):
    ACTIVE = "ACTIVE"
    FAILED = "FAILED"
    PENDING = "PENDING"


class IPAccessEntry(AtlasEntity):
    cidr_block: str | None = None
    ip_address: str | None = None
    comment: str | None = None
    aws_security_group: str | None = None
    delete_after_date: datetime | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(deleteAfterDate=optional_isoformat(response.pop("deleteAfterDate", None)), **response)

    def __str__(self):
        if self.aws_security_group is not None:
            return self.aws_security_group
        if self.cidr_block is not None:
            return quote(self.cidr_block)
        return self.ip_address


IPAccessEntryIdentifier: TypeAlias = IPAccessEntry | str


class ProjectIPAddressType(Enum):
    ALL = "all"
    INBOUND = "inbound"
    OUTBOUND = "outbound"


class ProjectIPAddress(AtlasEntity):
    services: dict  # TODO: Best way to model this? No way to get an exhaustive list of all `services`?

    def get_cluster_ip_addresses(
        self, cluster: "Cluster", address_type: ProjectIPAddressType = ProjectIPAddressType.ALL
    ) -> None | dict | list[str]:
        cluster_obj = next((x for x in self.services.get("clusters", []) if x["clusterName"] == cluster.name), None)
        if cluster_obj is None:
            return
        if address_type == ProjectIPAddressType.ALL:
            return cluster_obj
        if address_type == ProjectIPAddressType.INBOUND:
            return cluster_obj[ProjectIPAddressType.INBOUND.value]
        if address_type == ProjectIPAddressType.OUTBOUND:
            return cluster_obj[ProjectIPAddressType.OUTBOUND.value]


class ServerlessPrivateEndpointConnectionString(AtlasEntity):
    endpoint_id: str
    provider_name: CloudProviderName
    region: str

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessPrivateEndpointConnectionString":
        return cls(providerName=CloudProviderName(response.pop("providerName")), **response)


class ServerlessPrivateEndpoint(AtlasEntity):
    endpoints: list[ServerlessPrivateEndpointConnectionString]
    srv_connection_string: str
    type: str

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessPrivateEndpoint":
        return cls(endpoints=[ServerlessPrivateEndpointConnectionString.from_api(x) for x in response.pop("endpoints", [])], **response)


class PrivateEndpointServiceStatus(Enum):
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    FAILED = "FAILED"
    INITIATING = "INITIATING"
    WAITING_FOR_USER = "WAITING_FOR_USER"


class ServerlessPrivateEndpointStatus(Enum):
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    FAILED = "FAILED"
    RESERVATION_REQUESTED = "RESERVATION_REQUESTED"
    RESERVED = "RESERVED"


class PrivateEndpointProvider(Enum):
    AWS = "AWS"
    AZURE = "AZURE"
    GCP = "GCP"


class PrivateEndpointServiceAWS(AtlasEntity):
    id: str = ""
    endpoint_service_name: str | None = None
    error_message: str | None = None
    interface_endpoints: list[str] = Field(default_factory=list)
    provider_name: PrivateEndpointProvider = PrivateEndpointProvider.AWS
    region_name: str
    status: PrivateEndpointServiceStatus

    @classmethod
    def from_api(cls, response: dict) -> "PrivateEndpointServiceAWS":
        return cls(id=response.pop("_id"), status=PrivateEndpointServiceStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id


class PrivateEndpointServiceAzure(AtlasEntity):
    provider_name: PrivateEndpointProvider = PrivateEndpointProvider.AZURE
    error_message: str | None = None
    id: str = ""
    private_endpoints: list[str] = Field(default_factory=list)
    private_link_service_name: str
    private_link_service_resource_id: str
    region_name: str
    status: PrivateEndpointServiceStatus

    @classmethod
    def from_api(cls, response: dict) -> "PrivateEndpointServiceAzure":
        return cls(status=PrivateEndpointServiceStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id


class PrivateEndpointServiceGCP(AtlasEntity):
    provider_name: PrivateEndpointProvider = PrivateEndpointProvider.GCP
    endpoint_group_names: list[str]
    error_message: str | None = None
    id: str = ""
    region_name: str
    service_attachment_names: list[str]
    status: PrivateEndpointServiceStatus

    @classmethod
    def from_api(cls, response: dict) -> "PrivateEndpointServiceGCP":
        return cls(status=PrivateEndpointServiceStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id


PrivateEndpointService: TypeAlias = PrivateEndpointServiceAWS | PrivateEndpointServiceAzure | PrivateEndpointServiceGCP
PrivateEndpointServiceResource: TypeAlias = PrivateEndpointService | str


class PrivateEndpointCreationRequestAWS(AtlasEntity):
    id: str


class PrivateEndpointCreationRequestAzure(AtlasEntity):
    id: str
    private_endpoint_ip_address: IPvAnyAddress


class GCPConsumerForwardingRuleStatus(Enum):
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    FAILED = "FAILED"
    INITIATING = "INITIATING"


class GCPConsumerForwardingRule(AtlasEntity):
    endpoint_name: str
    ip_address: IPvAnyAddress
    status: GCPConsumerForwardingRuleStatus | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "GCPConsumerForwardingRule":
        return cls(status=GCPConsumerForwardingRuleStatus(response.pop("status")), **response)


class PrivateEndpointCreationRequestGCP(AtlasEntity):
    endpoint_group_name: str
    endpoints: list[GCPConsumerForwardingRule]
    gcp_project_id: str
    id: str


PrivateEndpointCreationRequest: TypeAlias = (
    PrivateEndpointCreationRequestAWS | PrivateEndpointCreationRequestAzure | PrivateEndpointCreationRequestGCP
)


class PrivateEndpointConnectionStatusAWS(Enum):
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    PENDING = "PENDING"
    PENDING_ACCEPTANCE = "PENDING_ACCEPTANCE"
    REJECTED = "REJECTED"


class PrivateEndpointAWS(AtlasEntity):
    connection_status: PrivateEndpointConnectionStatusAWS
    delete_requested: bool
    error_message: str | None = None
    interface_endpoint_id: str

    @classmethod
    def from_api(cls, response: dict) -> "PrivateEndpointAWS":
        return cls(connectionStatus=PrivateEndpointConnectionStatusAWS(response.pop("connectionStatus")), **response)

    def __str__(self):
        return self.interface_endpoint_id


class PrivateEndpointConnectionStatusAzure(Enum):
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    FAILED = "FAILED"
    INITIATING = "INITIATING"


class PrivateEndpointAzure(AtlasEntity):
    delete_requested: bool
    error_message: str | None = None
    private_endpoint_connection_name: str
    private_endpoint_ip_address: str
    private_endpoint_resource_id: str
    status: PrivateEndpointConnectionStatusAzure

    @classmethod
    def from_api(cls, response: dict) -> "PrivateEndpointAzure":
        return cls(status=PrivateEndpointConnectionStatusAzure(response.pop("status")), **response)

    def __str__(self):
        return quote(self.private_endpoint_resource_id)


class PrivateEndpointConnectionStatusGCP(Enum):
    AVAILABLE = "AVAILABLE"
    DELETING = "DELETING"
    FAILED = "FAILED"
    INITIATING = "INITIATING"
    VERIFIED = "VERIFIED"


class PrivateEndpointGCP(AtlasEntity):
    delete_requested: bool
    endpoint_group_name: str
    endpoints: list[GCPConsumerForwardingRule]
    error_message: str | None
    status: PrivateEndpointConnectionStatusGCP

    @classmethod
    def from_api(cls, response: dict) -> "PrivateEndpointGCP":
        return cls(
            endpoints=[GCPConsumerForwardingRule.from_api(x) for x in response.pop("endpoints", [])],
            status=PrivateEndpointConnectionStatusGCP(response.pop("status")),
            **response,
        )

    def __str__(self):
        return self.endpoint_group_name


PrivateEndpoint: TypeAlias = PrivateEndpointAWS | PrivateEndpointAzure | PrivateEndpointGCP


class ServerlessPrivateEndpointAWS(AtlasEntity):
    id: str = Field(default="", exclude=True)
    cloud_provider_endpoint_id: str
    comment: str | None = None
    endpoint_service_name: str | None = Field(default=None, exclude=True)
    error_message: str | None = Field(default=None, exclude=True)
    provider_name: PrivateEndpointProvider = PrivateEndpointProvider.AWS
    status: ServerlessPrivateEndpointStatus | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessPrivateEndpointAWS":
        return cls(id=response.pop("_id"), status=ServerlessPrivateEndpointStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id


class ServerlessPrivateEndpointAzure(AtlasEntity):
    id: str = Field(default="", exclude=True)
    cloud_provider_endpoint_id: str
    comment: str | None = None
    endpoint_service_name: str | None = Field(default=None, exclude=True)
    error_message: str | None = Field(default=None, exclude=True)
    provider_name: PrivateEndpointProvider = PrivateEndpointProvider.AZURE
    private_endpoint_ip_address: IPvAnyAddress
    private_link_service_resource_id: str | None = Field(default=None, exclude=True)
    status: ServerlessPrivateEndpointStatus | None = Field(default=None, exclude=True)

    @classmethod
    def from_api(cls, response: dict) -> "ServerlessPrivateEndpointAzure":
        return cls(id=response.pop("_id"), status=ServerlessPrivateEndpointStatus(response.pop("status")), **response)

    def __str__(self):
        return self.id
