import json
from datetime import datetime
from enum import Enum
from typing import Literal, Optional, TypeAlias, Union

from pydantic import Field, PositiveInt

from pymongoatlas.entities.backup import BackupFrequency, BackupRetentionUnit
from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.util import optional_isoformat


class SnapshotRetentionUnit(Enum):
    DAYS = "days"
    WEEKS = "weeks"
    MONTHS = "months"
    YEARS = "years"


class DatasetRetentionPolicy(AtlasEntity):
    last_modified_date: datetime | None = Field(default=None, exclude=True)
    units: BackupRetentionUnit
    value: PositiveInt

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[DatasetRetentionPolicy]":
        if not response:
            return
        return cls(
            lastModifiedDate=optional_isoformat(response.pop("lastModifiedDate", None)),
            units=BackupRetentionUnit(response.pop("units")),
            **response,
        )


class IngestionDestinationType(Enum):
    DLS = "DLS"


class PartitionField(AtlasEntity):
    field_name: str = Field(max_length=700)
    order: int = 0


class DLSIngestionDestination(AtlasEntity):
    metadata_provider: Literal["AWS"]
    metadata_region: str
    partition_fields: list[PartitionField] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict) -> "DLSIngestionDestination":
        return cls(partitionFields=[PartitionField.from_api(x) for x in response.pop("partitionFields", [])], **response)


IngestionDestination: TypeAlias = Union[DLSIngestionDestination]


class IngestionSourceType(Enum):
    PERIODIC_CPS = "PERIODIC_CPS"
    ON_DEMAND_CPS = "ON_DEMAND_CPS"


class OnDemandCPSIngestionSource(AtlasEntity):
    cluster_name: str
    collection_name: str
    database_name: str

    def to_api(self) -> str:
        payload = json.loads(super().to_api())
        payload["type"] = IngestionSourceType.ON_DEMAND_CPS.value
        return json.dumps(payload)


class DataLakePipelineState(Enum):
    ACTIVE = "ACTIVE"
    PAUSED = "PAUSED"


class PeriodicCPSIngestionSource(AtlasEntity):
    cluster_name: str
    collection_name: str
    database_name: str
    policy_item_id: str

    def to_api(self) -> str:
        payload = json.loads(super().to_api())
        payload["type"] = IngestionSourceType.PERIODIC_CPS.value
        return json.dumps(payload)


IngestionSource: TypeAlias = Union[OnDemandCPSIngestionSource, PeriodicCPSIngestionSource]


class FieldTransformation(AtlasEntity):
    field: str
    value: str = "EXCLUDE"


class DataLakePipeline(AtlasEntity):
    id: str = Field(default="", exclude=True)
    created_date: datetime | None = Field(default=None, exclude=True)
    dataset_retention_policy: DatasetRetentionPolicy
    last_updated_date: datetime | None = Field(default=None, exclude=True)
    name: str = Field(pattern=r'^[^/\\ "$]{1,64}$')
    sink: IngestionDestination
    source: IngestionSource
    state: DataLakePipelineState | None = Field(default=None, exclude=True)
    transformations: list[FieldTransformation] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict) -> "DataLakePipeline":
        return cls(
            createdDate=datetime.fromisoformat(response.pop("createdDate")),
            datasetRetentionPolicy=DatasetRetentionPolicy.from_api(response.pop("datasetRetentionPolicy", None)),
            lastUpdatedDate=datetime.fromisoformat(response.pop("lastUpdatedTime")),
            sink=IngestionDestination.from_api(response.pop("sink")),
            source=IngestionSource.from_api(response.pop("source")),
            state=DataLakePipelineState(response.pop("state")),
            transformations=[FieldTransformation.from_api(x) for x in response.pop("transformations", [])],
            **response,
        )

    def __str__(self):
        return self.name


DataLakePipelineIdentifier: TypeAlias = DataLakePipeline | str


class IngestionSchedule(AtlasEntity):
    frequency_interval: int = 1
    frequency_type: BackupFrequency
    id: str = ""
    retention_unit: SnapshotRetentionUnit
    retention_value: int

    @classmethod
    def from_api(cls, response: dict) -> "IngestionSchedule":
        return cls(
            frequencyType=BackupFrequency(response.pop("frequencyType").upper()),
            retentionUnit=SnapshotRetentionUnit(response.pop("retentionUnit")),
            **response,
        )


class PipelineRunPhase(Enum):
    EXPORT = "EXPORT"
    INGESTION = "INGESTION"
    SNAPSHOT = "SNAPSHOT"


class PipelineRunState(Enum):
    DATASET_DELETED = "DATASET_DELETED"
    DONE = "DONE"
    FAILED = "FAILED"
    IN_PROGRESS = "IN_PROGRESS"
    PENDING = "PENDING"


class PipelineRunStatistics(AtlasEntity):
    bytes_exported: int
    num_docs: int


class DataLakePipelineRun(AtlasEntity):
    id: str
    backup_frequency_type: BackupFrequency
    created_date: datetime
    dataset_name: str
    dataset_retention_policy: DatasetRetentionPolicy
    last_updated_date: datetime
    phase: PipelineRunPhase
    pipeline_id: str
    scheduled_deletion_date: datetime | None
    snapshot_id: str
    state: PipelineRunState
    stats: PipelineRunStatistics

    @classmethod
    def from_api(cls, response: dict) -> "DataLakePipelineRun":
        backup_frequency = response.pop("backupFrequencyType")
        if backup_frequency == "ON_DEMAND":
            backup_frequency = "ONDEMAND"
        return cls(
            backupFrequencyType=BackupFrequency(backup_frequency),
            createdDate=datetime.fromisoformat(response.pop("createdDate")),
            datasetRetentionPolicy=DatasetRetentionPolicy.from_api(response.pop("datasetRetentionPolicy")),
            lastUpdatedDate=datetime.fromisoformat(response.pop("lastUpdatedDate")),
            phase=PipelineRunPhase(response.pop("phase")),
            scheduledDeletionDate=optional_isoformat(response.pop("scheduledDeletionDate", None)),
            state=PipelineRunState(response.pop("state")),
            stats=PipelineRunStatistics.from_api(response.pop("stats")),
            **response,
        )

    def __str__(self):
        return self.id


DataLakePipelineRunIdentifier: TypeAlias = DataLakePipelineRun | str
