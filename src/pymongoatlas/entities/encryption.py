import json
from enum import Enum
from typing import Optional

from pydantic import Field

from pymongoatlas.entities.common import AtlasEntity


class AWSKMSConfiguration(AtlasEntity):
    access_key_id: str | None = None
    customer_master_key_id: str | None = None
    enabled: bool = False
    region: str | None = None
    role_id: str | None = None
    secret_access_key: str | None = None
    valid: bool = Field(default=False, exclude=True)


class AzureEnvironment(Enum):
    AZURE = "AZURE"
    AZURE_CHINA = "AZURE_CHINA"
    AZURE_GERMANY = "AZURE_GERMANY"


class AzureKeyVault(AtlasEntity):
    azure_environment: AzureEnvironment = AzureEnvironment.AZURE
    client_id: str | None = None
    enabled: bool = False
    key_identifier: str | None = None
    key_vault_name: str | None = None
    resource_group_name: str | None = None
    secret: str | None = None
    subscription_id: str | None = None
    tenant_id: str | None = None
    valid: bool = Field(default=False, exclude=True)

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[AzureKeyVault]":
        return cls(azureEnvironment=AzureEnvironment(response.pop("azureEnvironment")), **response)


class GoogleCloudKMS(AtlasEntity):
    enabled: bool = False
    key_version_resource_id: str | None = None
    service_account_key: dict = Field(default_factory=dict)
    valid: bool = Field(default=False, exclude=True)


class EncryptionAtRestConfig(AtlasEntity):
    aws_kms: AWSKMSConfiguration | None = None
    azure_key_vault: AzureKeyVault | None = None
    google_cloud_kms: GoogleCloudKMS | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            aws_kms=AWSKMSConfiguration.from_api(response.get("awsKms")),
            azure_key_vault=AzureKeyVault.from_api(response.get("azureKeyVault")),
            google_cloud_kms=GoogleCloudKMS.from_api(response.get("googleCloudKms")),
        )

    def to_api(self) -> str:
        return json.dumps(
            {
                **self._get_payload(self.aws_kms, "awsKms"),
                **self._get_payload(self.azure_key_vault, "azureKeyVault"),
                **self._get_payload(self.google_cloud_kms, "googleCloudKms"),
            }
        )

    @staticmethod
    def _get_payload(attribute, key):
        if attribute is not None:
            return {key: attribute.to_api()}
        else:
            return {}
