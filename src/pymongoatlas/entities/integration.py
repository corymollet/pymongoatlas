from enum import Enum

from pydantic import AnyUrl

from pymongoatlas.entities.common import AtlasEntity


class IntegrationType(Enum):
    DATADOG = "DATADOG"
    MICROSOFT_TEAMS = "MICROSOFT_TEAMS"
    NEW_RELIC = "NEW_RELIC"
    OPS_GENIE = "OPS_GENIE"
    PAGER_DUTY = "PAGER_DUTY"
    PROMETHEUS = "PROMETHEUS"
    SLACK = "SLACK"
    VICTOR_OPS = "VICTOR_OPS"
    WEBHOOK = "WEBHOOK"


class Integration(AtlasEntity):
    type: IntegrationType

    @classmethod
    def from_api(cls, response: dict):
        integration_type = IntegrationType(response["type"])
        integration = INTEGRATION_TYPE_TO_CLASS_MAPPING.get(integration_type)

        if integration is None:
            raise ValueError(f"Unknown integration type: {integration_type}")

        return integration.from_api(response)

    def to_api(self) -> str:
        raise NotImplementedError


class DatadogIntegration(Integration):
    type: IntegrationType = IntegrationType.DATADOG
    api_key: str
    region: str | None = None


class MicrosoftTeamsIntegration(Integration):
    type: IntegrationType = IntegrationType.MICROSOFT_TEAMS
    microsoft_teams_webhook_url: AnyUrl


class NewRelicIntegration(Integration):
    type: IntegrationType = IntegrationType.NEW_RELIC
    account_id: str
    license_key: str
    read_token: str
    write_token: str


class OpsGenieIntegration(Integration):
    type: IntegrationType = IntegrationType.OPS_GENIE
    api_key: str
    region: str | None = None


class PagerDutyIntegration(Integration):
    type: IntegrationType = IntegrationType.PAGER_DUTY
    region: str | None = None
    service_key: str


class PrometheusIntegration(Integration):
    type: IntegrationType = IntegrationType.PROMETHEUS
    enabled: bool
    listen_address: str
    scheme: str
    service_discovery: str
    tls_pem_path: str | None = None
    username: str


class SlackIntegration(Integration):
    type: IntegrationType = IntegrationType.SLACK
    api_token: str
    channel_name: str | None = None
    team_name: str | None = None


class VictorOpsIntegration(Integration):
    type: IntegrationType = IntegrationType.VICTOR_OPS
    api_key: str
    routing_key: str | None = None


class WebhookIntegration(Integration):
    type: IntegrationType = IntegrationType.WEBHOOK
    secret: str | None = None
    url: AnyUrl


INTEGRATION_TYPE_TO_CLASS_MAPPING = {
    IntegrationType.DATADOG: DatadogIntegration,
    IntegrationType.MICROSOFT_TEAMS: MicrosoftTeamsIntegration,
    IntegrationType.NEW_RELIC: NewRelicIntegration,
    IntegrationType.OPS_GENIE: OpsGenieIntegration,
    IntegrationType.PAGER_DUTY: PagerDutyIntegration,
    IntegrationType.PROMETHEUS: PrometheusIntegration,
    IntegrationType.SLACK: SlackIntegration,
    IntegrationType.VICTOR_OPS: VictorOpsIntegration,
    IntegrationType.WEBHOOK: WebhookIntegration,
}
