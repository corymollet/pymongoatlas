import json
from enum import Enum
from typing import Any

from pydantic import Field

from pymongoatlas.entities.common import AtlasEntity


class Namespace(AtlasEntity):
    namespace: str
    type: str = "collection"


class SlowQuery(AtlasEntity):
    line: dict
    namespace: str


class PerformanceAdvisorOpStat(AtlasEntity):
    ms: int
    n_returned: int
    n_scanned: int
    ts: int


class PerformanceAdvisorOperationView(AtlasEntity):
    predicates: list[dict]
    stats: PerformanceAdvisorOpStat

    @classmethod
    def from_api(cls, response: dict):
        return cls(stats=PerformanceAdvisorOpStat.from_api(response.pop("stats")), **response)


class PerformanceAdvisorIndex(AtlasEntity):
    avg_obj_size: float
    id: str
    impact: list[str]
    index: list[dict]
    namespace: str
    weight: float


class PerformanceAdvisorShape(AtlasEntity):
    avg_ms: int
    count: int
    id: str
    inefficiency_score: int
    namespace: str
    operations: list[PerformanceAdvisorOperationView]

    @classmethod
    def from_api(cls, response: dict):
        return cls(operations=[PerformanceAdvisorOperationView.from_api(x) for x in response.pop("operations")], **response)


class SuggestedIndex(AtlasEntity):
    shapes: list[PerformanceAdvisorShape]
    suggested_indexes: list[PerformanceAdvisorIndex]

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            shapes=[PerformanceAdvisorShape.from_api(x) for x in response["shapes"]],
            suggestedIndexes=[PerformanceAdvisorIndex.from_api(x) for x in response["suggestedIndexes"]],
        )


class Alternate(Enum):
    NON_IGNORABLE = "non-ignorable"
    SHIFTED = "shifted"


class CaseFirst(Enum):
    LOWER = "lower"
    UPPER = "upper"
    OFF = "off"


class Locale(Enum):
    AF = "af"
    SQ = "sq"
    AM = "am"
    AR = "ar"
    HY = "hy"
    AS = "as"
    AZ = "az"
    BN = "bn"
    BE = "be"
    BS = "bs"
    BS_CYRL = "bs_Cyrl"
    BG = "bg"
    MY = "my"
    CA = "ca"
    CHR = "chr"
    ZH = "zh"
    ZH_HANT = "zh_Hant"
    HR = "hr"
    CS = "cs"
    DA = "da"
    NL = "nl"
    DZ = "dz"
    EN = "en"
    EN_US = "en_US"
    EN_US_POSIX = "en_US_POSIX"
    EO = "eo"
    ET = "et"
    EE = "ee"
    FO = "fo"
    FIL = "fil"
    FI_FI = "fi_FI"
    FR = "fr"
    FR_CA = "fr_CA"
    GL = "gl"
    KA = "ka"
    DE = "de"
    DE_AT = "de_AT"
    EL = "el"
    GU = "gu"
    HA = "ha"
    HAW = "haw"
    HE = "he"
    HI = "hi"
    HU = "hu"
    IS = "is"
    IG = "ig"
    SMN = "smn"
    ID = "id"
    GA = "ga"
    IT = "it"
    JA = "ja"
    KL = "kl"
    KN = "kn"
    KK = "kk"
    KM = "km"
    KOK = "kok"
    KO = "ko"
    KY = "ky"
    LK = "lk"
    LO = "lo"
    LV = "lv"
    LI = "li"
    LT = "lt"
    DSB = "dsb"
    LB = "lb"
    MK = "mk"
    MS = "ms"
    ML = "ml"
    MT = "mt"
    MR = "mr"
    MN = "mn"
    NE = "ne"
    SE = "se"
    NB = "nb"
    NN = "nn"
    OR = "or"
    OM = "om"
    PS = "ps"
    FA = "fa"
    FA_AF = "fa_AF"
    PL = "pl"
    PT = "pt"
    PA = "pa"
    RO = "ro"
    RU = "ru"
    SR = "sr"
    SR_LATN = "sr_LATN"
    SI = "si"
    SK = "sk"
    SL = "sl"
    ES = "es"
    SW = "sw"
    SV = "sv"
    TA = "ta"
    TE = "te"
    TH = "th"
    BO = "bo"
    TO = "to"
    TR = "tr"
    UK = "uk"
    HSB = "hsb"
    UR = "ur"
    UG = "ug"
    VI = "vi"
    WAE = "wae"
    CY = "cy"
    YI = "yi"
    YO = "yo"
    ZU = "zu"
    SIMPLE = "simple"


class MaxVariable(Enum):
    PUNCT = "punct"
    SPACE = "space"


class Collation(AtlasEntity):
    """
    https://www.mongodb.com/docs/manual/reference/collation/
    """

    alternate: Alternate = Field(default=Alternate.NON_IGNORABLE)
    backwards: bool = Field(default=False)
    case_first: CaseFirst = Field(default=CaseFirst.OFF)
    case_level: bool = Field(default=False)
    locale: Locale = Field(default=Locale.EN_US)
    max_variable: MaxVariable = Field(default=MaxVariable.PUNCT, exclude=True)
    normalization: bool = Field(default=False)
    numeric_ordering: bool = Field(default=False)
    strength: int = Field(ge=1, le=5, default=3)

    def to_api(self) -> str:
        payload = json.loads(super().to_api())
        if self.alternate == Alternate.SHIFTED:
            payload["maxVariable"] = self.max_variable.value
        return json.dumps(payload)


class IndexOptions(AtlasEntity):
    twod_sphere_index_version: int | None = Field(default=None)
    background: bool = Field(default=False)
    bits: int | None = Field(default=None)
    columnstore_projection: dict[str, bool] = Field(default_factory=dict)
    default_language: str | None = Field(default=None)
    expire_after_seconds: int | None = Field(default=None)
    hidden: bool = Field(default=False)
    language_override: str | None = Field(default=None)
    max: int | None = Field(default=None)
    min: int | None = Field(default=None)
    name: str
    partial_filter_expression: dict[str, Any] = Field(default_factory=dict)
    sparse: bool = Field(default=False)
    storage_engine: dict[str, dict[str, str]] = Field(default_factory=dict)
    text_index_version: int | None = Field(default=None)
    unique: bool = Field(default=False)
    weights: dict[str, int] = Field(default_factory=dict)


class RollingIndex(AtlasEntity):
    collation: Collation | None = Field(default=None, exclude=True)
    collection: str = Field(description="Name of the collection.")
    db: str = Field(description="Name of the db.")
    keys: list[dict] = Field(
        description="Key-value pair that sets the parameter to index as the key and the type of index as its value. To create a multikey index, list each parameter in its own object within this array. See https://docs.mongodb.com/manual/indexes/#index-types for details."
    )
    options: IndexOptions | None = None
