import json
from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING, TypeAlias

from pydantic import Field, field_serializer

from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.util import optional_isoformat

if TYPE_CHECKING:
    from pymongoatlas.entities.organization import OrganizationIdentifier
    from pymongoatlas.entities.projects import ProjectIdentifier


class AWSIAMType(Enum):
    NONE = "NONE"
    USER = "USER"
    ROLE = "ROLE"


class AuthDatabaseName(Enum):
    ADMIN = "admin"
    EXTERNAL = "$external"
    LOCAL = "local"


class DatabaseUserScopeType(Enum):
    CLUSTER = "CLUSTER"
    DATA_LAKE = "DATA_LAKE"
    STREAM = "STREAM"


class DatabaseUserScope(AtlasEntity):
    name: str
    scope_type: DatabaseUserScopeType

    @classmethod
    def from_api(cls, response: dict):
        return cls(scopeType=DatabaseUserScopeType(response.pop("type")), **response)


class LDAPAuthType(Enum):
    NONE = "NONE"
    GROUP = "GROUP"
    USER = "USER"


class OIDCAuthType(Enum):
    NONE = "NONE"
    IDP_GROUP = "IDP_GROUP"


class X509Type(Enum):
    CUSTOMER = "CUSTOMER"
    MANAGED = "MANAGED"
    NONE = "NONE"


class StandardRoleName(Enum):
    ATLAS_ADMIN = "atlasAdmin"
    BACKUP = "backup"
    CLUSTER_MONITOR = "clusterMonitor"
    DATABASE_MONITOR = "databasemonitor"
    DB_ADMIN = "dbAdmin"
    DB_ADMIN_ANY_DATABASE = "dbAdminAnyDatabase"
    ENABLE_SHARDING = "enableSharding"
    READ = "read"
    READ_ANY_DATABASE = "readAnyDatabase"
    READ_WRITE = "readWrite"
    READ_WRITE_ANY_DATABASE = "readWriteAnyDatabase"


class AtlasOrgRole(Enum):
    ORG_OWNER = "ORG_OWNER"
    ORG_MEMBER = "ORG_MEMBER"
    ORG_GROUP_CREATOR = "ORG_GROUP_CREATOR"
    ORG_BILLING_ADMIN = "ORG_BILLING_ADMIN"
    ORG_READ_ONLY = "ORG_READ_ONLY"


class AtlasProjectRole(Enum):
    GROUP_CHARTS_ADMIN = "GROUP_CHARTS_ADMIN"
    GROUP_CLUSTER_MANAGER = "GROUP_CLUSTER_MANAGER"
    GROUP_DATA_ACCESS_ADMIN = "GROUP_DATA_ACCESS_ADMIN"
    GROUP_DATA_ACCESS_READ_ONLY = "GROUP_DATA_ACCESS_READ_ONLY"
    GROUP_DATA_ACCESS_READ_WRITE = "GROUP_DATA_ACCESS_READ_WRITE"
    GROUP_OWNER = "GROUP_OWNER"
    GROUP_READ_ONLY = "GROUP_READ_ONLY"
    GROUP_SEARCH_INDEX_EDITOR = "GROUP_SEARCH_INDEX_EDITOR"
    GROUP_STREAM_PROCESSING_OWNER = "GROUP_STREAM_PROCESSING_OWNER"


def get_atlas_role(role_name: str) -> AtlasOrgRole | AtlasProjectRole:
    try:
        return AtlasOrgRole(role_name)
    except ValueError:
        return AtlasProjectRole(role_name)


class RoleAssignment(AtlasEntity):
    group_id: str | None = None
    org_id: str | None = None
    role: AtlasOrgRole | AtlasProjectRole

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            role=get_atlas_role(response.get("role", response.get("roleName"))),
            groupId=response.get("groupId"),
            orgId=response.get("orgId"),
        )


class DatabaseUserRole(AtlasEntity):
    collection_name: str | None = None
    database_name: str
    role_name: StandardRoleName | str
    is_custom: bool = Field(default=False, exclude=True)

    @classmethod
    def from_api(cls, response: dict):
        role_name = response.pop("roleName")
        role = cls(collectionName=response.pop("collectionName", None), roleName=role_name, **response)
        try:
            role.role_name = StandardRoleName(role_name)
        except ValueError:
            role.is_custom = True
        return role


class GroupRole(AtlasEntity):
    group_id: str
    group_roles: list[AtlasProjectRole] = Field(default_factory=list)

    @classmethod
    def from_api(cls, response: dict):
        return cls(groupRoles=[AtlasProjectRole(x) for x in response.pop("groupRoles", [])], **response)


class ComponentLabel(AtlasEntity):
    key: str
    value: str


class DatabaseUser(AtlasEntity):
    aws_iam_type: AWSIAMType = AWSIAMType.NONE
    database_name: AuthDatabaseName = AuthDatabaseName.ADMIN
    delete_after_date: datetime | None = None
    labels: list[ComponentLabel] = Field(default_factory=list)
    ldap_auth_type: LDAPAuthType = LDAPAuthType.NONE
    oidc_auth_type: OIDCAuthType = OIDCAuthType.NONE
    roles: list[DatabaseUserRole] = Field(default_factory=list)
    username: str
    scopes: list[DatabaseUserScope] = Field(
        default_factory=list,
    )
    x509_type: X509Type = X509Type.NONE

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            awsIamType=AWSIAMType(response.pop("awsIAMType")),
            databaseName=AuthDatabaseName(response.pop("databaseName")),
            deleteAfterDate=optional_isoformat(response.pop("deleteAfterDate", None)),
            labels=[ComponentLabel.from_api(x) for x in response.pop("labels", [])],
            ldapAuthType=LDAPAuthType(response.pop("ldapAuthType")),
            oidcAuthType=OIDCAuthType(response.pop("oidcAuthType")),
            roles=[DatabaseUserRole.from_api(x) for x in response.pop("roles")],
            scopes=[DatabaseUserScope.from_api(x) for x in response.pop("scopes", [])],
            x509Type=X509Type(response.pop("x509Type")),
            **response,
        )

    @field_serializer("username")
    def ser_username(self, username: str) -> str:
        if self.aws_iam_type != AWSIAMType.NONE:
            # TODO return "arn:xxx"
            raise NotImplementedError
        if self.x509_type != X509Type.NONE:
            # TODO return "rfc2253 distinguished name"
            raise NotImplementedError
        if self.ldap_auth_type != LDAPAuthType.NONE:
            # TODO return "rfc2253 distinguished name"
            raise NotImplementedError
        if self.oidc_auth_type != OIDCAuthType.NONE:
            # TODO return "Atlas OIDC IdP ID (found in federation settings), followed by a '/', followed by the IdP group name"
            raise NotImplementedError
        return f"SCRAM-SHA: {self.username}"

    def __str__(self):
        return self.username


DatabaseUserIdentifier: TypeAlias = DatabaseUser | str


class DatabasePrivilegeAction(Enum):
    FIND = "FIND"
    INSERT = "INSERT"
    REMOVE = "REMOVE"
    UPDATE = "UPDATE"
    BYPASS_DOCUMENT_VALIDATION = "BYPASS_DOCUMENT_VALIDATION"
    USE_UUID = "USE_UUID"
    KILL_OP = "KILL_OP"
    CREATE_COLLECTION = "CREATE_COLLECTION"
    CREATE_INDEX = "CREATE_INDEX"
    DROP_COLLECTION = "DROP_COLLECTION"
    ENABLE_PROFILER = "ENABLE_PROFILER"
    CHANGE_STREAM = "CHANGE_STREAM"
    COLL_MOD = "COLL_MOD"
    COMPACT = "COMPACT"
    CONVERT_TO_CAPPED = "CONVERT_TO_CAPPED"
    DROP_DATABASE = "DROP_DATABASE"
    DROP_INDEX = "DROP_INDEX"
    RE_INDEX = "RE_INDEX"
    RENAME_COLLECTION_SAME_DB = "RENAME_COLLECTION_SAME_DB"
    SET_USER_WRITE_BLOCK = "SET_USER_WRITE_BLOCK"
    BYPASS_USER_WRITE_BLOCK = "BYPASS_USER_WRITE_BLOCK"
    LIST_SESSIONS = "LIST_SESSIONS"
    KILL_ANY_SESSION = "KILL_ANY_SESSION"
    COLL_STATS = "COLL_STATS"
    CONN_POOL_STATS = "CONN_POOL_STATS"
    DB_HASH = "DB_HASH"
    DB_STATS = "DB_STATS"
    GET_CMD_LINE_OPTS = "GET_CMD_LINE_OPTS"
    GET_LOG = "GET_LOG"
    GET_PARAMETER = "GET_PARAMETER"
    GET_SHARD_MAP = "GET_SHARD_MAP"
    HOST_INFO = "HOST_INFO"
    IN_PROG = "IN_PROG"
    LIST_DATABASES = "LIST_DATABASES"
    LIST_COLLECTIONS = "LIST_COLLECTIONS"
    LIST_INDEXES = "LIST_INDEXES"
    LIST_SHARDS = "LIST_SHARDS"
    NET_STAT = "NET_STAT"
    REPL_SET_GET_CONFIG = "REPL_SET_GET_CONFIG"
    REPL_SET_GET_STATUS = "REPL_SET_GET_STATUS"
    SERVER_STATUS = "SERVER_STATUS"
    VALIDATE = "VALIDATE"
    SHARDING_STATE = "SHARDING_STATE"
    TOP = "TOP"
    SQL_GET_SCHEMA = "SQL_GET_SCHEMA"
    SQL_SET_SCHEMA = "SQL_SET_SCHEMA"
    VIEW_ALL_HISTORY = "VIEW_ALL_HISTORY"
    OUT_TO_S3 = "OUT_TO_S3"
    STORAGE_GET_CONFIG = "STORAGE_GET_CONFIG"
    STORAGE_SET_CONFIG = "STORAGE_SET_CONFIG"
    FLUSH_ROUTER_CONFIG = "FLUSH_ROUTER_CONFIG"


class PermittedNamespace(AtlasEntity):
    cluster: bool = False
    collection: str
    db: str


class InheritedRole(AtlasEntity):
    db: str
    role: StandardRoleName

    @classmethod
    def from_api(cls, response: dict):
        return cls(role=StandardRoleName(response.pop("role")), **response)


class CustomRole(AtlasEntity):
    actions: list[dict[DatabasePrivilegeAction, list[PermittedNamespace]]] = Field(default_factory=list)
    name: str
    inherited_roles: list[InheritedRole] = Field(default_factory=list)

    @classmethod
    def from_api(cls, response: dict):
        role = cls(
            name=response.pop("roleName"),
            inheritedRoles=[InheritedRole.from_api(x) for x in response.pop("inheritedRoles", [])],
            actions=[],
        )
        actions = []
        for action in response.pop("actions", []):
            actions.append({DatabasePrivilegeAction(action["action"]): [PermittedNamespace.from_api(x) for x in action["resources"]]})
        role.actions = actions
        return role

    def to_api(self) -> str:
        actions = []
        for action in self.actions:
            for item in action.items():
                actions.append({item[0].value: [x.model_dump(exclude_none=True) for x in item[1]]})
        return json.dumps(
            {"actions": actions, "name": self.name, "inheritedRoles": [x.model_dump(exclude_none=True) for x in self.inherited_roles]}
        )

    def __str__(self):
        return self.name


CustomRoleIdentifier: TypeAlias = CustomRole | str


class StandardRole(AtlasEntity):
    db: str
    role: StandardRoleName
    collection: str | None = None


class AtlasUser(AtlasEntity):
    country: str = Field(
        max_length=2,
    )
    created_at: datetime | None = Field(default=None, exclude=True)
    first_name: str
    id: str | None = Field(default=None, exclude=True)
    last_auth: datetime | None = Field(default=None, exclude=True)
    last_name: str
    mobile_number: str | None = None
    password: str | None = Field(
        default=None,
        min_length=8,
    )
    roles: list[RoleAssignment] = Field(
        default_factory=list,
    )
    team_ids: list[str] = Field(default_factory=list, exclude=True)
    username: str = Field(exclude=True)

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            createdAt=optional_isoformat(response.pop("createdAt", None)),
            lastAuth=optional_isoformat(response.pop("lastAuth", None)),
            roles=[RoleAssignment.from_api(x) for x in response.pop("roles", [])],
            **response,
        )

    def get_project_roles(self, project: "ProjectIdentifier") -> list[AtlasProjectRole]:
        return [
            role_assignment.role
            for role_assignment in self.roles
            if isinstance(role_assignment.role, AtlasProjectRole) and role_assignment.group_id == project
        ]

    def get_org_roles(self, org: "OrganizationIdentifier") -> list[AtlasOrgRole]:
        return [
            role_assignment.role
            for role_assignment in self.roles
            if isinstance(role_assignment.role, AtlasOrgRole) and role_assignment.org_id == org
        ]

    def __str__(self):
        return self.id


AtlasUserTypeAlias: TypeAlias = AtlasUser | str


class ApiKey(AtlasEntity):
    desc: str
    id: str | None = Field(default=None, exclude=True)
    private_key: str | None = Field(
        default=None,
        exclude=True,
    )
    public_key: str | None = Field(default=None, exclude=True)
    roles: list[RoleAssignment] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict):
        return cls(roles=[RoleAssignment.from_api(x) for x in response.pop("roles", [])], **response)

    def get_roles_by_role_type(self, role_type: type[AtlasOrgRole] | type[AtlasProjectRole]):
        return [role.role for role in self.roles if role.role == role_type]

    def to_api(self, role_type: type[AtlasOrgRole] | type[AtlasProjectRole] | None = None) -> str:
        payload = {"desc": self.desc}
        if role_type is not None:
            payload["roles"] = [x.value for x in self.get_roles_by_role_type(role_type)]
        else:
            payload["roles"] = [x.role.value for x in self.roles]
        return json.dumps(payload)

    def __str__(self):
        return self.id


ApiKeyIdentifier: TypeAlias = ApiKey | str


class UserAccessList(AtlasEntity):
    cidr_block: str | None = None
    count: int = 0
    created: datetime | None = None
    ip_address: str | None = None
    last_used: datetime | None = None
    last_used_address: str | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            created=optional_isoformat(response.pop("created", None)),
            lastUsed=optional_isoformat(response.pop("lastUsed", None)),
            **response,
        )


class UserCert(AtlasEntity):
    id: str
    created_at: datetime
    group_id: str
    not_after: datetime
    subject: str
