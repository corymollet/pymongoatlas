from pydantic import BaseModel, ConfigDict, Field
from pydantic.alias_generators import to_camel

TAG_PATTERN = r"[a-zA-Z0-9\s;@_\-.+]{1,255}"


class AtlasEntity(BaseModel):
    model_config = ConfigDict(alias_generator=lambda field_name: to_camel(field_name))

    @classmethod
    def from_api(cls, response: dict | None):
        if not response:
            return None
        return cls(**response)

    def to_api(self) -> str:
        return self.model_dump_json(exclude_none=True, by_alias=True)


class Tag(AtlasEntity):
    """
    <https://www.mongodb.com/docs/atlas/tags/>_
    """

    key: str = Field(pattern=TAG_PATTERN)
    value: str = Field(pattern=TAG_PATTERN)
