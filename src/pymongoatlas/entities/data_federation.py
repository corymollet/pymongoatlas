import json
from datetime import datetime
from enum import Enum
from typing import Literal, Optional, TypeAlias

from pydantic import AnyUrl, Field, NonNegativeInt

from pymongoatlas.entities.cloud_providers import AWSRegion, AzureRegion, DataProcessingRegion
from pymongoatlas.entities.common import AtlasEntity, Tag


class DataLakeAWSCloudProviderConfig(AtlasEntity):
    external_id: str | None = None
    iam_assumed_role_arn: str | None = Field(default=None, ge=20, le=2048, exclude=True)
    iam_user_arn: str | None = Field(default=None, exclude=True)
    role_id: str

    def to_api(self, test_s3_bucket: str) -> dict:
        return {"roleId": self.role_id, "testS3Bucket": test_s3_bucket}


class DataLakeCloudProvider(AtlasEntity):
    aws: DataLakeAWSCloudProviderConfig

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[DataLakeCloudProvider]":
        if not response:
            return
        return cls(aws=DataLakeAWSCloudProviderConfig.from_api(response["aws"]))

    def to_api(self, test_s3_bucket: str):
        """
        Create a payload to creating an AWS cloud provider config.

        :param test_s3_bucket: Name of the S3 data bucket that the provided role ID is authorized to access.
        """
        return {"aws": self.aws.to_api(test_s3_bucket=test_s3_bucket)}


class PrivateEndpointHostname(AtlasEntity):
    hostname: str
    private_endpoint: str


class FederatedInstanceState(Enum):
    ACTIVE = "ACTIVE"
    DELETED = "DELETED"
    UNVERIFIED = "UNVERIFIED"


class DataLakeAPIView(AtlasEntity):
    name: str
    pipeline: str
    source: str


class DefaultFileFormat(Enum):
    AVRO = ".avro"
    AVRO_BZ2 = ".avro.bz2"
    AVRO_GZ = ".avro.gz"
    BSON = ".bson"
    BSON_BZ2 = ".bson.bz2"
    BSON_GZ = ".bson.gz"
    BSONX = ".bsonx"
    CSV = ".csv"
    CSV_BZ2 = ".csv.bz2"
    CSV_GZ = ".csv.gz"
    JSON = ".json"
    JSON_BZ2 = ".json.bz2"
    JSON_GZ = ".json.gz"
    ORC = ".orc"
    PARQUET = ".parquet"
    TSV = ".tsv"
    TSV_BZ2 = ".tsv.bz2"
    TSV_GZ = ".tsv.gz"


class DataSourceSettings(AtlasEntity):
    allow_insecure: bool = False
    collection: str | None = None
    collection_regex: str | None = None
    database: str | None = None
    database_regex: str | None = None
    dataset_name: str
    dataset_prefix: str | None = None
    default_format: DefaultFileFormat = DefaultFileFormat.JSON
    path: str = "/"
    provenance_field_name: str = ""
    store_name: str
    trim_level: NonNegativeInt = 0
    urls: list[AnyUrl] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict) -> "DataSourceSettings":
        return cls(defaultFormat=DefaultFileFormat(response.pop("defaultFormat")), **response)


class DataLakeDatabaseCollection(AtlasEntity):
    data_sources: list[DataSourceSettings] = Field(
        default_factory=list,
    )
    name: str

    @classmethod
    def from_api(cls, response: dict) -> "DataLakeDatabaseCollection":
        return cls(dataSources=[DataSourceSettings.from_api(x) for x in response.pop("dataSources", [])], **response)


class DataLakeDatabaseInstance(AtlasEntity):
    collections: list[DataLakeDatabaseCollection] = Field(
        default_factory=list,
    )
    max_wildcard_collections: int = Field(
        ge=1,
        le=1000,
        default=100,
    )
    name: str
    views: list[DataLakeAPIView] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict) -> "DataLakeDatabaseInstance":
        return cls(
            collections=[DataLakeDatabaseCollection.from_api(x) for x in response.pop("collections", [])],
            views=[DataLakeAPIView.from_api(x) for x in response.pop("views", [])],
            **response,
        )


class ReadConcernLevel(Enum):
    LOCAL = "LOCAL"
    MAJORITY = "MAJORITY"
    LINEARIZABLE = "LINEARIZABLE"
    SNAPSHOT = "SNAPSHOT"
    AVAILABLE = "AVAILABLE"


class ReadPreferenceMode(Enum):
    PRIMARY = "primary"
    PRIMARY_PREFERRED = "primaryPreferred"
    SECONDARY = "secondary"
    SECONDARY_PREFERRED = "secondaryPreferred"
    NEAREST = "nearest"


class DataStoreProvider(Enum):
    ATLAS = "atlas"
    AWS = "dls:aws"
    AZURE = "dls:azure"
    HTTP = "http"
    S3 = "s3"


class ReadConcern(AtlasEntity):
    """
    https://www.mongodb.com/docs/manual/reference/read-concern/#read-concern-levels
    """

    level: ReadConcernLevel

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[ReadConcern]":
        if not response:
            return
        return cls(level=ReadConcernLevel(response["level"]))


class ReadPreference(AtlasEntity):
    """
    https://docs.mongodb.com/manual/core/read-preference/#read-preference-modes
    """

    max_staleness_seconds: int | None = None
    mode: ReadPreferenceMode = ReadPreferenceMode.NEAREST
    tag_sets: list[Tag] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[ReadPreference]":
        if not response:
            return
        return cls(
            mode=ReadPreferenceMode(response.pop("mode")), tagSets=[Tag.from_api(x) for x in response.pop("tagSets", [])], **response
        )


class AtlasDataLakeStore(AtlasEntity):
    name: str
    cluster_name: str
    project_id: str
    read_concern: ReadConcern
    read_preference: ReadPreference

    @classmethod
    def from_api(cls, response: dict) -> "AtlasDataLakeStore":
        return cls(
            read_concern=ReadConcern.from_api(response.pop("readConcern", None)),
            read_preference=ReadPreference.from_api(response.pop("readPreference", None)),
            **response,
        )


class AWSDataLakeStore(AtlasEntity):
    name: str
    region: AWSRegion

    @classmethod
    def from_api(cls, response: dict) -> "AWSDataLakeStore":
        return cls(region=AWSRegion(response.pop("region")), **response)


class AzureDataLakeStore(AtlasEntity):
    name: str
    region: AzureRegion

    @classmethod
    def from_api(cls, response: dict) -> "AzureDataLakeStore":
        return cls(region=AzureRegion(response.pop("region")), **response)


class HTTPDataLakeStore(AtlasEntity):
    name: str
    allow_insecure: bool = False
    default_format: DefaultFileFormat
    urls: list[AnyUrl] = Field(default_factory=list)

    @classmethod
    def from_api(cls, response: dict) -> "HTTPDataLakeStore":
        return cls(defaultFormat=DefaultFileFormat(response.pop("defaultFormat")), **response)


class S3StorageClass(Enum):
    STANDARD = "STANDARD"
    INTELLIGENT_TIERING = "INTELLIGENT_TIERING"
    STANDARD_IA = "STANDARD_IA"


class S3DataLakeStore(AtlasEntity):
    name: str
    additional_storage_classes: list[S3StorageClass] = Field(
        default_factory=list,
    )
    bucket: str
    delimiter: str = "/"
    include_tags: bool = False
    prefix: str | None = None
    public: bool = False
    region: AWSRegion

    @classmethod
    def from_api(cls, response: dict) -> "S3DataLakeStore":
        return cls(
            additionalStorageClasses=[S3StorageClass(x) for x in response.pop("additionalStorageClasses", [])],
            region=AWSRegion(response.pop("region")),
            **response,
        )


DataLakeStore: TypeAlias = AtlasDataLakeStore | AWSDataLakeStore | AzureDataLakeStore | HTTPDataLakeStore | S3DataLakeStore


def process_data_lake_stores(stores: list[dict]) -> list[DataLakeStore]:
    processed_stores = []
    process_map = {
        DataStoreProvider.ATLAS: AtlasDataLakeStore,
        DataStoreProvider.AWS: AWSDataLakeStore,
        DataStoreProvider.AZURE: AzureDataLakeStore,
        DataStoreProvider.HTTP: HTTPDataLakeStore,
        DataStoreProvider.S3: S3DataLakeStore,
    }
    for store in stores:
        provider = DataStoreProvider(store["provider"])
        processed_stores.append(process_map[provider].from_api(store))
    return processed_stores


class DataLakeStorage(AtlasEntity):
    databases: list[DataLakeDatabaseInstance] = Field(default_factory=list)
    stores: list[DataLakeStore] = Field(default_factory=list)

    @classmethod
    def from_api(cls, response: dict) -> "DataLakeStorage":
        return cls(
            databases=[DataLakeDatabaseInstance.from_api(x) for x in response.get("databases", [])],
            stores=process_data_lake_stores(response.get("stores", [])),
        )


class AtlasDataLakeAWSRegion(Enum):
    SYDNEY_AUS = "SYDNEY_AUS"
    MUMBAI_IND = "MUMBAI_IND"
    FRANKFURT_DEU = "FRANKFURT_DEU"
    DUBLIN_IRL = "DUBLIN_IRL"
    LONDON_GBR = "LONDON_GBR"
    VIRGINIA_USA = "VIRGINIA_USA"
    OREGON_USA = "OREGON_USA"
    SAOPAULO_BRA = "SAOPAULO_BRA"
    SINGAPORE_SGP = "SINGAPORE_SGP"


class AtlasDataLakeAzureRegion(Enum):
    VIRGINIA_USA = "VIRGINIA_USA"
    CALIFORNIA_USA = "CALIFORNIA_USA"
    SINGAPORE_SGP = "SINGAPORE_SGP"
    DUBLIN_IRL = "DUBLIN_IRL"
    AMSTERDAM_NLD = "AMSTERDAM_NLD"
    SYDNEY_AUS = "SYDNEY_AUS"


AtlasDataLakeRegionMap = {
    AWSRegion.AP_SOUTHEAST_2: AtlasDataLakeAWSRegion.SYDNEY_AUS,
    AWSRegion.AP_SOUTH_1: AtlasDataLakeAWSRegion.MUMBAI_IND,
    AWSRegion.EU_CENTRAL_1: AtlasDataLakeAWSRegion.FRANKFURT_DEU,
    AWSRegion.EU_WEST_1: AtlasDataLakeAWSRegion.DUBLIN_IRL,
    AWSRegion.EU_WEST_2: AtlasDataLakeAWSRegion.LONDON_GBR,
    AWSRegion.US_EAST_1: AtlasDataLakeAWSRegion.VIRGINIA_USA,
    AWSRegion.US_WEST_2: AtlasDataLakeAWSRegion.OREGON_USA,
    AWSRegion.SA_EAST_1: AtlasDataLakeAWSRegion.SAOPAULO_BRA,
    AWSRegion.AP_SOUTHEAST_1: AtlasDataLakeAWSRegion.SINGAPORE_SGP,
    AzureRegion.US_EAST: AtlasDataLakeAzureRegion.VIRGINIA_USA,
    AzureRegion.US_WEST: AtlasDataLakeAzureRegion.CALIFORNIA_USA,
    AzureRegion.ASIA_SOUTH_EAST: AtlasDataLakeAzureRegion.SINGAPORE_SGP,
    AzureRegion.EUROPE_NORTH: AtlasDataLakeAzureRegion.DUBLIN_IRL,
    AzureRegion.EUROPE_WEST: AtlasDataLakeAzureRegion.AMSTERDAM_NLD,
    AzureRegion.AUSTRALIA_EAST: AtlasDataLakeAzureRegion.SYDNEY_AUS,
}


class FederatedDatabaseInstance(AtlasEntity):
    cloud_provider_config: DataLakeCloudProvider | None = None
    data_processing_region: DataProcessingRegion
    hostnames: list[str] = Field(default_factory=list)
    name: str
    private_endpoint_hostnames: list[PrivateEndpointHostname] = Field(default_factory=list)
    state: FederatedInstanceState | None = None
    storage: DataLakeStorage

    @classmethod
    def from_api(cls, response: dict) -> "FederatedDatabaseInstance":
        return cls(
            cloudProviderConfig=DataLakeCloudProvider.from_api(response.pop("cloudProviderConfig", None)),
            dataProcessingRegion=DataProcessingRegion.from_api(response.pop("dataProcessingRegion")),
            privateEndpointHostnames=[PrivateEndpointHostname.from_api(x) for x in response.pop("privateEndpointHostnames", [])],
            state=FederatedInstanceState(response.pop("state")),
            storage=DataLakeStorage.from_api(response.pop("storage")),
            **response,
        )

    def to_api(self, test_s3_bucket: str | None = None) -> str:
        data_processing_region = json.loads(self.data_processing_region.to_api())
        data_processing_region["region"] = AtlasDataLakeRegionMap[data_processing_region["region"]].value
        payload = {
            "name": self.name,
            "dataProcessingRegion": data_processing_region,
            "storage": self.storage.to_api(),
        }
        if self.cloud_provider_config is not None:
            payload["cloudProviderConfig"] = self.cloud_provider_config.to_api(test_s3_bucket=test_s3_bucket)
        return json.dumps(payload)

    def __str__(self):
        return self.name


FederatedDatabaseInstanceIdentifier: TypeAlias = FederatedDatabaseInstance | str


class OverrunPolicy(Enum):
    BLOCK = "BLOCK"
    BLOCK_AND_KILL = "BLOCK_AND_KILL"


class QueryLimitName(Enum):
    BYTES_PROCESSED_QUERY = "bytesProcessed.query"
    BYTES_PROCESSED_DAILY = "bytesProcessed.daily"
    BYTES_PROCESSED_WEEKLY = "bytesProcessed.weekly"
    BYTES_PROCESSED_MONTHLY = "bytesProcessed.monthly"


class QueryLimits(AtlasEntity):
    current_usage: int
    default_limit: int
    last_modified_date: datetime
    maximum_limit: int
    name: QueryLimitName
    overrun_policy: OverrunPolicy | None = None
    tenant_name: str
    value: int

    @classmethod
    def from_api(cls, response: dict) -> "QueryLimits":
        overrun_policy = response.pop("overrunPolicy", None)
        if overrun_policy is not None:
            overrun_policy = OverrunPolicy(overrun_policy)
        return cls(
            lastModifiedDate=datetime.fromisoformat(response.pop("lastModifiedDate")),
            name=QueryLimitName(response.pop("name")),
            overrunPolicy=overrun_policy,
            **response,
        )


class PrivateNetworkEndpoint(AtlasEntity):
    comment: str | None
    customer_endpoint_dns_name: str | None = None
    endpoint_id: str = Field(
        pattern=r"^vpce-[0-9a-f]{17}$",
    )
    provider: Literal["AWS"]
    region: str
    type: str = "DATA_LAKE"

    def __str__(self):
        return self.endpoint_id


PrivateNetworkEndpointIdentifier: TypeAlias = PrivateNetworkEndpoint | str
