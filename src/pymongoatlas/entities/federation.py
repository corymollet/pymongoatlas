from datetime import datetime
from enum import Enum
from typing import Optional, TypeAlias

from pydantic import AnyUrl, EmailStr, Field

from pymongoatlas.entities.auth import AtlasOrgRole, RoleAssignment
from pymongoatlas.entities.common import AtlasEntity
from pymongoatlas.util import optional_isoformat


class FederationSettings(AtlasEntity):
    federated_domains: list[str] = Field(default_factory=list)
    has_role_mappings: bool
    id: str
    identity_provider_id: str
    identity_provider_status: str

    def __str__(self):
        return self.id


FederationSettingsIdentifier: TypeAlias = FederationSettings | str


class FederatedRoleMapping(AtlasEntity):
    external_group_name: str
    id: str = Field(default="", exclude=True)
    role_assignments: list[RoleAssignment] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict):
        return cls(roleAssignments=[RoleAssignment.from_api(x) for x in response.pop("roleAssignments", [])], **response)

    def __str__(self):
        return self.id


FederatedRoleMappingIdentifier: TypeAlias = FederatedRoleMapping | str


class UserConflict(AtlasEntity):
    email_address: EmailStr
    federation_settings_id: str
    first_name: str
    last_name: str
    user_id: str | None = Field(default=None, exclude=True)


class ConnectedOrgConfig(AtlasEntity):
    data_access_identity_provider_ids: list[str] = Field(
        default_factory=list,
    )
    domain_allow_list: list[str] = Field(
        default_factory=list,
    )
    domain_restriction_enabled: bool
    identity_provider_id: str | None = None
    org_id: str = Field(default="", exclude=True)
    post_auth_role_grants: list[AtlasOrgRole] = Field(
        default_factory=list,
    )
    role_mappings: list[FederatedRoleMapping] = Field(default_factory=list)
    user_conflicts: list[UserConflict] = Field(
        default_factory=list,
    )

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            postAuthRoleGrants=[AtlasOrgRole(x) for x in response.pop("postAuthRoleGrants", [])],
            roleMappings=[FederatedRoleMapping.from_api(x) for x in response.pop("roleMappings", [])],
            userConflicts=[UserConflict.from_api(x) for x in response.pop("userConflicts", []) or []],
            **response,
        )


class IDPProtocol(Enum):
    OIDC = "OIDC"
    SAML = "SAML"


class CertificateInfo(AtlasEntity):
    not_after: datetime
    not_before: datetime

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            notAfter=datetime.fromisoformat(response["notAfter"]),
            notBefore=datetime.fromisoformat(response["notBefore"]),
        )


class PEMFileInfo(AtlasEntity):
    filename: str | None = None
    certificates: list[CertificateInfo]

    @classmethod
    def from_api(cls, response: dict | None) -> "Optional[PEMFileInfo]":
        if not response:
            return
        return cls(certificates=[CertificateInfo.from_api(x) for x in response.pop("certificates")], **response)


class IDPType(Enum):
    WORKFORCE = "WORKFORCE"
    WORKLOAD = "WORKLOAD"


class IDPStatus(Enum):
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"


class IDPRequestBinding(Enum):
    HTTP_POST = "HTTP-POST"
    HTTP_REDIRECT = "HTTP-REDIRECT"
    NONE = None


class ResponseSignatureAlgorithm(Enum):
    SHA1 = "SHA-1"
    SHA256 = "SHA-256"
    NONE = None


class OIDCAuthorizationType(Enum):
    GROUP = "GROUP"
    USER = "USER"


class SAMLIdentityProvider(AtlasEntity):
    acs_url: AnyUrl | None = None
    associated_domains: list[str] = Field(
        default_factory=list,
    )
    associated_orgs: list[ConnectedOrgConfig] = Field(
        default_factory=list,
    )
    created_at: datetime | None = None
    description: str | None = None
    display_name: str | None = Field(default=None, max_length=50)
    id: str = ""
    idp_type: IDPType = IDPType.WORKFORCE
    issuer_uri: str | None = None
    okta_idp_id: str
    pem_file_info: PEMFileInfo | None = None
    protocol: IDPProtocol = IDPProtocol.SAML
    request_binding: IDPRequestBinding = IDPRequestBinding.HTTP_REDIRECT
    response_signature_algorithm: ResponseSignatureAlgorithm = ResponseSignatureAlgorithm.SHA256
    sso_debug_enabled: bool = False
    sso_url: AnyUrl | None = None
    status: IDPStatus = IDPStatus.INACTIVE
    updated_at: datetime | None = None

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            associatedOrgs=[ConnectedOrgConfig.from_api(x) for x in response.pop("associatedOrgs", [])],
            createdAt=optional_isoformat(response.pop("createdAt", None)),
            pemFileInfo=PEMFileInfo.from_api(response.pop("pemFileInfo", None)),
            protocol=IDPProtocol(response.pop("protocol")),
            requestBinding=IDPRequestBinding(response.pop("requestBinding", None)),
            responseSignatureAlgorithm=ResponseSignatureAlgorithm(response.pop("responseSignatureAlgorithm", None)),
            status=IDPStatus(response.pop("status")),
            updatedAt=optional_isoformat(response.pop("updatedAt", None)),
            **response,
        )

    def __str__(self):
        return self.okta_idp_id


class OIDCWorkforceIdentityProvider(AtlasEntity):
    associated_domains: list[str] = Field(
        default_factory=list,
    )
    associated_orgs: list[ConnectedOrgConfig] = Field(
        default_factory=list,
    )
    audience: str | None = Field(default=None, exclude=True)
    authorization_type: OIDCAuthorizationType = OIDCAuthorizationType.GROUP
    client_id: str = ""
    created_at: datetime | None = Field(default=None, exclude=True)
    description: str | None = None
    display_name: str | None = Field(default=None, max_length=50)
    groups_claim: str | None = None
    id: str | None = Field(default=None, exclude=True)
    issuer_uri: str | None = None
    okta_idp_id: str = Field(exclude=True)
    requested_scopes: list[str] = Field(default_factory=list)
    updated_at: datetime | None = Field(default=None, exclude=True)
    user_claim: str | None = None
    protocol: IDPProtocol = Field(default=IDPProtocol.OIDC.value, frozen=True)

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            associatedOrgs=[ConnectedOrgConfig.from_api(x) for x in response.pop("associatedOrgs", [])],
            createdAt=datetime.fromisoformat(response.pop("createdAt")),
            updatedAt=optional_isoformat(response.pop("updatedAt", None)),
        )

    def __str__(self):
        return self.okta_idp_id


class OIDCWorkloadIdentityProvider(AtlasEntity):
    associated_orgs: list[ConnectedOrgConfig] = Field(
        default_factory=list,
    )
    audience: str | None = Field(default=None, exclude=True)
    authorization_type: OIDCAuthorizationType = OIDCAuthorizationType.GROUP
    created_at: datetime | None = Field(default=None, exclude=True)
    description: str | None = None
    display_name: str | None = Field(default=None, max_length=50)
    groups_claim: str | None = None
    id: str | None = Field(default=None, exclude=True)
    issuer_uri: str | None = None
    okta_idp_id: str
    updated_at: datetime | None = Field(default=None, exclude=True)
    user_claim: str | None = None
    protocol: IDPProtocol = Field(default=IDPProtocol.OIDC, frozen=True)

    @classmethod
    def from_api(cls, response: dict):
        return cls(
            associatedOrgs=[ConnectedOrgConfig.from_api(x) for x in response.pop("associatedOrgs", [])],
            createdAt=datetime.fromisoformat(response.pop("createdAt")),
            updatedAt=optional_isoformat(response.pop("updatedAt", None)),
            **response,
        )

    def __str__(self):
        return self.okta_idp_id


OIDCIdentityProvider: TypeAlias = OIDCWorkforceIdentityProvider | OIDCWorkloadIdentityProvider
IdentityProvider: TypeAlias = OIDCIdentityProvider | SAMLIdentityProvider
IdentityProviderIdentifier: TypeAlias = IdentityProvider | str
