import re
from datetime import UTC, datetime, timedelta


class InvalidTimePeriodParams(Exception):
    pass


def get_time_period_params(period: str | None = None, start: datetime | None = None, end: datetime | None = None) -> dict:
    error_message = "`period` is mutually exclusive with `start` and `end` options"
    if period is not None:
        if any(x is not None for x in (start, end)):
            raise InvalidTimePeriodParams(error_message)
        return {"period": period}
    if any(x is not None for x in (start, end)):
        if period is not None:
            raise InvalidTimePeriodParams(error_message)
        return {"start": start.isoformat(), "end": end.isoformat()}
    raise InvalidTimePeriodParams("At least one of `period` or `start` and `end` must be passed in to this function.")


def optional_isoformat(value: str | None) -> datetime | None:
    if not value:
        return None
    return datetime.fromisoformat(value)


def remove_port(hostname: str) -> str:
    return re.sub(r":\d+$", "", hostname)


def minutes_ago(minutes: int) -> tuple[datetime, datetime]:
    end = datetime.now(UTC)
    start = end - timedelta(minutes=minutes)
    return start, end


def remove_empty_fields(d: dict) -> dict:
    return {k: v for (k, v) in d.items() if v}


def remove_none_values(d: dict) -> dict:
    pruned = {}
    for key, value in d.items():
        if isinstance(value, dict):
            value = remove_none_values(value)
        if value is not None:
            pruned[key] = value
    return pruned


def get_request_body(**kwargs) -> dict:
    return {k: v for (k, v) in kwargs.items() if v}


def labels_to_dict(labels: list[dict[str, str]]) -> dict:
    return {x["key"]: x["value"] for x in labels}


def dict_to_labels(d: dict) -> list[dict[str, str]]:
    return [{"key": k, "value": v} for (k, v) in d.items()]
