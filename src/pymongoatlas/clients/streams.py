import math
from datetime import datetime
from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.projects import ProjectIdentifier
from pymongoatlas.entities.streams import (
    ClusterStreamConnection,
    KafkaStreamConnection,
    StreamInstance,
    StreamInstanceIdentifier,
    process_connections,
)


class StreamsClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> List[StreamInstance]:
        """
        Returns all stream instances for the specified project.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/listStreamInstances>_

        :param project: ``Project`` (entity or id)
        :return: List of ``StreamInstance`` entities
        """
        response = self.client.get(f"/groups/{project}/streams")
        return [StreamInstance.from_api(x) for x in response["results"]]

    def create(self, project: ProjectIdentifier, stream_instance: StreamInstance) -> StreamInstance:
        """
        Creates one stream instance in the specified project. To use this resource, the requesting API Key must have the
        Project Data Access Admin role, Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/createStreamInstance>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``SteamInstance`` entity to create
        :return: Created ``StreamInstance`` entity
        """
        response = self.client.post(f"/groups/{project}/streams", json=stream_instance.to_api())
        return StreamInstance.from_api(response)

    def delete(self, project: ProjectIdentifier, stream_instance: StreamInstanceIdentifier) -> None:
        """
        Delete one stream instance in the specified project. To use this resource, the requesting API Key must have the
        Project Data Access Admin role, Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/deleteStreamInstance>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or name)
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/streams/{stream_instance}")

    def get(self, project: ProjectIdentifier, stream_tenant_name: str) -> StreamInstance:
        """
        Returns the details of one stream instance within the specified project. To use this resource, the requesting
        API Key must have the Project Data Access roles, Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/getStreamInstance>_

        :param project: ``Project`` (entity or id)
        :param stream_tenant_name: Name of the stream to fetch.
        :return: ``StreamInstance`` entity
        """
        response = self.client.get(f"/groups/{project}/streams/{stream_tenant_name}")
        return StreamInstance.from_api(response)

    def update(self, project: ProjectIdentifier, stream_instance: StreamInstance) -> StreamInstance:
        """
        Update one stream instance in the specified project. To use this resource, the requesting API Key must have the
        Project Data Access Admin role, Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/updateStreamInstance>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` entity with updated data
        :return: Updated ``StreamInstance`` entity
        """
        response = self.client.patch(f"/groups/{project}/streams/{stream_instance}", json=stream_instance.to_api())
        return StreamInstance.from_api(response)

    def list_connections(
        self, project: ProjectIdentifier, stream_instance: StreamInstanceIdentifier
    ) -> List[ClusterStreamConnection | KafkaStreamConnection]:
        """
        Returns all connections of the stream instance for the specified project.To use this resource, the requesting
        API Key must have the Project Data Access roles, Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/listStreamConnections>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or id)
        :return: List of connections
        """
        response = self.client.get(f"/groups/{project}/streams/{stream_instance}/connections")
        return process_connections(response["results"])

    def create_connection(
        self,
        project: ProjectIdentifier,
        stream_instance: StreamInstanceIdentifier,
        connection: ClusterStreamConnection | KafkaStreamConnection,
    ) -> ClusterStreamConnection | KafkaStreamConnection:
        """
        Creates one connection for a stream instance in the specified project. To use this resource, the requesting API
        Key must have the Project Owner or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/createStreamConnection>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or id)
        :param connection: Connection entity to create
        :return: Created connection entity.
        """
        response = self.client.post(f"/groups/{project}/streams/{stream_instance}/connections", json=connection.to_api())
        return process_connections([response])[0]

    def delete_connection(
        self,
        project: ProjectIdentifier,
        stream_instance: StreamInstanceIdentifier,
        connection: ClusterStreamConnection | KafkaStreamConnection | str,
    ) -> None:
        """
        Delete one connection of the specified stream instance. To use this resource, the requesting API Key must have
        the Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/deleteStreamConnection>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or id)
        :param connection: Connection to delete (entity or id)
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/streams/{stream_instance}/connections/{connection}")

    def get_connection(
        self, project: ProjectIdentifier, stream_instance: StreamInstanceIdentifier, connection_name: str
    ) -> ClusterStreamConnection | KafkaStreamConnection:
        """
        Returns the details of one stream connection within the specified stream instance. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/getStreamConnection>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or id)
        :param connection_name: Name of the connection to fetch
        :return: Connection entity.
        """
        response = self.client.get(f"/groups/{project}/streams/{stream_instance}/connections/{connection_name}")
        return process_connections([response])[0]

    def update_connection(
        self,
        project: ProjectIdentifier,
        stream_instance: StreamInstanceIdentifier,
        connection: ClusterStreamConnection | KafkaStreamConnection,
    ) -> ClusterStreamConnection | KafkaStreamConnection:
        """
        Update one connection for the specified stream instance in the specified project. To use this resource, the
        requesting API Key must have the Project Owner role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/updateStreamConnection>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or id)
        :param connection: Connection entity with updated data
        :return: Updated Connection entity
        """
        response = self.client.patch(f"/groups/{project}/streams/{stream_instance}/connections/{connection}", json=connection.to_api())
        return process_connections([response])[0]

    def get_audit_logs(
        self, project: ProjectIdentifier, stream_instance: StreamInstanceIdentifier, start_date: datetime, end_date: datetime
    ):
        """
        Downloads the audit logs for the specified Atlas Streams Processing instance. By default, logs cover periods of
        30 days. To use this resource, the requesting API Key must have the Project Data Access roles, Project Owner
        role or Project Stream Processing Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Streams/operation/downloadStreamTenantAuditLogs>_

        :param project: ``Project`` (entity or id)
        :param stream_instance: ``StreamInstance`` (entity or id)
        :param start_date: Beginning of date range
        :param end_date: End of date range
        :return:
        """
        params = {"startDate": math.floor(start_date.timestamp()), "endDate": math.ceil(end_date.timestamp())}
        return self.client.stream_gzipped_logs(f"/groups/{project}/streams/{stream_instance}/auditLogs", params=params)
