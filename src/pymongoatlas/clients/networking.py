from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.cloud_providers import CloudProviderIdentifier, CloudProviderName, check_cloud_provider
from pymongoatlas.entities.cluster import ServerlessInstance
from pymongoatlas.entities.networking import (
    IPAccessEntry,
    IPAccessEntryIdentifier,
    IPAccessListStatus,
    PeeringConnection,
    PeeringConnectionAWS,
    PeeringConnectionAzure,
    PeeringConnectionGCP,
    PeeringConnectionIdentifier,
    PeeringContainer,
    PeeringContainerIdentifier,
    PrivateEndpoint,
    PrivateEndpointAWS,
    PrivateEndpointAzure,
    PrivateEndpointCreationRequest,
    PrivateEndpointGCP,
    PrivateEndpointProvider,
    PrivateEndpointService,
    PrivateEndpointServiceAWS,
    PrivateEndpointServiceAzure,
    PrivateEndpointServiceGCP,
    PrivateEndpointServiceResource,
    ServerlessPrivateEndpointAWS,
    ServerlessPrivateEndpointAzure,
)
from pymongoatlas.entities.projects import ProjectIdentifier


def process_endpoint(response: dict, cloud_provider: CloudProviderIdentifier) -> PrivateEndpoint:
    if cloud_provider == CloudProviderName.AWS:
        return PrivateEndpointAWS.from_api(response)
    if cloud_provider == CloudProviderName.AZURE:
        return PrivateEndpointAzure.from_api(response)
    if cloud_provider == CloudProviderName.GCP:
        return PrivateEndpointGCP.from_api(response)


def process_endpoint_services(response: list[dict]) -> list[PrivateEndpointService]:
    endpoints = []
    for endpoint in response:
        provider = PrivateEndpointProvider(endpoint["providerName"])
        if provider == PrivateEndpointProvider.AWS:
            endpoints.append(PrivateEndpointServiceAWS.from_api(endpoint))
        elif provider == PrivateEndpointProvider.AZURE:
            endpoints.append(PrivateEndpointServiceAzure.from_api(endpoint))
        elif provider == PrivateEndpointProvider.GCP:
            endpoints.append(PrivateEndpointServiceGCP.from_api(endpoint))
    return endpoints


def process_serverless_endpoints(response: list[dict]) -> list[ServerlessPrivateEndpointAWS | ServerlessPrivateEndpointAzure]:
    endpoints = []
    for endpoint in response:
        provider = PrivateEndpointProvider(endpoint["providerName"])
        if provider == PrivateEndpointProvider.AWS:
            endpoints.append(ServerlessPrivateEndpointAWS.from_api(endpoint))
        elif provider == PrivateEndpointProvider.AZURE:
            endpoints.append(ServerlessPrivateEndpointAzure.from_api(endpoint))
    return endpoints


def process_connections(response: list[dict]) -> list[PeeringConnection]:
    connections = []
    for connection in response:
        provider = CloudProviderName(connection["providerName"])
        if provider == CloudProviderName.AWS:
            connections.append(PeeringConnectionAWS.from_api(connection))
        elif provider == CloudProviderName.AZURE:
            connections.append(PeeringConnectionAzure.from_api(connection))
        elif provider == CloudProviderName.GCP:
            connections.append(PeeringConnectionGCP.from_api(connection))
    return connections


class NetworkClient(AtlasResourceClient):
    def create_peering_container(self, project: ProjectIdentifier, container: PeeringContainer) -> PeeringContainer:
        """
        Creates one new network peering container in the specified project. MongoDB Cloud can deploy Network Peering
        connections in a network peering container. GCP can have one container per project. AWS and Azure can have one
        container per cloud provider region. To use this resource, the requesting API Key must have the Project Owner
        role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/createPeeringContainer>_

        :param project: ``Project`` entity or its string id.
        :param container: ``PeeringContainer`` entity to create.
        :return: Created ``PeeringContainer`` entity.
        """
        response = self.client.post(f"/groups/{project}/containers", json=container.to_api())
        return PeeringContainer.from_api(response)

    def update_peering_container(self, project: ProjectIdentifier, container: PeeringContainer) -> PeeringContainer:
        """
        Updates the network details and labels of one specified network peering container in the specified project. To
        use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/updatePeeringContainer>_

        :param project: ``Project`` entity or its string id.
        :param container: ``PeeringContainer`` entity with data to update.
        :return: Updated ``PeeringContainer`` entity.
        """
        response = self.client.patch(f"/groups/{project}/containers/{container}", json=container.to_api())
        return PeeringContainer.from_api(response)

    def get_peering_container(self, project: ProjectIdentifier, container_id: str) -> PeeringContainer:
        """
        Returns details about one network peering container in one specified project. Network peering containers contain
        network peering connections. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/getPeeringContainer>_

        :param project: ``Project`` entity or its string id.
        :param container_id: ID of the peering container to fetch.
        :return: ``PeeringContainer`` entity.
        """
        response = self.client.get(f"/groups/{project}/containers/{container_id}")
        return PeeringContainer.from_api(response)

    def list_peering_containers(self, project: ProjectIdentifier) -> list[PeeringContainer]:
        """
        Returns details about all network peering containers in the specified project. Network peering containers
        contain network peering connections. To use this resource, the requesting API Key must have the Project Read
        Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/listPeeringContainers>_

        :param project: ``Project`` entity or its string id.
        :return: List of ``PeeringContainer`` entities.
        """
        response = self.client.get(f"/groups/{project}/containers/all")
        return [PeeringContainer.from_api(x) for x in response["results"]]

    def delete_peering_container(self, project: ProjectIdentifier, container: PeeringContainerIdentifier) -> None:
        """
        Removes one network peering container in the specified project. To use this resource, the requesting API Key
        must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/deletePeeringContainer>_

        :param project: ``Project`` entity or its string id.
        :param container: ``Container`` entity or its string id to delete.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/containers/{container}")

    def list_peering_connections(self, project: ProjectIdentifier) -> list[PeeringConnection]:
        """
        Returns details about all network peering connections in the specified project. Network peering allows multiple cloud-hosted applications to securely connect to the same project. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/listPeeringConnections>_

        :param project: ``Project`` entity or its string id.
        :return: List of ``PeeringConnection`` entities.
        """
        response = self.client.get(f"/groups/{project}/peers")
        return process_connections(response["results"])

    def create_peering_connection(self, project: ProjectIdentifier, connection: PeeringConnection) -> PeeringConnection:
        """
        Creates one new network peering connection in the specified project. Network peering allows multiple
        cloud-hosted applications to securely connect to the same project. To use this resource, the requesting API Key
        must have the Project Owner role. To learn more about considerations and prerequisites, see the `Network Peering
        Documentation <https://www.mongodb.com/docs/atlas/security-vpc-peering/>`_.

        `Azure Networking Peering Prerequisites <https://docs.atlas.mongodb.com/reference/api/vpc-create-peering-connection/#prerequisites>`_

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/createPeeringConnection>_

        :param project: ``Project`` entity or its string id.
        :param connection: ``PeeringConnection`` entity to create.
        :return: Created ``PeeringConnection`` entity.
        """
        response = self.client.post(f"/groups/{project}/peers", json=connection.to_api())
        return process_connections([response])[0]

    def delete_peering_connection(self, project: ProjectIdentifier, peering_connection: PeeringConnectionIdentifier) -> None:
        """
        Removes one network peering connection in the specified project. If you remove the last network peering
        connection associated with a project, MongoDB Cloud also removes any AWS security groups from the project IP
        access list. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/deletePeeringConnection>_

        :param project: ``Project`` entity or its string id.
        :param peering_connection: Peering Connection entity or its string id.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/peers/{peering_connection}")

    def get_peering_connection(self, project: ProjectIdentifier, peering_connection_id: str) -> PeeringConnection:
        """
        Returns details about one specified network peering connection in the specified project. To use this resource,
        the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/getPeeringConnection>_

        :param project: ``Project`` entity or its string id.
        :param peering_connection_id: Atlas ID of the peering connection to fetch.
        :return: Peering Connection entity
        """
        response = self.client.get(f"/groups/{project}/peers/{peering_connection_id}")
        return process_connections([response])[0]

    def update_peering_connection(self, project: ProjectIdentifier, peering_connection: PeeringConnection) -> PeeringConnection:
        """
        Updates one specified network peering connection in the specified project. To use this resource, the requesting
        API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Network-Peering/operation/updatePeeringConnection>_

        :param project: ``Project`` entity or its string id.
        :param peering_connection: Peering connection entity with data to update.
        :return: Updated peering connection entity.
        """
        response = self.client.patch(f"/groups/{project}/peers/{peering_connection}", json=peering_connection.to_api())
        return process_connections([response])[0]

    def list_project_ip_access_lists(self, project: ProjectIdentifier) -> list[IPAccessEntry]:
        """
        Returns all access list entries from the specified project's IP access list. Each entry in the project's IP
        access list contains either one IP address or one CIDR-notated block of IP addresses. MongoDB Cloud only allows
        client connections to the cluster from entries in the project's IP access list. To use this resource, the
        requesting API Key must have the Project Read Only or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Project-IP-Access-List/operation/listProjectIpAccessLists>_

        :param project: ``Project`` entity or its string id.
        :return: List of ``IPAccessList`` entities.
        """
        response = self.client.get(f"/groups/{project}/accessList")
        return [IPAccessEntry.from_api(x) for x in response["results"]]

    def get_project_ip_access_list_status(self, project: ProjectIdentifier, ip_access_list: IPAccessEntryIdentifier) -> IPAccessListStatus:
        """
        Returns the status of one project IP access list entry. This resource checks if the provided project IP access
        list entry applies to all cloud providers serving clusters from the specified project.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Project-IP-Access-List/operation/getProjectIpAccessListStatus>_

        :param project: ``Project`` entity or its string id
        :param ip_access_list: ``IPAccessList`` entity or its CIDR block or IP Address.
        :return: ``IPAccessListStatus``
        """
        response = self.client.get(f"/groups/{project}/accessList/{ip_access_list}/status")
        return IPAccessListStatus(response["STATUS"])

    def add_ip_access_entry(self, project: ProjectIdentifier, *ip_access_entries: IPAccessEntry) -> List[IPAccessEntry]:
        """
        Adds one or more access list entries to the specified project. MongoDB Cloud only allows client connections to
        the cluster from entries in the project's IP access list. Write each entry as either one IP address or one
        CIDR-notated block of IP addresses. To use this resource, the requesting API Key must have the Project Owner
        or Project Charts Admin roles. Update your applications to use this new resource. The
        ``/groups/{GROUP-ID}/accessList`` endpoint manages the database IP access list. This endpoint is distinct
        from the ``orgs/{ORG-ID}/apiKeys/{API-KEY-ID}/accesslist`` endpoint, which manages the access list for MongoDB
        Cloud organizations. This endpoint doesn't support concurrent POST requests. You must submit multiple POST
        requests synchronously.

        `IP Access List <https://docs.atlas.mongodb.com/security/ip-access-list/#std-label-access-list>`_

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Project-IP-Access-List/operation/createProjectIpAccessList>_

        :param project: ``Project`` entity or its string id.
        :param ip_access_entries: One or more ``IPAccessList`` to add to this Project.
        :return:
        """
        response = self.client.post(f"/groups/{project}/accessList", json=[x.to_api() for x in ip_access_entries])
        return [IPAccessEntry.from_api(x) for x in response["results"]]

    def delete_ip_access_entry(self, project: ProjectIdentifier, ip_access_entry: IPAccessEntryIdentifier) -> None:
        """
        Removes one access list entry from the specified project's IP access list. Each entry in the project's IP access
        list contains one IP address, one CIDR-notated block of IP addresses, or one AWS Security Group ID. MongoDB
        Cloud only allows client connections to the cluster from entries in the project's IP access list.
        To use this resource, the requesting API Key must have the Project Owner role. The
        ``/groups/{GROUP-ID}/accessList`` endpoint manages the database IP access list. This endpoint is distinct from
        the ``orgs/{ORG-ID}/apiKeys/{API-KEY-ID}/accesslist`` endpoint, which manages the access list for MongoDB Cloud
        organizations.

        `IP Access List <https://docs.atlas.mongodb.com/security/ip-access-list/#std-label-access-list>`_

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Project-IP-Access-List/operation/deleteProjectIpAccessList>_

        :param project: ``Project`` entity or its string id.
        :param ip_access_entry: IP Access Entry to delete
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/accessList/{ip_access_entry}")

    def create_private_endpoint_service(
        self, project: ProjectIdentifier, cloud_provider_name: CloudProviderName | str, region: str
    ) -> PrivateEndpointService:
        """
        Creates one private endpoint service for the specified cloud service provider. This cloud service provider
        manages the private endpoint service for the project. When you create a private endpoint service, MongoDB Cloud
        creates a network container in the project for the cloud provider for which you create the private endpoint
        service if one doesn't already exist. To learn more about private endpoint terminology in MongoDB Cloud, see
        `Private Endpoint Concepts <https://dochub.mongodb.org/core/private-endpoint-concepts>`_. To use this resource,
        the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/createPrivateEndpointService>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider_name: Cloud provider in which to create the private endpoint.
        :param region: Name of the cloud provider region in which to create the private endpoint.
        :return: Created private endpoint service
        """
        cloud_provider_name = CloudProviderName(cloud_provider_name)
        response = self.client.post(
            f"/groups/{project}/privateEndpoint/endpointService", json={"providerName": cloud_provider_name.value, "region": region}
        )
        return process_endpoint_services([response])[0]

    def get_regionalized_private_endpoint_status(self, project: ProjectIdentifier) -> bool:
        """
        Checks whether each region in the specified cloud service provider can create multiple private endpoints per
        region. The cloud service provider manages the private endpoint for the project. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/getRegionalizedPrivateEndpointSetting>_

        :param project: ``Project`` entity or its string id.
        :return: Flag indicating status.
        """
        response = self.client.get(f"/groups/{project}/privateEndpoint/regionalMode")
        return response["enabled"]

    def toggle_regionalized_private_endpoint_status(self, project: ProjectIdentifier, enabled: bool) -> bool:
        """
        Enables or disables the ability to create multiple private endpoints per region in all cloud service providers
        in one project. The cloud service provider manages the private endpoints for the project. Connection strings to
        existing multi-region and global sharded clusters change when you enable this setting. You must update your
        applications to use the new connection strings. This might cause downtime. To use this resource, the requesting
        API Key must have the Project Owner role and all clusters in the deployment must be sharded clusters. Once
        enabled, you cannot create replica sets.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/toggleRegionalizedPrivateEndpointSetting>_

        :param project: ``Project`` entity or its string id.
        :param enabled: Toggle ability on (``True``) or off (``False``).
        :return: Flag indicating status.
        """
        response = self.client.patch(f"/groups/{project}/privateEndpoint/regionalMode", json={"enabled": enabled})
        return response["enabled"]

    def list_private_endpoint_services(
        self, project: ProjectIdentifier, cloud_provider: CloudProviderIdentifier
    ) -> list[PrivateEndpointService]:
        """
        Returns the name, interfaces, and state of all private endpoint services for the specified cloud service
        provider. This cloud service provider manages the private endpoint service for the project. To use this
        resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/listPrivateEndpointServices>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider: Name of the cloud provider.
        :return: List of Private Endpoint Service entities.
        """
        cloud_provider = check_cloud_provider(cloud_provider)
        response = self.client.get(f"/groups/{project}/privateEndpoint/{cloud_provider.value}/endpointService")
        return process_endpoint_services(response)

    def delete_private_endpoint_service(
        self, project: ProjectIdentifier, cloud_provider: CloudProviderIdentifier, endpoint_service: PrivateEndpointServiceResource
    ) -> None:
        """
        Removes one private endpoint service from the specified project. This cloud service provider manages the private
        endpoint service that belongs to the project. To use this resource, the requesting API Key must have the Project
        Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/deletePrivateEndpointService>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider: Name of the cloud provider.
        :param endpoint_service: Private Endpoint Service entity or its string id.
        :return: ``None``
        """
        cloud_provider = check_cloud_provider(cloud_provider)
        self.client.delete(f"/groups/{project}/privateEndpoint/{cloud_provider.value}/endpointServices/{endpoint_service}")

    def get_private_endpoint_service(
        self, project: ProjectIdentifier, cloud_provider: CloudProviderIdentifier, endpoint_service_id: str
    ) -> PrivateEndpointService:
        """
        Returns the name, interfaces, and state of the specified private endpoint service from one project. The cloud
        service provider hosted this private endpoint service that belongs to the project. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/getPrivateEndpointService>_

        :param project: ``Project`` entity or its string id
        :param cloud_provider: Name of the cloud provider.
        :param endpoint_service_id: ID of the endpoint service to fetch.
        :return: Endpoint Service entity.
        """
        cloud_provider = check_cloud_provider(cloud_provider)
        response = self.client.get(f"/groups/{project}/privateEndpoint/{cloud_provider.value}/endpointService/{endpoint_service_id}")
        return process_endpoint_services([response])[0]

    def create_private_endpoint(
        self,
        project: ProjectIdentifier,
        cloud_provider: CloudProviderIdentifier,
        endpoint_service: PrivateEndpointServiceResource,
        creation_request: PrivateEndpointCreationRequest,
    ) -> PrivateEndpoint:
        """
        Creates one private endpoint for the specified cloud service provider. This cloud service provider manages the
        private endpoint service, which in turn manages the private endpoints for the project. To use this resource, the
        requesting API Key must have the Project Owner role. To learn more about considerations, limitations, and
        prerequisites, see the MongoDB documentation for setting up a private endpoint.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/createPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider: Name of the cloud provider.
        :param endpoint_service: Endpoint Service entity or its string id.
        :param creation_request: Creation request entity.
        :return: Created Private Endpoint entity.
        """
        cloud_provider = check_cloud_provider(cloud_provider)
        response = self.client.post(
            f"/groups/{project}/privateEndpoint/{cloud_provider.value}/endpointService/{endpoint_service}/endpoint",
            json=creation_request.to_api(),
        )
        return process_endpoint(response, cloud_provider)

    def delete_private_endpoint(
        self,
        project: ProjectIdentifier,
        cloud_provider: CloudProviderIdentifier,
        endpoint: PrivateEndpoint,
        endpoint_service: PrivateEndpointServiceResource,
    ) -> None:
        """
        Removes one private endpoint from the specified project and private endpoint service, as managed by the
        specified cloud service provider. When the last private endpoint is removed from a given private endpoint
        service, that private endpoint service is also removed. To use this resource, the requesting API Key must have
        the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/deletePrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider: Name of the cloud provider.
        :param endpoint: Private Endpoint entity to delete.
        :param endpoint_service: Private Endpoint Service entity or string id containing the endpoint to delete.
        :return: ``None``
        """
        cloud_provider = check_cloud_provider(cloud_provider)
        self.client.delete(
            f"/groups/{project}/privateEndpoint/{cloud_provider.value}/endpointService/{endpoint_service}/endpoint/{endpoint}"
        )

    def get_private_endpoint(
        self,
        project: ProjectIdentifier,
        cloud_provider: CloudProviderIdentifier,
        endpoint_id: str,
        endpoint_service: PrivateEndpointServiceResource,
    ) -> PrivateEndpoint:
        """
        Returns the connection state of the specified private endpoint. The private endpoint service manages this
        private endpoint which belongs to one project hosted from one cloud service provider. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Private-Endpoint-Services/operation/getPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider: Name of the cloud provider.
        :param endpoint_id: Unique string that identifies the private endpoint you want to return. The format of the
                            endpointId parameter differs for AWS, Azure, and GCP. You must URL encode the endpoint_id
                            for Azure private endpoints.
        :param endpoint_service: Private Endpoint Service entity or string id containing the endpoint to fetch.
        :return: Private Endpoint entity.
        """
        cloud_provider = check_cloud_provider(cloud_provider)
        response = self.client.get(
            f"/groups/{project}/privateEndpoint/{cloud_provider.value}/endpointService/{endpoint_service}/endpoint/{endpoint_id}"
        )
        return process_endpoint(response, cloud_provider)

    def list_serverless_private_endpoints(
        self, project: ProjectIdentifier, instance: ServerlessInstance | str
    ) -> List[ServerlessPrivateEndpointAWS | ServerlessPrivateEndpointAzure]:
        """
        Returns all private endpoints for one serverless instance. You must have at least the Project Read Only role for
        the project to successfully call this resource.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Private-Endpoints/operation/listServerlessPrivateEndpoints>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``ServerlessInstance`` or its string id.
        :return: List of Serverless Private Endpoint entities.
        """
        response = self.client.get(f"/groups/{project}/privateEndpoint/serverless/instance/{instance}/endpoint")
        return process_serverless_endpoints(response)

    def create_serverless_private_endpoint(
        self, project: ProjectIdentifier, instance: ServerlessInstance | str, comment: str | None = None
    ) -> str:
        """
        Creates one private endpoint for one serverless instance. To use this resource, the requesting API Key must have the Project Owner role.

        A new endpoint won't be immediately available after creation. Read the steps in the linked tutorial for detailed guidance.

        Set up a Private Endpoint for a Serverless Instance Tutorial

        <https://dochub.mongodb.org/core/serverless-private-endpoint>_

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Private-Endpoints/operation/createServerlessPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``ServerlessInstance`` or its string id.
        :param comment: Optional comment to include about this private endpoint.
        :return:
        """
        payload = {}
        if comment is not None:
            payload["comment"] = comment
        response = self.client.post(f"/groups/{project}/privateEndpoint/serverless/instance/{instance}/endpoint", json=payload)
        return response["_id"]

    def delete_serverless_private_endpoint(
        self,
        project: ProjectIdentifier,
        instance: ServerlessInstance | str,
        endpoint: ServerlessPrivateEndpointAWS | ServerlessPrivateEndpointAzure | str,
    ) -> None:
        """
        Remove one private endpoint from one serverless instance. To use this resource, the requesting API Key must have
        the Project Owner role.

        :param project: ``Project`` entity or its string id.
        :param instance: ``ServerlessInstance`` or its string id.
        :param endpoint: Endpoint to delete
        :return:
        """
        self.client.delete(f"/groups/{project}/privateEndpoint/serverless/instance/{instance}/endpoint/{endpoint}")

    def get_serverless_private_endpoint(
        self, project: ProjectIdentifier, instance: ServerlessInstance | str, endpoint_id: str
    ) -> ServerlessPrivateEndpointAWS | ServerlessPrivateEndpointAzure:
        """
        Return one private endpoint for one serverless instance. Identify this endpoint using its unique ID. You must
        have at least the Project Read Only role for the project to successfully call this resource.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Private-Endpoints/operation/getServerlessPrivateEndpoint>_

        :param project ``Project`` entity or its string id.
        :param instance: ``ServerlessInstance`` or its string id.
        :param endpoint_id: ID of the endpoint to fetch.
        :return: Private Endpoint entity.
        """
        response = self.client.get(f"/groups/{project}/privateEndpoint/serverless/instance/{instance}/endpoint/{endpoint_id}")
        return process_serverless_endpoints([response])[0]

    def update_serverless_private_endpoint(
        self,
        project: ProjectIdentifier,
        instance: ServerlessInstance | str,
        endpoint: ServerlessPrivateEndpointAWS | ServerlessPrivateEndpointAzure | str,
    ) -> ServerlessPrivateEndpointAWS | ServerlessPrivateEndpointAzure:
        """
        Updates one private endpoint for one serverless instance. To use this resource, the requesting API Key must have
        the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Private-Endpoints/operation/updateServerlessPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``ServerlessInstance` or its string id.
        :param endpoint: Endpoint entity data to update.
        :return: Endpoint entity with updated data.
        """
        response = self.client.patch(
            f"/groups/{project}/privateEndpoint/serverless/instance/{instance}/endpoint/{endpoint}", json=endpoint.to_api()
        )
        return process_serverless_endpoints([response])[0]
