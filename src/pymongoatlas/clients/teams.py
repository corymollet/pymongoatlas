from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.auth import AtlasUser, AtlasUserTypeAlias
from pymongoatlas.entities.organization import OrganizationIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier
from pymongoatlas.entities.teams import Team, TeamRole, TeamTypeAlias


class TeamsClient(AtlasResourceClient):
    def list_project_teams(self, project: ProjectIdentifier) -> List[TeamRole]:
        """
        Returns all teams to which the authenticated user has access in the project specified using its unique
        24-hexadecimal digit identifier. All members of the team share the same project access. To use this resource,
        the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/listProjectTeams>_
        :param project: ``Project`` (entity or id)
        :return: List of ``TeamRole`` entities
        """
        response = self.client.get(f"/groups/{project}/teams")
        return [TeamRole.from_api(x) for x in response["results"]]

    def list(self, org: OrganizationIdentifier) -> List[Team]:
        """
        Returns all teams that belong to the specified organization. Teams enable you to grant project access roles to
        MongoDB Cloud users. MongoDB Cloud only returns teams for which you have access. To use this resource, the
        requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/listOrganizationTeams>_

        :param org: ``Organization`` (entity or id)
        :return: List of ``Team`` entities
        """
        response = self.client.get(f"/orgs/{org}/teams")
        return [Team.from_api(x) for x in response["results"]]

    def create(self, org: OrganizationIdentifier, team: Team) -> Team:
        """
        Creates one team in the specified organization. Teams enable you to grant project access roles to MongoDB Cloud
        users. MongoDB Cloud limits the number of teams to a maximum of 250 teams per organization. To use this
        resource, the requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/createTeam>_

        :param org: ``Organization`` (entity or id) in which to create the team
        :param team: ``Team`` entity to create
        :return: Created ``Team`` entity
        """
        response = self.client.post(f"/orgs/{org}/teams", json=team.to_api())
        return Team.from_api(response)

    def get(self, org: OrganizationIdentifier, team_id: str) -> Team:
        """
        Returns one team that you identified using its unique 24-hexadecimal digit ID. This team belongs to one
        organization. Teams enable you to grant project access roles to MongoDB Cloud users. To use this resource, the
        requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/getTeamById>_

        :param org: ``Organization`` (entity or id)
        :param team_id: Team ID to fetch
        :return: ``Team`` entity
        """
        response = self.client.get(f"/orgs/{org}/teams/{team_id}")
        return Team.from_api(response)

    def get_by_name(self, org: OrganizationIdentifier, team_name: str) -> Team:
        """
        Returns one team that you identified using its human-readable name. This team belongs to one organization. Teams
        enable you to grant project access roles to MongoDB Cloud users. To use this resource, the requesting API Key
        must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/getTeamByName>_

        :param org: ``Organization`` (entity or id)
        :param team_name: Name of the team to fetch
        :return: ``Team`` entity
        """
        response = self.client.get(f"/orgs/{org}/teams/byName/{team_name}")
        return Team.from_api(response)

    def delete(self, org: OrganizationIdentifier, team: TeamTypeAlias) -> None:
        """
        Removes one team specified using its unique 24-hexadecimal digit identifier from the organization specified
        using its unique 24-hexadecimal digit identifier. To use this resource, the requesting API Key must have the
        Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/deleteTeam>_

        :param org: ``Organization`` (entity or id)
        :param team: ``Team`` (entity or id)
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}/teams/{team}")

    def add_team_to_project(self, project: ProjectIdentifier, team_role: TeamRole) -> TeamRole:
        """
        Adds one team to the specified project. All members of the team share the same project access. MongoDB Cloud
        limits the number of users to a maximum of 100 teams per project and a maximum of 250 teams per organization.
        To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/listProjectTeams>_

        :param project: ``Project`` (entity or id)
        :param team_role: ``TeamRole`` entity to add to the project
        :return: ``TeamRole`` entity
        """
        response = self.client.post(f"/groups/{project}/teams", json=team_role.to_api())
        return TeamRole.from_api(response["results"][0])

    def remove_team_from_project(self, project: ProjectIdentifier, team: TeamTypeAlias) -> None:
        """
        Removes one team specified using its unique 24-hexadecimal digit identifier from the project specified using its
        unique 24-hexadecimal digit identifier. To use this resource, the requesting API Key must have the Project Owner
        role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/removeProjectTeam>_

        :param project: ``Project`` (entity or id)
        :param team: ``Team`` (entity or id)
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/teams/{team}")

    def update_team_role(self, project: ProjectIdentifier, team: TeamRole) -> TeamRole:
        """
        Updates the project roles assigned to the specified team. You can grant team roles for specific projects and
        grant project access roles to users in the team. All members of the team share the same project access. To use
        this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/updateTeamRoles>_

        :param project: ``Project`` (entity or id)
        :param team: ``TeamRole`` entity with updated data
        :return: Updated ``TeamRole`` entity
        """
        response = self.client.patch(f"/groups/{project}/teams/{team}", json=team.to_api())
        return TeamRole.from_api(response["results"][0])

    def rename(self, org: OrganizationIdentifier, team: TeamTypeAlias, name: str | None = None) -> Team:
        """
        Renames one team in the specified organization. Teams enable you to grant project access roles to MongoDB Cloud
        users. To use this resource, the requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/renameTeam>_

        :param org: ``Organization`` (entity or id)
        :param team: ``Team`` (entity or id). If passing in the entity instead of the id, ensure the name is updated on the ``Team`` entity.
        :param name: If passing in the string id for the ``team`` parameter, use this parameter to update the name instead.
        :return: Updated ``Team`` entity.
        """
        payload = {}
        if name is not None:
            payload["name"] = name
        elif isinstance(team, Team):
            payload["name"] = team.name
        else:
            raise ValueError("Must specify a name")
        response = self.client.patch(f"/orgs/{org}/teams/{team}", json=payload)
        return Team.from_api(response)

    def list_users(self, org: OrganizationIdentifier, team: TeamTypeAlias) -> List[AtlasUser]:
        """
        Returns all MongoDB Cloud users assigned to the team specified using its unique 24-hexadecimal digit identifier.
        This team belongs to one organization. Teams enable you to grant project access roles to MongoDB Cloud users.
        To use this resource, the requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/listTeamUsers>_

        :param org: ``Organization`` (entity or id)
        :param team: ``Team`` (entity or id)
        :return: List of ``AtlasUser`` entities associated with this team
        """
        response = self.client.get(f"/orgs/{org}/teams/{team}/users")
        return [AtlasUser.from_api(x) for x in response["results"]]

    def assign_users(self, org: OrganizationIdentifier, team: TeamTypeAlias, users: List[AtlasUser | str]) -> List[AtlasUser]:
        """
        Adds one or more MongoDB Cloud users from the specified organization to the specified team. Teams enable you to
        grant project access roles to MongoDB Cloud users. You can assign up to 250 MongoDB Cloud users from one
        organization to one team. To use this resource, the requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/addTeamUser>_

        :param org: ``Organization`` (entity or id)
        :param team: ``Team`` (entity or id)
        :param users:
        :return:
        """
        response = self.client.post(f"/orgs/{org}/teams/{team}/users", json=[str(x) for x in users])
        return [AtlasUser.from_api(x) for x in response["results"]]

    def remove_user(self, org: OrganizationIdentifier, team: TeamTypeAlias, user: AtlasUserTypeAlias) -> None:
        """
        Removes one MongoDB Cloud user from the specified team. This team belongs to one organization. Teams enable you
        to grant project access roles to MongoDB Cloud users. To use this resource, the requesting API Key must have the
        Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Teams/operation/removeTeamUser>_

        :param org: ``Organization`` (entity or id)
        :param team: ``Team`` (entity or id)
        :param user: ``AtlasUser`` (entity or id)
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}/teams/{team}/users/{user}")
