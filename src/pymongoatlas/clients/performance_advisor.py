from datetime import datetime

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.cluster import Cluster, ClusterIdentifier
from pymongoatlas.entities.monitoring import ProcessIdentifier
from pymongoatlas.entities.performance_advisor import Namespace, RollingIndex, SlowQuery, SuggestedIndex
from pymongoatlas.entities.projects import Project, ProjectIdentifier


class PerformanceAdvisorClient(AtlasResourceClient):
    def list_slow_query_namespaces(self, project: ProjectIdentifier, process: ProcessIdentifier) -> list[Namespace]:
        """
        Returns up to 20 namespaces for collections experiencing slow queries on the specified host. If you specify a
        secondary member of a replica set that hasn't received any database read operations, the endpoint doesn't return
        any namespaces. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/listSlowQueryNamespaces>_

        :param project: ``Project`` (entity or id)
        :param process: ``Process`` (entity or id)
        :return: list of ``Namespace`` entities
        """
        response = self.client.get(f"/groups/{project}/processes/{process}/performanceAdvisor/namespaces")
        return [Namespace.from_api(x) for x in response["namespaces"]]

    def list_slow_queries(
        self,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        duration: int | None = None,
        namespaces: list[Namespace] | None = None,
        n_logs: int = 20000,
        since: int | datetime | None = None,
    ) -> list[SlowQuery]:
        """
        Returns log lines for slow queries that the Performance Advisor and Query Profiler identified. The Performance
        Advisor monitors queries that MongoDB considers slow and suggests new indexes to improve query performance.
        MongoDB Cloud bases the threshold for slow queries on the average time of operations on your cluster. This
        enables workload-relevant recommendations. To use this resource, the requesting API Key must have the Project
        Data Access Read Write role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/listSlowQueries>_

        :param project: ``Project`` (entity or id).
        :param process: ``Process`` (entity or id).
        :param duration: Length of time expressed during which the query finds slow queries among the managed namespaces
                         in the cluster. This parameter expresses its value in milliseconds. If you don't specify the
                         ``since`` parameter, the endpoint returns data covering the duration before the current time.
                         If you specify neither the ``duration`` nor ``since`` parameters, the endpoint returns data
                         from the previous 24 hours.
        :param namespaces: list of ``Namespace`` entities from which to retrieve slow queries.
        :param n_logs: Maximum number of lines from the log to return.
        :param since: Date and time from which the query retrieves the slow queries. This parameter expresses its value
                      in the number of milliseconds that have elapsed since the UNIX epoch.
        :return: list of ``SlowQuery`` entities

        """
        params = {}
        if duration is not None:
            params["duration"] = duration
        if namespaces is not None:
            params["namespaces"] = [x.namespace for x in namespaces]
        if n_logs is not None:
            params["nLogs"] = n_logs
        if since is not None:
            if isinstance(since, datetime):
                since = since.timestamp() * 1000
            params["since"] = since
        response = self.client.get(f"/groups/{project}/processes/{process}/performanceAdvisor/slowQueryLogs", params=params)
        return [SlowQuery.from_api(x) for x in response["slowQueries"]]

    def list_suggested_indexes(self, project: ProjectIdentifier, process: ProcessIdentifier) -> SuggestedIndex:
        """
        Returns the indexes that the Performance Advisor suggests. The Performance Advisor monitors queries that MongoDB
        considers slow and suggests new indexes to improve query performance. To use this resource, the requesting API
        Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/listSuggestedIndexes>_

        :param project: ``Project`` (entity or id)
        :param process: ``Process`` (entity or id)
        :return: ``SuggestedIndex`` entity (contains both suggested indexes and shapes)
        """
        response = self.client.get(f"/groups/{project}/processes/{process}/performanceAdvisor/suggestedIndexes")
        return SuggestedIndex.from_api(response)

    def get_serverless_auto_indexing_enabled(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> bool:
        """
        Get whether the Serverless Auto Indexing feature is enabled.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/getServerlessAutoIndexing>_

        :param project: ``Project`` (entity or id)
        :param cluster: ``Cluster`` (entity or name)
        :return: bool indicating status
        """
        return self.client.get(f"/groups/{project}/serverless/{cluster}/performanceAdvisor/autoIndexing")

    def set_serverless_auto_indexing(self, project: ProjectIdentifier, cluster: ClusterIdentifier, enable: bool) -> None:
        """
        Set whether the Serverless Auto Indexing feature is enabled.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/setServerlessAutoIndexing>_

        :param project: ``Project`` (entity or id)
        :param cluster: ``Cluster`` (entity or name)
        :param enable: Toggle Serverelss Auto Indexing on (``True``) or off (``False``).
        :return: ``None``
        """
        self.client.post(f"/groups/{project}/serverless/{cluster}/performanceAdvisor/autoIndexing", params={"enable": enable})

    def create_rolling_index(self, project: Project | str, cluster: Cluster | str, rolling_index: RollingIndex):
        """
        Creates an index on the cluster identified by its name in a rolling manner. Creating the index in this way
        allows index builds on one replica set member as a standalone at a time, starting with the secondary members.
        Creating indexes in this way requires at least one replica set election. To use this resource, the requesting
        API Key must have the Project Data Access Admin role.

        :param project:
        :param cluster:
        :param rolling_index:
        :return:
        """
        self.client.post(f"/groups/{project}/clusters/{cluster}/index", json=rolling_index.to_api())

    def enable_managed_slow_operation_threshold(self, project: ProjectIdentifier) -> None:
        """
        Enables MongoDB Cloud to use its slow operation threshold for the specified project. The threshold determines
        which operations the Performance Advisor and Query Profiler considers slow. When enabled, MongoDB Cloud uses the
        average execution time for operations on your cluster to determine slow-running queries. As a result, the
        threshold is more pertinent to your cluster workload. The slow operation threshold is enabled by default for
        dedicated clusters (M10+). When disabled, MongoDB Cloud considers any operation that takes longer than 100
        milliseconds to be slow. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/enableSlowOperationThresholding>_

        :param project: ``Project`` (entity or id)
        :return: ``None``
        """
        self.client.post(f"/groups/{project}/managedSlowMs/enable")

    def disable_managed_slow_operation_threshold(self, project: ProjectIdentifier) -> None:
        """
        Disables the slow operation threshold that MongoDB Cloud calculated for the specified project. The threshold
        determines which operations the Performance Advisor and Query Profiler considers slow. When enabled, MongoDB
        Cloud uses the average execution time for operations on your cluster to determine slow-running queries. As a
        result, the threshold is more pertinent to your cluster workload. The slow operation threshold is enabled by
        default for dedicated clusters (M10+). When disabled, MongoDB Cloud considers any operation that takes longer
        than 100 milliseconds to be slow. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Performance-Advisor/operation/disableSlowOperationThresholding>_

        :param project: ``Project`` (entity or id)
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/managedSlowMs/disable")
