import json
from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.auth import (
    ApiKey,
    ApiKeyIdentifier,
    AtlasUser,
    AuthDatabaseName,
    CustomRole,
    CustomRoleIdentifier,
    DatabaseUser,
    DatabaseUserIdentifier,
    UserAccessList,
    UserCert,
)
from pymongoatlas.entities.ldap import CustomerX509, LDAPSecuritySettings
from pymongoatlas.entities.organization import OrganizationIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier


class ApiKeysClient(AtlasResourceClient):
    def list_org_keys(self, org: OrganizationIdentifier) -> List[ApiKey]:
        """
        Returns all organization API keys for the specified organization. The organization API keys grant programmatic
        access to an organization. You can't use the API key to log into MongoDB Cloud through the console. To use this
        resource, the requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/listApiKeys>_

        :param org: ``Organization`` entity or its string id.
        :return: List of ``ApiKey`` entities
        """
        response = self.client.get(f"/orgs/{org}/apiKeys")
        return [ApiKey.from_api(x) for x in response["results"]]

    def list_project_keys(self, project: ProjectIdentifier) -> List[ApiKey]:
        """
        Returns all organization API keys that you assigned to the specified project. Users with the Project Owner role
        in the project associated with the API key can use the organization API key to access the resources. To use this
        resource, the requesting API Key must have the Project User Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/listProjectApiKeys>_

        :param project: ``Project`` entity or its string id.
        :return: List of ``ApiKey`` entities
        """
        response = self.client.get(f"/groups/{project}/apiKeys")
        return [ApiKey.from_api(x) for x in response["results"]]

    def create_org_key(self, org: OrganizationIdentifier, api_key: ApiKey) -> ApiKey:
        """
        Creates one API key for the specified organization. An organization API key grants programmatic access to an
        organization. You can't use the API key to log into the console. To use this resource, the requesting API Key
        must have the Organization Owner role.

        :param org: ``Organization`` entity or its string id.
        :param api_key: The org api key entity to create
        :return: Created ``ApiKey`` entity.
        """
        response = self.client.post(f"/orgs/{org}/apiKeys", data=api_key.to_api())
        return ApiKey.from_api(response)

    def create_project_key(self, project: ProjectIdentifier, api_key: ApiKey) -> ApiKey:
        """
        Creates and assigns the specified organization API key to the specified project. Users with the Project Owner role
        in the project associated with the API key can use the organization API key to access the resources. To use this
        resource, the requesting API Key must have the Project User Admin role.

        :param project: ``Project`` entity or its string id.
        :param api_key: The project api key entity to create
        :return: Created ``ApiKey`` entity
        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/createProjectApiKey>_
        """
        response = self.client.post(f"/groups/{project}/apiKeys", data=api_key.to_api())
        return ApiKey.from_api(response)

    def remove_project_key(self, project: ProjectIdentifier, api_key: ApiKeyIdentifier) -> None:
        """
        Removes one organization API key from the specified project. To use this resource, the requesting API Key must
        have the Project User Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/removeProjectApiKey>_

        :param project: ``Project`` entity or its string id.
        :param api_key: ``ApiKey`` entity or its tsring id.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/apiKeys/{api_key}")

    def update_org_key(self, org: OrganizationIdentifier, api_key: ApiKey) -> ApiKey:
        """
        Updates one organization API key in the specified organization. The organization API keys grant programmatic
        access to an organization. To use this resource, the requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/updateApiKey>_

        :param org: ``Organization`` entity or its string id
        :param api_key: ``ApiKey`` entity with updated data
        :return: Updated ``ApiKey`` entity
        """
        response = self.client.patch(f"/orgs/{org}/apiKeys/{api_key}", data=api_key.to_api())
        return ApiKey.from_api(response)

    def update_project_key(self, project: ProjectIdentifier, api_key: ApiKey) -> ApiKey:
        """
        Updates the roles of the organization API key that you specify for the project that you specify. You must
        specify at least one valid role for the project. The application removes any roles that you do not include in
        this request if they were previously set in the organization API key that you specify for the project.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/updateApiKeyRoles>_

        :param project: ``Project`` entity or its string id.
        :param api_key: ``ApiKey`` entity with updated data.
        :return: Updated ``ApiKey`` entity.
        """
        response = self.client.patch(f"/groups/{project}/apiKeys/{api_key}", data=api_key.to_api())
        return ApiKey.from_api(response)

    def delete(self, org: OrganizationIdentifier, api_key: ApiKeyIdentifier) -> None:
        """
        Removes one organization API key from the specified organization. When you remove an API key from an
        organization, MongoDB Cloud also removes that key from any projects that use that key. To use this resource, the
        requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/deleteApiKey>_

        :param org: ``Organization`` entity or its string id.
        :param api_key: ``ApiKey`` entity or its string id to delete
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}/apiKeys/{api_key}")

    def create_access_list(
        self,
        org: OrganizationIdentifier,
        api_key: ApiKeyIdentifier,
        cidr_blocks: List[str] | None = None,
        ip_addresses: List[str] | None = None,
    ) -> List[UserAccessList]:
        """
        Creates the access list entries for the specified organization API key. Resources require all API requests
        originate from IP addresses on the API access list. To use this resource, the requesting API Key must have the
        Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/createApiKeyAccessList>_

        :param org: ``Organization`` entity or its string id.
        :param api_key: ``ApiKey`` entity or its string id associated with this access list.
        :param cidr_blocks: List of CIDR blocks of the access list (mutually exclusive with ``ip_address``).
        :param ip_addresses: List of IP addresses of the access list (mutually exclusive with ``cidr_block``).
        :return: List of ``UserAccessList`` entities
        """
        payload = []
        if cidr_blocks is not None:
            for cidr_block in cidr_blocks:
                payload.append({"cidrBlock": cidr_block})
        if ip_addresses is not None:
            for ip_address in ip_addresses:
                payload.append({"ipAddress": ip_address})
        response = self.client.post(f"/orgs/{org}/apiKeys/{api_key}/accessList", json=payload)
        return [UserAccessList.from_api(x) for x in response["results"]]

    def list_access_list_entries(self, org: OrganizationIdentifier, api_key: ApiKeyIdentifier) -> List[UserAccessList]:
        """
        Returns all access list entries that you configured for the specified organization API key. To use this
        resource, the requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/listApiKeyAccessListsEntries>_

        :param org: ``Organization`` entity or its string id.
        :param api_key: ``ApiKey`` entity or its string id associated with the access list entries.
        :return: List of ``UserAccessList`` entities.
        """
        response = self.client.get(f"/orgs/{org}/apiKeys/{api_key}/accessList")
        return [UserAccessList.from_api(x) for x in response["results"]]

    def delete_access_list_entry(self, org: OrganizationIdentifier, api_key: ApiKeyIdentifier, ip_address: str) -> None:
        """
        Removes the specified access list entry from the specified organization API key. Resources require all API
        requests originate from the IP addresses on the API access list. To use this resource, the requesting API Key
        must have the Read Write role. In addition, you cannot remove the requesting IP address from the requesting
        organization API key.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Programmatic-API-Keys/operation/deleteApiKeyAccessListEntry>_

        :param org: ``Organization`` entity or its string id.
        :param api_key: ``ApiKey`` entity or its string id.
        :param ip_address: One IP address or multiple IP addresses represented as one CIDR block to limit requests to
                           API resources in the specified organization.
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}/apiKeys/{api_key}/accessList/{ip_address}")


class AtlasUsersClient(AtlasResourceClient):
    def get_by_username(self, username: str) -> AtlasUser:
        """
        Returns the details for one MongoDB Cloud user account with the specified username. You can't use this endpoint
        to return information about an API Key. To return information about an API Key, use the Return One Organization
        API Key endpoint. To use this resource, the requesting API Key can have any role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/MongoDB-Cloud-Users/operation/getUserByUsername>_

        :param username: Username of the user to retrieve.
        :return: AtlasUser entity.
        """
        response = self.client.get(f"/users/byName/{username}")
        return AtlasUser.from_api(response)

    def get_by_id(self, user_id: str) -> AtlasUser:
        """
        Returns the details for one MongoDB Cloud user account with the specified unique identifier for the user. You
        can't use this endpoint to return information on an API Key. To return information about an API Key, use the
        Return One Organization API Key endpoint. You can always retrieve your own user account. If you are the owner of
        a MongoDB Cloud organization or project, you can also retrieve the user profile for any user with membership in
        that organization or project. To use this resource, the requesting API Key can have any role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/MongoDB-Cloud-Users/operation/getUser_>

        :param user_id: ID of the user to retrieve.
        :return: AtlasUser entity.
        """
        response = self.client.get(f"/users/{user_id}")
        return AtlasUser.from_api(response)

    def create(self, user: AtlasUser) -> AtlasUser:
        """
        Creates one MongoDB Cloud user account. A MongoDB Cloud user account grants access to only the MongoDB Cloud
        application. To grant database access, create a database user. MongoDB Cloud sends an email to the users you
        specify, inviting them to join the project. Invited users don't have access to the project until they accept
        the invitation. Invitations expire after 30 days.

        MongoDB Cloud limits MongoDB Cloud user membership to a maximum of 250 MongoDB Cloud users per team. MongoDB
        Cloud limits MongoDB Cloud user membership to 500 MongoDB Cloud users per project and 500 MongoDB Cloud users
        per organization, which includes the combined membership of all projects in the organization. MongoDB Cloud
        raises an error if an operation exceeds these limits. For example, if you have an organization with five
        projects, and each project has 100 MongoDB Cloud users, and each MongoDB Cloud user belongs to only one project,
        you can't add any MongoDB Cloud users to this organization without first removing existing MongoDB Cloud users
        from the organization.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/MongoDB-Cloud-Users/operation/createUser>_
        :param user: AtlasUser entity to create
        :return: Created AtlasUser entity
        """
        response = self.client.post("/users", data=user.to_api())
        return AtlasUser.from_api(response)


class DatabaseUsersClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> List[DatabaseUser]:
        """
        Returns all database users that belong to the specified project. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Database-Users/operation/listDatabaseUsers>_

        :param project: Project from which to list database users
        :return: List of DatabaseUser entities
        """
        response = self.client.get(f"/groups/{project}/databaseUsers")
        return [DatabaseUser.from_api(x) for x in response["results"]]

    def get(self, project: ProjectIdentifier, username: str, database_name: AuthDatabaseName = AuthDatabaseName.ADMIN) -> DatabaseUser:
        """
        Returns one database user that belong to the specified project. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Database-Users/operation/getDatabaseUser>_

        :param project: Project from which to retrieve the database user
        :param username: Username of the database user
        :param database_name: Authentication database against which the database user authenticates
        :return: DatabaseUser entity
        """
        response = self.client.get(f"/groups/{project}/databaseUsers/{database_name.value}/{username}")
        return DatabaseUser.from_api(response)

    def create(self, project: ProjectIdentifier, database_user: DatabaseUser, password: str | None = None) -> DatabaseUser:
        """
        Creates one database user in the specified project. This MongoDB Cloud supports a maximum of 100 database users
        per project. If you require more than 100 database users on a project, contact Support. To use this resource,
        the requesting API Key must have the Project Owner or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Database-Users/operation/createDatabaseUser>_

        :param project: Project in which to create the database user
        :param database_user: The DatabaseUser entity to create
        :param password: Optional password for the database user, if authenticating via SCRAM-SHA
        :return: Created DatabaseUser entity
        """
        payload = database_user.to_api()
        if password is not None:
            payload = json.loads(payload)
            payload["password"] = password
            payload = json.dumps(payload)
        response = self.client.post(f"/groups/{project}/databaseUsers", data=payload)
        return DatabaseUser.from_api(response)

    def delete(self, project: ProjectIdentifier, database_user: DatabaseUser) -> None:
        """
        Removes one database user from the specified project. To use this resource, the requesting API Key must have the
        Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Database-Users/operation/deleteDatabaseUser>_

        :param project: Project from which to delete this database user
        :param database_user: DatabaseUser entity to delete
        """
        self.client.delete(f"/groups/{project}/databaseUsers/{database_user.database_name.value}/{database_user.username}")

    def update(self, project: ProjectIdentifier, database_user: DatabaseUser, password: str | None = None):
        """
        Updates one database user that belongs to the specified project. To use this resource, the requesting API Key
        must have the Project Owner or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Database-Users/operation/updateDatabaseUser>_

        :param project: Project in which to the database user to be updated exists
        :param database_user: DatabaseUser entity with updated data
        :param password: Optional password for the database user, if authenticating via SCRAM-SHA
        """
        payload = database_user.to_api()
        if password is not None:
            payload = json.loads(payload)
            payload["password"] = password
            payload = json.dumps(payload)
        response = self.client.patch(
            f"/groups/{project}/databaseUsers/{database_user.database_name.value}/{database_user.username}",
            json=payload,
        )
        return response


class CustomDatabaseRolesClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> List[CustomRole]:
        """
        Returns all custom roles for the specified project. To use this resource, the requesting API Key must have the
        Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Custom-Database-Roles/operation/listCustomDatabaseRoles>_

        :param project: Project from which to list custom roles.
        :return: List of ``CustomRole`` entities.
        """
        response = self.client.get(f"/groups/{project}/customDBRoles/roles")
        return [CustomRole.from_api(x) for x in response]

    def create(self, project: ProjectIdentifier, custom_role: CustomRole) -> CustomRole:
        """
        Creates one custom role in the specified project. To use this resource, the requesting API Key must have the
        Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Custom-Database-Roles/operation/createCustomDatabaseRole>_

        :param project: Project in which to create the custom role
        :param custom_role: ``CustomRole`` entity to create.
        :return: Created ``CustomRole`` entity
        """
        response = self.client.post(f"/groups/{project}/customDBRoles/roles", data=custom_role.to_api())
        return CustomRole.from_api(response)

    def get(self, project: ProjectIdentifier, role_name: str) -> CustomRole:
        """
        Returns one custom role for the specified project. To use this resource, the requesting API Key must have the
        Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Custom-Database-Roles/operation/getCustomDatabaseRole>_

        :param project: Project from which to retrieve the custom role.
        :param role_name: Name of the custom role to retrieve.
        :return: ``CustomRole`` entity.
        """
        response = self.client.get(f"/groups/{project}/customDBRoles/roles/{role_name}")
        return CustomRole.from_api(response)

    def delete(self, project: ProjectIdentifier, custom_role: CustomRoleIdentifier) -> None:
        """
        Removes one custom role from the specified project. You can't remove a custom role that would leave one or more
        child roles with no parent roles or actions. You also can't remove a custom role that would leave one or more
        database users without roles. To use this resource, the requesting API Key must have the Project Atlas Admin
        role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Custom-Database-Roles/operation/deleteCustomDatabaseRole>_

        :param project: Project from which to delete the custom role.
        :param custom_role: ``CustomRole`` entity to delete.
        """
        self.client.delete(f"/groups/{project}/customDBRoles/roles/{custom_role}")

    def update(self, project: ProjectIdentifier, custom_role: CustomRole) -> CustomRole:
        """
        Updates one custom role in the specified project. To use this resource, the requesting API Key must have the
        Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Custom-Database-Roles/operation/updateCustomDatabaseRole>_

        :param project: Project containing the custom role to update.
        :param custom_role: ``CustomRole`` entity with updated data.
        :return: Updated ``CustomRole`` entity.
        """
        response = self.client.patch(f"/groups/{project}/customDBRoles/roles/{custom_role.name}", data=custom_role.to_api())
        return CustomRole.from_api(response)


class X509AuthenticationClient(AtlasResourceClient):
    """
    Returns, edits, and removes user-managed X.509 configurations. Also returns and generates MongoDB Cloud-managed
    X.509 certificates for database users. The following resources help manage database users who authenticate using
    X.509 certificates. You can manage these X.509 certificates or let MongoDB Cloud do it for you. If MongoDB Cloud
    manages your certificates, it also manages your Certificate Authority and can generate certificates for your
    database users. No additional X.509 configuration is required. If you manage your certificates, you must provide a
    Certificate Authority and generate certificates for your database users.

    <https://www.mongodb.com/docs/atlas/security-self-managed-x509/>_
    """

    def list(self, project: ProjectIdentifier, user: DatabaseUserIdentifier) -> List[UserCert]:
        """
        Returns all unexpired X.509 certificates for the specified MongoDB user. This MongoDB user belongs to one
        project. Atlas manages these certificates and the MongoDB user. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/X.509-Authentication>_

        :param project: ``Project`` entity or its string id.
        :param user: ``DatabaseUser`` entity or its string name.
        :return: List of ``UserCert`` entities.
        """
        response = self.client.get(f"/groups/{project}/databaseUsers/{user}/certs")
        return [UserCert.from_api(x) for x in response["results"]]

    def create(self, project: ProjectIdentifier, user: DatabaseUserIdentifier, months_until_expiration: int = 3) -> str:
        """
        Generates one X.509 certificate for the specified MongoDB user. Atlas manages the certificate and MongoDB user
        that belong to one project. To use this resource, the requesting API Key must have the Project Owner role.

        To get MongoDB Cloud to generate a managed certificate for a database user, set "x509Type" : "MANAGED" on the
        desired MongoDB Database User.

        If you are managing your own Certificate Authority (CA) in Self-Managed X.509 mode, you must generate
        certificates for database users using your own CA.

        <https://www.mongodb.com/docs/atlas/security-self-managed-x509/#std-label-self-managed-x509>_

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/X.509-Authentication/operation/createDatabaseUserCertificate>_

        :param project: ``Project`` entity or its string id.
        :param user: Database user for which to create the cert.
        :param months_until_expiration: Number of months (between 3 and 24) for which the cert will be valid.
        :return: PEM-encoded string that contains the user's X.509 certificate and private key.
        """
        if not 3 <= months_until_expiration <= 24:
            raise ValueError(f"`months_until_expiration` must be between 3 and 24, not {months_until_expiration}")
        response = self.client.post(
            f"/groups/{project}/databaseUsers/{user}/certs", json={"monthsUntilExpiration": months_until_expiration}
        )
        return response

    def disable(self, project: ProjectIdentifier) -> dict[str, CustomerX509 | LDAPSecuritySettings]:
        """
        Clears the customer-managed X.509 settings on a project, including the uploaded Certificate Authority, which
        disables self-managed X.509.

        Updating this configuration triggers a rolling restart of the database. You must have the Project Owner role to
        use this endpoint.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/X.509-Authentication/operation/disableCustomerManagedX509>_

        :param project: ``Project`` entity or its string id.
        :return: Dictionary of X509 and LDAP security settings entities.
        """
        response = self.client.delete(f"/groups/{project}/userSecurity/customerX509")
        return {"customerX509": CustomerX509.from_api(response["customerX509"]), "ldap": LDAPSecuritySettings.from_api(response["ldap"])}
