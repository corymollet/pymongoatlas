from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.integration import Integration, IntegrationType
from pymongoatlas.entities.projects import Project


def parse_integration_type(integration_type: IntegrationType | str) -> IntegrationType:
    if isinstance(integration_type, IntegrationType):
        return integration_type
    elif isinstance(integration_type, str):
        return IntegrationType(integration_type.upper().strip().replace(" ", "_").replace("-", "_"))
    else:
        raise TypeError(f"Cannot parse integration type from {integration_type}")


class IntegrationsClient(AtlasResourceClient):
    def create(self, project: Project | str, integration: Integration) -> Integration:
        """
        Adds the settings for configuring one third-party service integration. These settings apply to all databases
        managed in the specified MongoDB Cloud project. Each project can have only one configuration per
        {INTEGRATION-TYPE}. To use this resource, the requesting API Key must have the Organization Owner or Project
        Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Third-Party-Integrations/operation/createThirdPartyIntegration>_
        """
        self.client.post(f"/groups/{project.id}/integrations/{integration.type.value}", json=integration.to_api())
        return self.get(project, integration.type)

    def list(self, project: Project | str) -> list[Integration]:
        """
        Returns the settings that permit integrations with all configured third-party services. These settings apply to
        all databases managed in one MongoDB Cloud project. To use this resource, the requesting API Key must have the
        Organization Owner or Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Third-Party-Integrations/operation/listThirdPartyIntegrations>_
        """
        response = self.client.get(f"/groups/{project}/integrations")
        return [Integration.from_api(x) for x in response["results"]]

    def get(self, project: Project | str, integration_type: IntegrationType | str) -> Integration:
        """
        Returns the settings for configuring integration with one third-party service. These settings apply to all
        databases managed in one MongoDB Cloud project. To use this resource, the requesting API Key must have the
        Organization Owner or Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Third-Party-Integrations/operation/getThirdPartyIntegration>_
        """
        integration_type = parse_integration_type(integration_type)
        response = self.client.get(f"/groups/{project}/integrations/{integration_type.value}")
        return Integration.from_api(response)

    def delete(self, project: Project | str, integration_type: IntegrationType | str) -> None:
        """
        Removes the settings that permit configuring one third-party service integration. These settings apply to all
        databases managed in one MongoDB Cloud project. If you delete an integration from a project, you remove that
        integration configuration only for that project. This action doesn't affect any other project or organization's
        configured {INTEGRATION-TYPE} integrations. To use this resource, the requesting API Key must have the
        Organization Owner or Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Third-Party-Integrations/operation/deleteThirdPartyIntegration>_

        :param project:
        :param integration_type:
        :return:
        """
        integration_type = parse_integration_type(integration_type)
        self.client.delete(f"/groups/{project}/integrations/{integration_type.value}")

    def update(self, project: Project | str, integration: Integration) -> Integration:
        """
        Updates the settings for configuring integration with one third-party service. These settings apply to all
        databases managed in one MongoDB Cloud project. To use this resource, the requesting API Key must have the
        Organization Owner or Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Third-Party-Integrations/operation/updateThirdPartyIntegration>_

        :param project:
        :param integration:
        :return:
        """
        self.client.put(f"/groups/{project}/integrations/{integration.type.value}", json=integration.to_api())
        return self.get(project, integration.type)
