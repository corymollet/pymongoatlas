from enum import Enum
from os import PathLike
from pathlib import Path
from typing import List

from pymongoatlas.clients.common import AcceptHeader, AtlasResourceClient, get_headers
from pymongoatlas.entities.invoices import BillingInvoice, BillingInvoiceIdentifier, CostExplorerQuery
from pymongoatlas.entities.organization import OrganizationIdentifier


class InvoiceReportFormat(Enum):
    CSV = "CSV"
    JSON = "JSON"


class ReportStillProcessing(Exception):
    pass


class InvoicesClient(AtlasResourceClient):
    def list(self, org: OrganizationIdentifier) -> List[BillingInvoice]:
        """
        Returns all invoices that MongoDB issued to the specified organization. This list includes all invoices
        regardless of invoice status. To use this resource, the requesting API Key must have the Organization Billing
        Viewer, Organization Billing Admin, or Organization Owner role. If you have a cross-organization setup, you can
        view linked invoices if you have the Organization Billing Admin or Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Invoices/operation/listInvoices>_

        :param org: ``Organization`` entity or its string id.
        :return: List of ``BillingInvoice`` entities.
        """
        response = self.client.get(f"/orgs/{org}/invoices")
        return [BillingInvoice.from_api(x) for x in response["results"]]

    def list_pending(self, org: OrganizationIdentifier) -> List[BillingInvoice]:
        """
        Returns all invoices accruing charges for the current billing cycle for the specified organization. To use this
        resource, the requesting API Key must have the Organization Billing Viewer, Organization Billing Admin, or
        Organization Owner role. If you have a cross-organization setup, you can view linked invoices if you have the
        Organization Billing Admin or Organization Owner Role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Invoices/operation/listPendingInvoices>_

        :param org: ``Organization`` entity or its string id.
        :return: List of pending ``BillingInvoice`` entities.
        """
        response = self.client.get(f"/orgs/{org}/invoices/pending")
        return [BillingInvoice.from_api(x) for x in response["results"]]

    def get(
        self,
        org: OrganizationIdentifier,
        invoice: BillingInvoiceIdentifier,
        path: PathLike,
        fmt: InvoiceReportFormat | str = InvoiceReportFormat.JSON,
    ) -> None:
        """
        Returns one invoice that MongoDB issued to the specified organization. A unique 24-hexadecimal digit string
        identifies the invoice. You can choose to receive this invoice in JSON or CSV format. To use this resource,
        the requesting API Key must have the Organization Billing Viewer, Organization Billing Admin, or Organization
        Owner role. If you have a cross-organization setup, you can query for a linked invoice if you have the
        Organization Billing Admin or Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Invoices/operation/getInvoice>_

        :param org: ``Organization`` entity or its string id.
        :param invoice: ``BillingInvoice`` entity or its string id.
        :param path: Path to save the downloaded invoice
        :param fmt: Format of the invoice to retrieve (CSV or JSON)
        :return: ``None``
        """
        if isinstance(fmt, str):
            fmt = InvoiceReportFormat(fmt.upper().strip())
        path = Path(path)
        headers = getattr(AcceptHeader, fmt.value)
        with self.client.get_stream(f"/orgs/{org}/invoices/{invoice}", headers=get_headers(headers)) as resp:
            resp.raise_for_status()
            with path.open("wb") as fh:
                fh.write(resp.content)

    def create_cost_explorer_query_process(self, org: OrganizationIdentifier, query: CostExplorerQuery) -> str:
        """
        Creates a query process within the Cost Explorer for the given parameters. A token is returned that can be used
        to poll the status of the query and eventually retrieve the results.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Invoices/operation/createCostExplorerQueryProcess>_

        :param org: ``Organization`` entity or its string id.
        :param query: ``CostExplorerQuery`` entity to create.
        :return: Token used to identify the created Cost Explorer query.
        """
        response = self.client.post(f"/orgs/{org}/billing/costExplorer/usage", json=query.to_api())
        return response["token"]

    def get_cost_explorer_query_report(
        self, org: OrganizationIdentifier, token: str, path: PathLike, fmt: InvoiceReportFormat = InvoiceReportFormat.JSON
    ):
        """
        Download a Cost Explorer report.

        :param org: ``Organization`` entity or its string id.
        :param token: Token used to identify the Cost Explorer query
        :param path: Path to save the downloaded report
        :param fmt: Format of the report (CSV or JSON)
        :return: ``None``
        """
        headers = getattr(AcceptHeader, fmt.value)
        path = Path(path)
        with self.client.get_stream(f"/orgs/{org}/billing/costExplorer/usage/{token}", headers=get_headers(headers)) as resp:
            if resp.status_code == 102:
                raise ReportStillProcessing("Report is still processing, please try again later")
            with path.open("wb") as fh:
                fh.write(resp.content)
