import gzip
import io
import json
import logging
from enum import Enum

import requests
from pydantic import BaseModel, Field
from requests.auth import HTTPDigestAuth
from requests.exceptions import HTTPError

from pymongoatlas.logger import LogFormatter, get_log_formatter_type, setup_logger


class AcceptHeader(Enum):
    CSV = "csv"
    GZIP = "gzip"
    JSON = "json"


class UnexpectedResponse(Exception):
    pass


class Loggable(BaseModel):
    def get_log(self, logger: logging.Logger) -> dict | str:
        log_formatter_type = get_log_formatter_type(logger)
        if log_formatter_type == LogFormatter.JSON:
            return self.model_dump()
        else:
            return str(self)


class DebugRequest(Loggable):
    url: str = Field(description="The requested URL.")
    method: str = Field(description="The HTTP request method.")
    kwargs: dict = Field(default_factory=dict, description="Any keyword arguments passed to a request method")

    def __str__(self):
        return f"{self.method} {self.url}"


class AtlasError(Loggable):
    detail: str = Field(description="Describes the specific conditions or reasons that cause each type of error")
    error: int = Field(description="HTTP status code returned with this error")
    error_code: str = Field(description="Application error code returned with this error")
    parameters: list[str] = Field(description="Parameters used to give more information about the error.")
    reason: str = Field(description="Application error message returned with this error.")
    body: dict = Field(default_factory=dict, description="The request body.")
    raw_response: str = Field(description="Raw text of the response.")
    request: DebugRequest

    def __str__(self):
        return f"{self.request} {self.error} {self.error_code} {self.reason}"


def get_headers(accept_header: AcceptHeader) -> dict[str, str]:
    return {"Accept": f"application/vnd.atlas.2023-11-15+{accept_header.value}"}


def atlas_request(func):
    def wrapper(self, *args, **kwargs):
        request_info = DebugRequest(url=args[0], method=func.__name__.upper(), kwargs=kwargs)
        self.logger.debug(request_info.get_log(self.logger))
        response: requests.Response = func(self, *args, **kwargs)
        body: dict = kwargs.get("data", kwargs.get("json", {}))
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            try:
                data = response.json()
            except json.JSONDecodeError:
                data = {
                    "body": body,
                    "url": response.url,
                    "error": response.status_code,
                    "detail": "",
                    "errorCode": "",
                    "parameters": [],
                    "reason": "",
                }
            atlas_error = AtlasError(
                detail=data.get("detail", ""),
                error=data["error"],
                error_code=data.get("errorCode", ""),
                parameters=data.get("parameters", []),
                reason=data.get("reason", ""),
                raw_response=response.text,
                body=body,
                request=request_info,
            )
            self.logger.error(atlas_error.get_log(self.logger))
            raise http_err
        try:
            data = response.json()
            if isinstance(data, dict):
                data["_meta"] = {"status_code": response.status_code}
            return data
        except JSONDecodeError:
            return response

    return wrapper


class AtlasBaseClient:
    def __init__(self, public_key: str, private_key: str, log_level=logging.INFO) -> None:
        self.session = requests.Session()
        self.session.auth = HTTPDigestAuth(public_key, private_key)
        self.session.headers.update(get_headers(AcceptHeader.JSON))
        self.base_url = "https://cloud.mongodb.com/api/atlas/v2"
        self.logger = setup_logger(level=log_level)

    @atlas_request
    def get(self, endpoint: str, **kwargs):
        return self.session.get(url=f"{self.base_url}{endpoint}", **kwargs)

    def paginate(self, endpoint: str, items_per_page: int = 100, **kwargs):
        page_num = 1

        while True:
            params = {"pageNum": page_num, "itemsPerPage": items_per_page}
            params.update(kwargs.get("params", {}))
            response = self.get(endpoint, params=params)
            if not isinstance(response, dict):
                raise UnexpectedResponse(f"Unexpected pagination response: {response}")
            count = response.get("count", response.get("totalCount"))

            yield response

            page_num += 1
            if page_num > (count / items_per_page) + 1:
                break

    def get_stream(self, endpoint: str, **kwargs):
        return self.session.get(url=f"{self.base_url}{endpoint}", stream=True, **kwargs)

    def stream_gzipped_logs(self, endpoint: str, **kwargs):
        """
        Stream gzipped-logs download endpoints as JSON lines (instead of downloading to a file).

        :param endpoint: Endpoint to fetch.
        :param kwargs: Any additional keyword arguments to pass to ``requests.get``
        :return: Generator of dictionaries
        """
        with self.session.get(url=f"{self.base_url}{endpoint}", stream=True, headers=get_headers(AcceptHeader.GZIP), **kwargs) as req:
            req.raise_for_status()
            decompressed = gzip.GzipFile(fileobj=io.BytesIO(req.content))
            yield from (json.loads(line.decode("utf-8")) for line in decompressed)

    @atlas_request
    def post(self, endpoint: str, **kwargs):
        return self.session.post(url=f"{self.base_url}{endpoint}", **kwargs)

    @atlas_request
    def delete(self, endpoint: str, **kwargs):
        return self.session.delete(url=f"{self.base_url}{endpoint}", **kwargs)

    @atlas_request
    def patch(self, endpoint: str, **kwargs):
        return self.session.patch(url=f"{self.base_url}{endpoint}", **kwargs)

    @atlas_request
    def put(self, endpoint: str, **kwargs):
        return self.session.put(url=f"{self.base_url}{endpoint}", **kwargs)


class AtlasResourceClient:
    def __init__(self, client: AtlasBaseClient, logger_name: str, log_level=logging.INFO) -> None:
        self.client = client
        self.logger = setup_logger(name=f"pymongoatlas.clients.{logger_name}", level=log_level)
