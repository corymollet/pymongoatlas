import typing
from collections.abc import Generator
from datetime import datetime
from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.alerts import Alert, AlertConfiguration, AlertIdentifier, AlertStatus
from pymongoatlas.entities.projects import ProjectIdentifier


class AlertConfigurationClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> Generator[AlertConfiguration, None, None]:
        """
        Returns all alert configurations for one project. These alert configurations apply to any component in the project.
        Alert configurations define the triggers and notification methods for alerts. To use this resource, the requesting API Key
        must have the Project Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/listAlertConfigurations>_
        """
        for page in self.client.paginate(f"/groups/{project}/alertConfigs", params={"includeCount": "true"}):
            yield from (AlertConfiguration.from_api(x) for x in page["results"])

    def list_by_alert_id(self, project: ProjectIdentifier, alert: AlertIdentifier) -> List[AlertConfiguration]:
        """
        Returns all alert configurations set for the specified alert. To use this resource, the requesting API Key must have the Project Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/listAlertConfigurationsByAlertId>_
        """
        response = self.client.get(f"/groups/{project}/alerts/{alert}/alertConfigs")
        return [AlertConfiguration.from_api(x) for x in response["results"]]

    def list_matchers_field_names(self) -> List[str]:
        """
        Get all field names that the ``matchers.fieldName`` parameter accepts when you create or update an Alert Configuration.
        You can successfully call this endpoint with any assigned role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/listAlertConfigurationMatchersFieldNames>_
        """
        response = self.client.get("/alertConfigs/matchers/fieldNames")
        return response

    def create(self, project: ProjectIdentifier, alert_config: AlertConfiguration) -> AlertConfiguration:
        """
        Creates one alert configuration for the specified project. Alert configurations define the triggers and notification methods for alerts. To use this resource, the requesting API Key must have the Organization Owner or Project Owner role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/createAlertConfiguration>_
        """
        response = self.client.post(f"/groups/{project}/alertConfigs", json=alert_config.to_api())
        return AlertConfiguration.from_api(response)

    def delete(self, project: ProjectIdentifier, alert_config: AlertConfiguration) -> int:
        """
        Removes one alert configuration from the specified project. To use this resource, the requesting API Key must have the Organization Owner or Project Owner role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/deleteAlertConfiguration>_
        """
        response = self.client.delete(f"/groups/{project}/alertConfigs/{alert_config.id}")
        return response

    def get(self, project: ProjectIdentifier, alert_config_id: str) -> AlertConfiguration:
        """
        Returns the specified alert configuration from the specified project. To use this resource, the requesting API Key must have the Project Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/getAlertConfiguration>_
        """
        response = self.client.get(f"/groups/{project}/alertConfigs/{alert_config_id}")
        return AlertConfiguration.from_api(response)

    def toggle(self, project: ProjectIdentifier, alert_config: AlertConfiguration, enabled: bool):
        """
        Enables or disables the specified alert configuration in the specified project. The resource enables the
        specified alert configuration if currently enabled. The resource disables the specified alert configuration if
        currently disabled. To use this resource, the requesting API Key must have the Organization Owner or Project
        Owner role.

        NOTE: This endpoint updates only the enabled/disabled state for the alert configuration. To update more than
        just this configuration, see Update One Alert Configuration.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/toggleAlertConfiguration>_
        """
        response = self.client.patch(f"/groups/{project}/alertConfigs/{alert_config.id}", json={"enabled": enabled})
        return response

    def update(self, project: ProjectIdentifier, alert_config: AlertConfiguration) -> AlertConfiguration:
        """
        Updates one alert configuration in the specified project. Alert configurations define the triggers and
        notification methods for alerts. To use this resource, the requesting API Key must have the Organization Owner
        or Project Owner role.

        NOTE: To enable or disable the alert configuration, see Toggle One State of One Alert Configuration in One Project.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alert-Configurations/operation/toggleAlertConfiguration>_
        """
        response = self.client.put(f"/groups/{project}/alertConfigs/{alert_config.id}", json=alert_config.to_api())
        return AlertConfiguration.from_api(response)


class AlertsClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier, status: AlertStatus | None = None) -> typing.Generator[Alert, None, None]:
        """
        Returns all alerts. These alerts apply to all components in one project. You receive an alert when a monitored
        component meets or exceeds a value you set. To use this resource, the requesting API Key must have the Project
        Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alerts/operation/listAlerts>_
        """
        params = {"includeCount": "true"}
        if status is not None:
            params["status"] = status.value
        for page in self.client.paginate(f"/groups/{project}/alerts", params=params):
            yield from (Alert.from_api(x) for x in page["results"])

    def list_by_alert_configuration_id(self, project: ProjectIdentifier, alert_config: AlertConfiguration):
        """
        Returns all open alerts that the specified alert configuration triggers. These alert configurations apply to the
        specified project only. Alert configurations define the triggers and notification methods for alerts. Open
        alerts have been triggered but remain unacknowledged. To use this resource, the requesting API Key must have the
        Project Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alerts/operation/listAlertsByAlertConfigurationId>_
        """
        response = self.client.get(f"/groups/{project}/alertConfigs/{alert_config.id}/alerts")
        return response

    def get(self, project: ProjectIdentifier, alert_id: str) -> Alert:
        """
        Returns one alert. This alert applies to any component in one project. You receive an alert when a monitored
        component meets or exceeds a value you set. To use this resource, the requesting API Key must have the Project
        Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alerts/operation/getAlert>_
        """
        response = self.client.get(f"/groups/{project}/alerts/{alert_id}")
        return Alert.from_api(response)

    def acknowledge(self, project: ProjectIdentifier, alert: AlertIdentifier, until: datetime, comment: str = "") -> Alert:
        """
        Confirms receipt of one existing alert. This alert applies to any component in one project. Acknowledging an
        alert prevents successive notifications. You receive an alert when a monitored component meets or exceeds a
        value you set until you acknowledge the alert. To use this resource, the requesting API Key must have the
        Organization Owner or Project Owner role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Alerts/operation/acknowledgeAlert>_
        """
        response = self.client.patch(
            f"/groups/{project}/alerts/{alert}", json={"acknowledgedUntil": until.isoformat(), "acknowledgementComment": comment}
        )
        return Alert.from_api(response)
