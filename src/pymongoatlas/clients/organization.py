from typing import List

from pydantic import BaseModel

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.auth import ApiKey, AtlasOrgRole, AtlasUser, AtlasUserTypeAlias
from pymongoatlas.entities.federation import FederationSettingsIdentifier
from pymongoatlas.entities.invoices import BillingInvoice
from pymongoatlas.entities.organization import (
    Organization,
    OrganizationIdentifier,
    OrganizationInvite,
    OrganizationInviteIdentifier,
    OrganizationSettings,
)
from pymongoatlas.entities.projects import Project


class OrganizationNotFound(Exception):
    pass


class CreatedOrg(BaseModel):
    api_key: ApiKey | None = None
    org: Organization

    @classmethod
    def from_api(cls, response: dict) -> "CreatedOrg":
        org = cls(org=Organization.from_api(response["organization"]))
        if response.get("apiKey"):
            org.api_key = ApiKey.from_api(response["apiKey"])
        return org


class OrganizationClient(AtlasResourceClient):
    def list(self) -> List[Organization]:
        """
        Returns all organizations to which the requesting API Key has access. To use this resource, the requesting API
        Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/listOrganizations>_

        :return: List of ``Organization`` entities.
        """
        response = self.client.get("/orgs")
        return [Organization.from_api(x) for x in response["results"]]

    def get(self, org_id: str) -> Organization:
        """
        Returns one organization to which the requesting API key has access. To use this resource, the requesting API
        Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/getOrganization>_

        :param org_id: ID of the Organization to fetch.
        :return: ``Organization`` entity.
        """
        response = self.client.get(f"/orgs/{org_id}")
        return Organization.from_api(response)

    def create(
        self,
        name: str,
        org_owner: AtlasUserTypeAlias,
        api_key: ApiKey | None = None,
        federation_settings: FederationSettingsIdentifier | None = None,
    ) -> CreatedOrg:
        """
        Creates one organization in MongoDB Cloud and links it to the requesting API Key's organization. To use this
        resource, the requesting API Key must have the Organization Owner role. The requesting API Key's organization
        must be a paying organization. To learn more, see
        `Configure a Paying Organization <https://www.mongodb.com/docs/atlas/billing/#configure-a-paying-organization>`_
        in the MongoDB Atlas documentation.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/createOrganization>_

        :param name: Name of the organization to create.
        :param org_owner: ``AtlasUser`` entity or the string id of the MongoDB Cloud user that you want to
                          assign the Organization Owner role. This user must be a member of the same organization as the
                          calling API key. If you provide ``federation_settings``, this user must instead have the
                          Organization Owner role on an organization in the specified federation.
        :param api_key: Optional organization API key to create for this org.
        :param federation_settings: Optional ``FederationSettings`` entity or its string id to link the newly created
                                    org to. If specified, the proposed Organization Owner of the new organization must
                                    have the Organization Owner role in an organization associated with the federation.
        :return: Created ``Organization`` and optional ``ApiKey`` entities.
        """
        payload = {"name": name, "orgOwnerId": str(org_owner)}
        if api_key is not None:
            payload["apiKey"] = api_key.to_api(role_type=AtlasOrgRole)
        if federation_settings is not None:
            payload["federationSettingsId"] = str(federation_settings)
        response = self.client.post("/orgs", json=payload)
        return CreatedOrg.from_api(response)

    def delete(self, org: OrganizationIdentifier) -> None:
        """
        Removes one specified organization. MongoDB Cloud imposes the following limits on this resource:

        - Organizations with active projects cannot be removed.
        - All projects in the organization must be removed before you can remove the organization.

        To use this resource, the requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/deleteOrganization>_

        :param org: ``Organization`` entity or its string id to delete
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}")

    def rename(self, org: OrganizationIdentifier, name: str) -> Organization:
        """
        Renames one organization. To use this resource, the requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/renameOrganization>_

        :param org: ``Organization`` entity or its string id to rename.
        :param name: The new name.
        :return: Renamed ``Organization`` entity.
        """
        response = self.client.patch(f"/orgs/{org}", json={"name": name})
        return Organization.from_api(response)

    def get_by_name(self, name: str, orgs: List[Organization] | None = None, case_sensitive: bool = False) -> Organization:
        """
        Search for an organization from its name.

        :param name: Name to search.
        :param orgs: Optional list of ``Organization`` entities. If omitted, will search every org to which the
                     requesting API key has access.
        :param case_sensitive: Whether to consider string case when searching.
        :return: ``Organization`` entity
        :raises: ``OrganizationNotFound``, if no match is found.
        """
        if orgs is None:
            orgs = self.list()
        if case_sensitive:
            org = next((x for x in orgs if x.name.strip() == name.strip()), None)
        else:
            org = next((x for x in orgs if x.name.strip().lower() == name.strip().lower()), None)
        if org is None:
            raise OrganizationNotFound(f"Organization named {name} not found")
        return org

    def list_projects(self, org: OrganizationIdentifier) -> List[Project]:
        """
        Returns multiple projects in the specified organization. Each organization can have multiple projects.

        Use projects to:

        - Isolate different environments, such as development, test, or production environments, from each other.
        - Associate different MongoDB Cloud users or teams with different environments, or give different permission to
          MongoDB Cloud users in different environments.
        - Maintain separate cluster security configurations.
        - Create different alert settings.

        To use this resource, the requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/listOrganizationProjects>_

        :param org: ``Organization`` entity or its string id.
        :return: List of ``Project`` entities belonging to the org.
        """
        response = self.client.get(f"/orgs/{org}/groups")
        return [Project.from_api(x) for x in response["results"]]

    def list_invitations(self, org: OrganizationIdentifier) -> List[OrganizationInvite]:
        """
        Returns all pending invitations to the specified organization. To use this resource, the requesting API Key must
        have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/listOrganizationInvitations>_

        :param org: ``Organization`` entity or its string id.
        :return: List of ``OrganizationInvite`` entities.
        """
        response = self.client.get(f"/orgs/{org}/invites")
        return [OrganizationInvite.from_api(x) for x in response]

    def update_invitation(self, org: OrganizationIdentifier, invitation: OrganizationInvite) -> OrganizationInvite:
        """
        Updates the details of one pending invitation to the specified organization. To specify which invitation,
        provide the username of the invited user. To use this resource, the requesting API Key must have the
        Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/updateOrganizationInvitation>_

        :param org: ``Organization`` entity or its string id
        :param invitation: ``OrganizationInvite`` entity with updated data.
        :return: Updated ``OrganizationInvite`` entity.
        """
        response = self.client.patch(f"/orgs/{org}/invites", json=invitation.to_api())
        return OrganizationInvite.from_api(response)

    def invite_user(self, org: OrganizationIdentifier, invitation: OrganizationInvite):
        """
        Invites one MongoDB Cloud user to join the specified organization. The user must accept the invitation to access
        information within the specified organization. To use this resource, the requesting API Key must have the
        Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/createOrganizationInvitation>_

        :param org: ``Organization`` entity or its string id
        :param invitation: ``OrganizationInvite`` entity.
        :return: Created ``OrganizationInvite`` entity.
        """
        response = self.client.post(f"/orgs/{org}/invites", json=invitation.to_api())
        return OrganizationInvite.from_api(response)

    def cancel_invitation(self, org: OrganizationIdentifier, invitation: OrganizationInviteIdentifier) -> None:
        """
        Cancels one pending invitation sent to the specified MongoDB Cloud user to join an organization. You can't
        cancel an invitation that the user accepted. To use this resource, the requesting API Key must have the
        Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/deleteOrganizationInvitation>-

        :param org: ``Organization`` entity or its string id
        :param invitation: ``OrganizationInvite`` entity or its string id to cancel
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}/invites/{invitation}")

    def list_users(self, org: OrganizationIdentifier) -> List[AtlasUser]:
        """
        Returns details about the MongoDB Cloud users associated with the specified organization. Each MongoDB Cloud
        user returned must belong to the specified organization or to a project within the specified organization. To
        use this resource, the requesting API Key must have the Organization Member role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/listOrganizationUsers>_

        :param org: ``Organization`` entity or its string id.
        :return: List of ``AtlasUser`` entities associated with the org.
        """
        response = self.client.get(f"/orgs/{org}/users")
        return [AtlasUser.from_api(x) for x in response["results"]]

    def remove_user(self, org: OrganizationIdentifier, user: AtlasUserTypeAlias) -> None:
        """
        Removes one MongoDB Cloud user from the specified organization. To use this resource, the requesting API Key
        must have the Organization User Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/removeOrganizationUser>_

        :param org: ``Organization`` entity or its string id
        :param user: ``AtlasUser`` entity or its string id to remove
        :return: ``None``
        """
        self.client.delete(f"/orgs/{org}/users/{user}")

    def update_user_roles(self, org: OrganizationIdentifier, user: AtlasUserTypeAlias, roles: List[AtlasOrgRole | str]):
        """
        Updates the roles of the specified user in the specified organization. To specify the user to update, provide
        the unique 24-hexadecimal digit string that identifies the user in the specified organization. To use this
        resource, the requesting API Key must have the Organization User Admin role.

        :param org: ``Organization`` entity or its string id.
        :param user: ``AtlasUser`` entity or its string id to update.
        :param roles: List of roles to set for the user.
        :return: Updated list of roles associated to this user
        """
        roles = [AtlasOrgRole(x) for x in roles]
        response = self.client.put(f"/orgs/{org}/users/{user}/roles", json=[x.value for x in roles])
        return [AtlasOrgRole(x) for x in response["orgRoles"]]

    def list_invoices(self, org: OrganizationIdentifier) -> List[BillingInvoice]:
        """
        Returns all invoices that MongoDB issued to the specified organization. This list includes all invoices
        regardless of invoice status. To use this resource, the requesting API Key must have the Organization Billing
        Viewer, Organization Billing Admin, or Organization Owner role. If you have a cross-organization setup, you can
        view linked invoices if you have the Organization Billing Admin or Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Invoices/operation/listInvoices>_

        :param org: ``Organization`` entity or its string id.
        :return: List of ``BillingInvoice`` entities.
        """
        response = self.client.get(f"/orgs/{org}/invoices")
        return response

    def get_settings(self, org: OrganizationIdentifier) -> OrganizationSettings:
        """
        Returns details about the specified organization's settings. To use this resource, the requesting API Key must
        have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/getOrganizationSettings>_

        :param org: ``Organization`` entity or its string id
        :return: ``OrganizationSettings`` entity
        """
        response = self.client.get(f"/orgs/{org}/settings")
        return OrganizationSettings.from_api(response)

    def update_settings(self, org: OrganizationIdentifier, settings: OrganizationSettings) -> OrganizationSettings:
        """
        Updates the organization's settings. To use this resource, the requesting API Key must have the Organization
        Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Organizations/operation/updateOrganizationSettings>_

        :param org: ``Organization`` entity or its string id
        :param settings: ``OrganizationSettings`` entity with updated data
        :return: Updated ``OrganizationSettings`` entity
        """
        response = self.client.patch(f"/orgs/{org}/settings", json=settings.to_api())
        return OrganizationSettings.from_api(response)
