from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.cluster import ClusterIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier
from pymongoatlas.entities.search import AtlasSearchIndex, AtlasSearchIndexIdentifier, SearchDeploymentNode


class AtlasSearchClient(AtlasResourceClient):
    def create_index(self, project: ProjectIdentifier, cluster: ClusterIdentifier, search_index: AtlasSearchIndex) -> AtlasSearchIndex:
        """
        Creates one Atlas Search index on the specified collection. Atlas Search indexes define the fields on which to
        create the index and the analyzers to use when creating the index. Only clusters running MongoDB v4.2 or later
        can use Atlas Search. To use this resource, the requesting API Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/createAtlasSearchIndex>_

        :param project: Project (entity or id) in which to create the search index
        :param cluster: Cluster (entity or name) that contains the collection on which to create an Atlas Search index.
        :param search_index: Search index data
        :return: The created search index
        """
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/fts/indexes", json=search_index.to_api())
        return AtlasSearchIndex.from_api(response)

    def list_indexes(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, database: str, collection: str
    ) -> List[AtlasSearchIndex]:
        """
        Returns all Atlas Search indexes on the specified collection. Atlas Search indexes contain the indexed fields
        and the analyzers used to create the indexes. To use this resource, the requesting API Key must have the Project
        Data Access Read Write role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/listAtlasSearchIndexes>_


        :param project: Project (entity or id) containing one or more Atlas Search indexes.
        :param cluster: Cluster (entity or id) that contains one or more Atlas Search indexes.
        :param database: Human-readable label that identifies the database that contains the collection with one or more
                         Atlas Search indexes.
        :param collection: Name of the collection that contains one or more Atlas Search indexes.
        :return: List of ``AtlasSearchIndex`` entities
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/fts/indexes/{database}/{collection}")
        return [AtlasSearchIndex.from_api(x) for x in response]

    def delete_index(self, project: ProjectIdentifier, cluster: ClusterIdentifier, search_index: AtlasSearchIndexIdentifier) -> None:
        """
        Removes one Atlas Search index that you identified with its unique ID. To use this resource, the requesting API
        Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/deleteAtlasSearchIndex>_

        :param project: Project (entity or id) containing the Atlas Search Index
        :param cluster: Cluster (entity or name) containing the Atlas Search Index
        :param search_index: The Atlas Search Index (entity or id) to delete
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/clusters/{cluster}/fts/indexes/{search_index}")

    def get(self, project: ProjectIdentifier, cluster: ClusterIdentifier, index_id: str) -> AtlasSearchIndex:
        """
        Returns one Atlas Search index in the specified project. You identify this index using its unique ID. Atlas
        Search index contains the indexed fields and the analyzers used to create the index. To use this resource, the
        requesting API Key must have the Project Data Access Read Write role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/getAtlasSearchIndex>_

        :param project: Project (entity or id) containing the Atlas Search index to retrieve
        :param cluster: Cluster (entity or id) containing the Atlas Search index to retrieve
        :param index_id: ID of the index to retrieve
        :return: ``AtlasSearchIndex`` entity
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/fts/indexes/{index_id}")
        return AtlasSearchIndex.from_api(response)

    def update_index(self, project: ProjectIdentifier, cluster: ClusterIdentifier, search_index: AtlasSearchIndex) -> AtlasSearchIndex:
        """
        Updates one Atlas Search index that you identified with its unique ID. Atlas Search indexes define the fields on
        which to create the index and the analyzers to use when creating the index. To use this resource, the
        requesting API Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/updateAtlasSearchIndex>_

        :param project: Project (entity or id) containing the Atlas Search index to update
        :param cluster: Cluster (entity or id) containing the Atlas Search index to update
        :param search_index: ``AtlasSearchIndex`` entity with updated data
        :return: The updated search index
        """
        response = self.client.patch(f"/groups/{project}/clusters/{cluster}/fts/indexes/{search_index}", json=search_index.to_api())
        return AtlasSearchIndex.from_api(response)

    def delete_nodes(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> None:
        """
        Deletes the search nodes for the specified cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/deleteAtlasSearchDeployment>_

        :param project: Project (entity or id) containing the search nodes to delete
        :param cluster: Cluster (entity or id) containing the search nodes to delete
        """
        self.client.delete(f"/groups/{project}/clusters/{cluster}/search/deployment")

    def get_nodes(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> SearchDeploymentNode:
        """
        Return the search nodes for the specified cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/getAtlasSearchDeployment>_

        :param project: Project (entity or id) containing the search nodes to retrieve
        :param cluster: Cluster (entity or id) containing the search nodes to retrieve
        :return: Search nodes entity
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/search/deployment")
        return SearchDeploymentNode.from_api(response)

    def update_nodes(self, project: ProjectIdentifier, cluster: ClusterIdentifier, nodes: SearchDeploymentNode) -> SearchDeploymentNode:
        """
        Updates the search nodes for the specified cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/getAtlasSearchDeployment>_

        :param project: Project (entity or id) containing the search nodes to update
        :param cluster: Cluster (entity or id) containing the search nodes to update
        :param nodes: ``SearchDeploymentNode`` entity with updated data
        :return: Updated ``SearchDeploymentNode`` entity
        """
        response = self.client.patch(f"/groups/{project}/clusters/{cluster}/search/deployment", json=nodes.to_api())
        return SearchDeploymentNode.from_api(response)

    def create_nodes(self, project: ProjectIdentifier, cluster: ClusterIdentifier, nodes: SearchDeploymentNode) -> SearchDeploymentNode:
        """
        Create search nodes for the specified cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Atlas-Search/operation/getAtlasSearchDeployment>_

        :param project: Project (entity or id) in which to create the search nodes
        :param cluster: Cluster (entity or id) in which to create the search nodes
        :param nodes: ``SearchDeploymentNode`` entity with updated data
        :return: Created ``SearchDeploymentNode`` entity
        """
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/search/deployment", json=nodes.to_api())
        return SearchDeploymentNode.from_api(response)
