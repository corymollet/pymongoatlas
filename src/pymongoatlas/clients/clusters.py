from typing import Iterable, List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.cloud_providers import CloudProviderName
from pymongoatlas.entities.cluster import (
    ChangeStatus,
    CloudProvider,
    Cluster,
    ClusterIdentifier,
    CustomZoneMapping,
    GlobalClusterManagedNamespace,
    OutageSimulation,
    OutageSimulationFilter,
    SampleDatasetLoadStatus,
    SampleDatasetLoadStatusIdentifier,
    ServerlessInstance,
)
from pymongoatlas.entities.projects import Project, ProjectIdentifier
from pymongoatlas.util import remove_none_values


class ClustersClient(AtlasResourceClient):
    def get(self, project: ProjectIdentifier, cluster_name: str) -> Cluster:
        """
        Returns the details for one cluster in the specified project. Clusters contain a group of hosts that maintain
        the same data set. The response includes multi-cloud clusters. To use this resource, the requesting API Key must
        have the Project Read Only role.

        :param project: ``Project`` entity or its string id.
        :param cluster_name: Name of the cluster to fetch.
        :return: ``Cluster`` entity.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster_name}")
        return Cluster.from_api(response)

    def list(self, project: ProjectIdentifier) -> List[Project]:
        """
        Returns the details for all clusters in the specific project to which you have access. Clusters contain a group
        of hosts that maintain the same data set. The response includes multi-cloud clusters. To use this resource,
        the requesting API Key must have the Project Read Only role. This feature is not available for serverless
        clusters.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/listClusters>_

        :param project: ``Project`` entity or its string id.
        :return: Generator of ``Cluster`` entities.
        """
        response = self.client.get(f"/groups/{project}/clusters")
        return [Cluster.from_api(x) for x in response["results"]]

    def create(self, project: ProjectIdentifier, cluster: Cluster) -> Cluster:
        """
        Creates one cluster in the specified project. Clusters contain a group of hosts that maintain the same data set.
        This resource can create multi-cloud clusters. Each project supports up to 25 database deployments. To use this
        resource, the requesting API Key must have the Project Owner role. This feature is not available for serverless
        clusters.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/createCluster>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity to create.
        :return: Created ``Cluster`` entity.
        """
        response = self.client.post(f"/groups/{project}/clusters", json=cluster.to_api())
        return Cluster.from_api(response)

    def list_cloud_provider_regions(
        self, project: ProjectIdentifier, cloud_provider_names: Iterable[CloudProviderName | str]
    ) -> List[CloudProvider]:
        """
        Returns the list of regions available for the specified cloud provider at the specified tier. To use this
        resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/listCloudProviderRegions>_

        :param project: ``Project`` entity or its string id.
        :param cloud_provider_names: One or more ``CloudProviderNames`` for which to fetch regions.
        :return: List of ``CloudProvider`` entities
        """
        params = {"providers": [CloudProviderName(x).value for x in cloud_provider_names]}
        response = self.client.get(f"/groups/{project}/clusters/provider/regions", params=params)
        return [CloudProvider.from_api(x) for x in response["results"]]

    def upgrade_shared_tier_cluster(self, project: ProjectIdentifier, cluster: Cluster) -> Cluster:
        """
        Upgrades a shared-tier cluster in the specified project. To use this resource, the requesting API key must have
        the Project Cluster Manager role. Each project supports up to 25 clusters.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/upgradeSharedCluster>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity to upgrade
        :return: Upgraded ``Cluster`` entity.
        """
        response = self.client.post(f"/groups/{project}/clusters/tenantUpgrade", json=cluster.to_api())
        return Cluster.from_api(response)

    def upgrade_shared_tier_cluster_to_serverless(self, project: ProjectIdentifier, instance: ServerlessInstance) -> ServerlessInstance:
        """
        Upgrades a shared-tier cluster to a serverless instance in the specified project. To use this resource, the
        requesting API key must have the Project Cluster Manager role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/upgradeSharedClusterToServerless>_

        :param project: ``Project`` entity or its string id.
        :param instance: Details of the shared-tier cluster upgrade.
        :return: ``ServerlessInstance`` entity.
        """
        response = self.client.post(f"/groups/{project}/clusters/tenantUpgradeToServerless", json=instance.to_api())
        return ServerlessInstance.from_api(response)

    def delete(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> None:
        """
        Removes one cluster from the specified project. The cluster must have termination protection disabled in order
        to be deleted. To use this resource, the requesting API Key must have the Project Owner role. This feature is
        not available for serverless clusters.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/deleteCluster>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/clusters/{cluster}")

    def update(self, project: ProjectIdentifier, cluster: Cluster) -> Cluster:
        """
        Updates the details for one cluster in the specified project. Clusters contain a group of hosts that maintain
        the same data set. This resource can update multi-cloud clusters. To update a cluster's termination protection,
        the requesting API Key must have the Project Owner role. For all other updates, the requesting API Key must have
        the Project Cluster Manager role. You can't modify a paused cluster (paused : true). You must call this endpoint
        to set paused : false. After this endpoint responds with paused : false, you can call it again with the changes
        you want to make to the cluster. This feature is not available for serverless clusters.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/updateCluster>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity with updated data.
        :return: Updated ``Cluster`` entity.
        """
        response = self.client.patch(f"/groups/{project}/clusters/{cluster}", json=cluster.to_api())
        return Cluster.from_api(response)

    def test_failover(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> None:
        """
        Starts a failover test for the specified cluster in the specified project. Clusters contain a group of hosts
        that maintain the same data set. A failover test checks how MongoDB Cloud handles the failure of the cluster's
        primary node. During the test, MongoDB Cloud shuts down the primary node and elects a new primary. To use this
        resource, the requesting API Key must have the Project Cluster Manager role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/testFailover>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :return: ``None``
        """
        self.client.post(f"/groups/{project}/clusters/{cluster}/restartPrimaries")

    def get_status(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> ChangeStatus:
        """
        Returns the status of all changes that you made to the specified cluster in the specified project. Use this
        resource to check the progress MongoDB Cloud has made in processing your changes. The response does not include
        the deployment of new dedicated clusters. To use this resource, the requesting API Key must have the Project
        Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/getClusterStatus>

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: ``ChangeStatus`` Enum.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/status")
        return ChangeStatus(response["changeStatus"])

    def load_sample_dataset(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> SampleDatasetLoadStatus:
        """
        Requests loading the MongoDB sample dataset into the specified cluster. To use this resource, the requesting API
        Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/loadSampleDataset>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name in which to load the sample dataset.
        :return: ``SampleDatasetLoadStatus`` entity.
        """
        response = self.client.post(f"/groups/{project}/sampleDatasetLoad/{cluster}")
        return SampleDatasetLoadStatus.from_api(response)

    def check_sample_dataset_request(
        self, project: ProjectIdentifier, sample_dataset: SampleDatasetLoadStatusIdentifier
    ) -> SampleDatasetLoadStatus:
        """
        Checks the progress of loading the sample dataset into one cluster. To use this resource, the requesting API Key
        must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Clusters/operation/getSampleDatasetLoadStatus>_

        :param project: ``Project`` entity or its string id.
        :param sample_dataset: ``SampleDatasetLoadStatus`` entity or its string id.
        :return: ``SampleDatasetLoadStatus`` entity.
        """
        response = self.client.get(f"/groups/{project}/sampleDatasetLoad/{sample_dataset}")
        return SampleDatasetLoadStatus.from_api(response)

    def get_aws_custom_dns(self, project: ProjectIdentifier) -> bool:
        """
        Returns the custom DNS configuration for AWS clusters in the specified project. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/AWS-Clusters-DNS/operation/getAWSCustomDNS>_

        :param project: ``Project`` entity or its string id.
        :return: Flag that indicates whether the project's clusters deployed to Amazon Web Services (AWS) use a custom
                 Domain Name System (DNS). When ``True``, connect to your cluster using Private IP for Peering
                 connection strings. To learn more, see `FAQ: Connection String Options <https://www.mongodb.com/docs/atlas/reference/faq/connection-changes/#how-does-this-affect-aws-vpc-peering-when-i-use-custom-dns->`_
        """
        response = self.client.get(f"/groups/{project}/awsCustomDNS")
        return response["enabled"]

    def get_outage_simulation(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> OutageSimulation:
        """
        Returns one outage simulation for one cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cluster-Outage-Simulation/operation/getOutageSimulation>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: ``OutageSimulation`` entity.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/outageSimulation")
        return response

    def end_outage_simulation(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> OutageSimulation:
        """
        Ends a cluster outage simulation.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cluster-Outage-Simulation>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: ``OutageSimulation`` entity.
        """
        response = self.client.delete(f"/groups/{project}/clusters/{cluster}/outageSimulation")
        return OutageSimulation.from_api(response)

    def start_outage_simulation(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, filters: List[OutageSimulationFilter]
    ) -> OutageSimulation:
        """
        Starts a cluster outage simulation.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cluster-Outage-Simulation/operation/startOutageSimulation>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param filters: List of settings that specify the type of cluster outage simulation.
        :return: ``OutageSimulation`` entity.
        """
        response = self.client.post(
            f"/groups/{project}/clusters/{cluster}outageSimulation", json={"outageFilters": [x.to_api() for x in filters]}
        )
        return OutageSimulation.from_api(response)

    def list_federated_databases(self, project: ProjectIdentifier):
        """
        Returns the details of all federated database instances in the specified project. To use this resource, the
        requesting API Key must have the Project Read Only or higher role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/listFederatedDatabases>_
        """
        response = self.client.get(f"/groups/{project}/dataFederation")
        return response

    def list_serverless_instances(self, project: ProjectIdentifier) -> List[ServerlessInstance]:
        """
        Returns details for all serverless instances in the specified project. To use this resource, the requesting API
        Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Instances>_

        :param project:
        :return:
        """
        response = self.client.get(f"/groups/{project}/serverless")
        return [ServerlessInstance.from_api(x) for x in response["results"]]

    def create_serverless_instance(self, project: ProjectIdentifier, instance: ServerlessInstance) -> ServerlessInstance:
        """
        Creates one serverless instance in the specified project. To use this resource, the requesting API Key must have
        the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Instances/operation/createServerlessInstance>_

        :param project:
        :param instance:
        :return:
        """
        response = self.client.post(f"/groups/{project}/serverless", json=instance.to_api())
        return ServerlessInstance.from_api(response)

    def delete_serverless_instance(self, project: ProjectIdentifier, instance: ServerlessInstance | str) -> None:
        """
        Removes one serverless instance from the specified project. The serverless instance must have termination
        protection disabled in order to be deleted. To use this resource, the requesting API Key must have the Project
        Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Instances/operation/deleteServerlessInstance>_

        :param project:
        :param instance:
        :return:
        """
        self.client.delete(f"/groups/{project}/serverless/{instance}")

    def get_serverless_instance(self, project: ProjectIdentifier, instance_name: str) -> ServerlessInstance:
        """
        Returns details for one serverless instance in the specified project. To use this resource, the requesting API
        Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Instances/operation/getServerlessInstance>_

        :param project:
        :param instance_name:
        :return:
        """
        response = self.client.get(f"/groups/{project}/serverless/{instance_name}")
        return ServerlessInstance.from_api(response)

    def update_serverless_instance(self, project: ProjectIdentifier, instance: ServerlessInstance | str) -> ServerlessInstance:
        """
        Updates one serverless instance in the specified project. To use this resource, the requesting API Key must have
        the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Serverless-Instances/operation/updateServerlessInstance>_

        :param project:
        :param instance:
        :return:
        """
        response = self.client.patch(f"/groups/{project}/serverless/{instance}", json=instance.to_api())
        return ServerlessInstance.from_api(response)

    def get_global_cluster_managed_namespace(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> GlobalClusterManagedNamespace:
        """
        Returns one managed namespace within the specified global cluster. A managed namespace identifies a collection
        using the database name, the dot separator, and the collection name. To use this resource, the requesting API
        Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Global-Clusters/operation/getManagedNamespace>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: ``GlobalClusterManagedNamespace`` entity.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/globalWrites")
        return GlobalClusterManagedNamespace.from_api(response)

    def remove_custom_zone_mappings(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> GlobalClusterManagedNamespace:
        """
        Removes all custom zone mappings for the specified global cluster. A custom zone mapping matches one ISO 3166-2
        location code to a zone in your global cluster. Removing the custom zone mappings restores the default mapping.
        By default, MongoDB Cloud maps each location code to the closest geographical zone. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Global-Clusters/operation/deleteAllCustomZoneMappings>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: Updated ``GlobalClusterManagedNamespace`` entity.
        """
        response = self.client.delete(f"/groups/{project}/clusters/{cluster}/globalWrites/customZoneMapping")
        return GlobalClusterManagedNamespace.from_api(response)

    def add_custom_zone_mapping(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, mappings: List[CustomZoneMapping]
    ) -> GlobalClusterManagedNamespace:
        """
        Creates one custom zone mapping for the specified global cluster. A custom zone mapping matches one ISO 3166-2
        location code to a zone in your global cluster. By default, MongoDB Cloud maps each location code to the closest
        geographical zone. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Global-Clusters/operation/createCustomZoneMapping>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param mappings: List of ``CustomZoneMapping`` entities to add.
        :return: Updated ``GlobalClusterManagedNamespace`` entity.
        """
        response = self.client.post(
            f"/groups/{project}/clusters/{cluster}/globalWrites/customZoneMapping",
            json={"customZoneMappings": [x.to_api() for x in mappings]},
        )
        return GlobalClusterManagedNamespace.from_api(response)

    def remove_global_cluster_managed_namespace(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, db: str, collection: str
    ) -> GlobalClusterManagedNamespace:
        """
        Removes one managed namespace within the specified global cluster. A managed namespace identifies a collection
        using the database name, the dot separator, and the collection name. Deleting a managed namespace does not
        remove the associated collection or data. To use this resource, the requesting API Key must have the Project
        Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Global-Clusters/operation/deleteManagedNamespace>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param db: Name of the database.
        :param collection: Name of the collection
        :return: Updated ``GlobalClusterManagedNamespace`` entity.
        """
        response = self.client.delete(
            f"/groups/{project}/clusters/{cluster}/globalWrites/managedNamespace", params={"db": db, "collection": collection}
        )
        return GlobalClusterManagedNamespace.from_api(response)

    def create_global_cluster_managed_namespace(
        self,
        project: ProjectIdentifier,
        cluster: ClusterIdentifier,
        collection: str,
        db: str,
        custom_shard_key: str | None = None,
        is_custom_shard_key_hashed: bool = False,
        is_shard_key_unique: bool = False,
        num_initial_chunks: int | None = None,
        presplit_hashed_zones: bool = False,
    ) -> GlobalClusterManagedNamespace:
        """
        Creates one managed namespace within the specified global cluster. A managed namespace identifies a collection
        using the database name, the dot separator, and the collection name. To use this resource, the requesting API
        Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Global-Clusters/operation/createManagedNamespace>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param collection: Name of the collection to manage for this Global Cluster.
        :param db: Name of the database to manage for this Global Cluster.
        :param custom_shard_key: Database parameter used to divide the collection into shards. Global clusters require a
                                 compound shard key. This compound shard key combines the location parameter and the
                                 user-selected custom key.
        :param is_custom_shard_key_hashed: Flag that indicates whether someone hashed the custom shard key. If this
                                           parameter returns false, this cluster uses ranged sharding.
        :param is_shard_key_unique: Flag that indicates whether the underlying index enforces unique values.
        :param num_initial_chunks: Minimum number of chunks to create initially when sharding an empty collection with a
                                   hashed shard key.
        :param presplit_hashed_zones: Flag that indicates whether MongoDB Cloud should create and distribute initial
                                      chunks for an empty or non-existing collection. MongoDB Cloud distributes data
                                      based on the defined zones and zone ranges for the collection.
        :return: Created ``GlobalClusterManagedNamespace`` entity.
        """
        payload = remove_none_values(
            {
                "collection": collection,
                "customShardKey": custom_shard_key,
                "db": db,
                "isCustomShardKeyHashed": is_custom_shard_key_hashed,
                "isShardKeyUnique": is_shard_key_unique,
                "numInitialChunks": num_initial_chunks,
                "presplitHashedZones": presplit_hashed_zones,
            }
        )
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/globalWrites/managedNamespaces", json=payload)
        return GlobalClusterManagedNamespace.from_api(response)
