import gzip
import io
import json
import math
import os
import typing
from datetime import datetime
from pathlib import Path

from pymongoatlas.clients.common import AcceptHeader, AtlasResourceClient, get_headers
from pymongoatlas.entities.cluster import ClusterIdentifier
from pymongoatlas.entities.monitoring import (
    ClusterView,
    CollectionStatsMetric,
    DatabaseMeasurement,
    DiskMeasurement,
    FTSHostMetric,
    FTSIndexMetric,
    HostMeasurement,
    LogMessage,
    LogName,
    Measurement,
    Process,
    ProcessIdentifier,
    PushBasedLogExport,
    QueryLatency,
)
from pymongoatlas.entities.projects import ProjectIdentifier
from pymongoatlas.entities.search import AtlasSearchIndexIdentifier
from pymongoatlas.util import get_time_period_params


class MonitoringClient(AtlasResourceClient):
    def list_processes(self, project: ProjectIdentifier) -> list[Process]:
        """
        Returns details of all processes for the specified project. A MongoDB process can be either a mongod or mongos.
         To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/listAtlasProcesses>_

        :param project: ``Project`` entity or its string id
        :return: List of ``Process`` entities
        """
        response = self.client.get(f"/groups/{project}/processes")
        return [Process.from_api(x) for x in response.get("results")]

    def get_process(self, project: ProjectIdentifier, process_id: str) -> Process:
        """
        Returns the processes for the specified host for the specified project. To use this resource, the requesting API
        Key must have the Project Read Only role.

        https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/getAtlasProcess>_

        :param project: ``Project`` entity or its string id.
        :param process_id: ID of the process to fetch.
        :return: ``Process`` entity
        """
        response = self.client.get(f"/groups/{project}/processes/{process_id}")
        return Process.from_api(response)

    def get_host_logs(
        self,
        project: ProjectIdentifier,
        process: Process,
        start_date: datetime,
        end_date: datetime,
        log_name: LogName = LogName.MONGODB,
    ) -> typing.Generator[LogMessage, None, None]:
        """
        Returns a compressed (.gz) log file that contains a range of log messages for the specified host for the
        specified project. MongoDB updates process and audit logs from the cluster backend infrastructure every five
        minutes and contain log data from the previous five minutes. If you poll the API for log files, we recommend
        polling every five minutes. For example, if the logs are updated at 4:00 UTC and then you poll the API,
        the API returns log data from the interval between 3:55 UTC and 4:00 UTC. This feature isn't available for M0
        free clusters, M2, M5, or serverless clusters. To use this resource, the requesting API Key must have the
        Project Data Access Read Only or higher role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/getHostLogs>_
        <https://www.mongodb.com/docs/manual/reference/log-messages/>_
        """
        request_data = self._prep_logs_request(
            project=project, process=process, start_date=start_date, end_date=end_date, log_name=log_name
        )

        with self.client.session.get(stream=True, **request_data) as fh:
            fh.raise_for_status()
            decompressed = gzip.GzipFile(fileobj=io.BytesIO(fh.content))
            yield from (LogMessage.from_api(json.loads(line.decode("utf-8"))) for line in decompressed)

    def _prep_logs_request(
        self, project: ProjectIdentifier, process: Process, start_date: datetime, end_date: datetime, log_name: LogName = LogName.MONGODB
    ) -> dict[str, str | dict]:
        params = {}
        if start_date:
            params["startDate"] = math.floor(start_date.timestamp())
        if end_date:
            params["endDate"] = math.ceil(end_date.timestamp())
        return {
            "url": f"{self.client.base_url}/groups/{project}/clusters/{process.hostname}/logs/{log_name.value}.gz",
            "params": params,
            "headers": get_headers(AcceptHeader.GZIP),
        }

    def _get_host_log_filename(
        self,
        path: os.PathLike,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        start_date: datetime,
        end_date: datetime,
        log_name: LogName,
    ):
        date_fmt = "%Y-%m-%dT%H-%M-%S"
        file_path = (
            Path(path)
            / str(project)
            / f"{start_date.strftime(date_fmt)}_{end_date.strftime(date_fmt)}"
            / str(process)
            / f"{log_name.value}.jsonl.gz"
        )
        return file_path

    def download_host_logs(
        self,
        path: os.PathLike,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        start_date: datetime,
        end_date: datetime,
        log_name: LogName = LogName.MONGODB,
    ):
        request_data = self._prep_logs_request(
            project=project, process=process, start_date=start_date, end_date=end_date, log_name=log_name
        )
        file_path = self._get_host_log_filename(
            path=path, project=project, process=process, start_date=start_date, end_date=end_date, log_name=log_name
        )
        file_path.parent.mkdir(parents=True, exist_ok=True)
        with file_path.open("wb") as fh:
            with self.client.session.get(stream=True, **request_data) as req:
                for chunk in req.iter_content(chunk_size=2048):
                    if chunk:
                        fh.write(chunk)

    def list_atlas_search_metrics(
        self,
        *metrics: FTSIndexMetric,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        database: str,
        collection: str,
        granularity: str = "PT10M",
        period: str | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
    ) -> list[Measurement]:
        """
        Returns the measurements of one database for the specified host for the specified project. Returns the
        database's on-disk storage space based on the MongoDB dbStats command output. To calculate some metric series,
        Atlas takes the rate between every two adjacent points. For these metric series, the first data point has a
        null value because Atlas can't calculate a rate for the first data point given the query time range. Atlas
        retrieves database metrics every 20 minutes but reduces frequency when necessary to optimize database
        performance. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/listIndexMetrics>_

        :param metrics: One or more types of metrics that MongoDB Atlas reports for the associated data series.
        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param database: Database name.
        :param collection: Collection name.
        :param granularity: Duration that specifies the interval at which Atlas reports the metrics. This parameter
                            expresses its value in the ISO 8601 duration format in UTC.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do not
                    set ``period``.
        """
        params = {"granularity": granularity}
        if metrics:
            params["metrics"] = [x.value for x in metrics]
        params.update(get_time_period_params(period=period, start=start, end=end))
        response = self.client.get(f"/groups/{project}/hosts/{process}/fts/metrics/indexes/{database}/{collection}/measurements")
        return [Measurement.from_api(x) for x in response["indexStatMeasurements"]]

    def get_atlas_search_metrics(
        self,
        *metrics: FTSIndexMetric,
        project: ProjectIdentifier,
        index: AtlasSearchIndexIdentifier,
        process: ProcessIdentifier,
        database: str,
        collection: str,
        granularity: str = "PT10M",
        period: str | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
    ) -> list[Measurement]:
        """
        Returns the measurements of one database for the specified host for the specified project. Returns the
        database's on-disk storage space based on the MongoDB dbStats command output. To calculate some metric series,
        Atlas takes the rate between every two adjacent points. For these metric series, the first data point has a
        null value because Atlas can't calculate a rate for the first data point given the query time range. Atlas
        retrieves database metrics every 20 minutes but reduces frequency when necessary to optimize database
        performance. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/getIndexMetrics>_

        :param metrics: One or more types of metrics that MongoDB Atlas reports for the associated data series.
        :param project: ``Project`` entity or its string id.
        :param index: ``AtlasSearchIndex`` entity or its string name.
        :param process: ``Process`` entity or its string id.
        :param database: Database name.
        :param collection: Collection name.
        :param granularity: Duration that specifies the interval at which Atlas reports the metrics. This parameter
                            expresses its value in the ISO 8601 duration format in UTC.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do not
                    set ``period``.
        """
        params = {"granularity": granularity}
        if metrics:
            params["metrics"] = [x.value for x in metrics]
        params.update(get_time_period_params(period=period, start=start, end=end))
        response = self.client.get(f"/groups/{project}/hosts/{process}/fts/metrics/indexes/{database}/{collection}/{index}/measurements")
        return [Measurement.from_api(x) for x in response["indexStatMeasurements"]]

    def list_atlas_search_hardware_metrics(
        self,
        *metrics: FTSHostMetric,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        granularity: str = "PT10M",
        period: str | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
    ) -> list[Measurement]:
        """
        Returns the Atlas Search hardware and status data series within the provided time range for one process in the
        specified project. You must have the Project Read Only or higher role to view the Atlas Search metric types.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/getDatabaseMeasurements>_

        :param metrics: One or more types of metrics that MongoDB Atlas reports for the associated data series.
        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param granularity: Duration that specifies the interval at which Atlas reports the metrics. This parameter
                            expresses its value in the ISO 8601 duration format in UTC.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do not
                    set ``period``.
        """
        params = {"granularity": granularity}
        if metrics:
            params["metrics"] = [x.value for x in metrics]
        params.update(get_time_period_params(period=period, start=start, end=end))
        response = self.client.get(f"/groups/{project}/hosts/{process}/fts/metrics/measurements")
        return [Measurement.from_api(x) for x in response["hardwareMeasurements"]]

    def list_databases(self, project: ProjectIdentifier, process: ProcessIdentifier) -> list[str]:
        """
        Returns the list of databases running on the specified host for the specified project. M0 free clusters, M2,
        M5, and serverless clusters have some operational limits. The MongoDB Cloud process must be a mongod. To use
        this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/listDatabases>_

        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        """
        response = self.client.get(f"/groups/{project}/processes/{process}/databases")
        return [x["databaseName"] for x in response["results"]]

    def get_database_measurements(
        self,
        *measurements: DatabaseMeasurement,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        database: str,
        granularity: str = "PT10M",
        period: str | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
    ) -> list[Measurement]:
        """
        Returns the measurements of one database for the specified host for the specified project. Returns the
        database's on-disk storage space based on the MongoDB dbStats command output. To calculate some metric series,
        Atlas takes the rate between every two adjacent points. For these metric series, the first data point has a
        null value because Atlas can't calculate a rate for the first data point given the query time range. Atlas
        retrieves database metrics every 20 minutes but reduces frequency when necessary to optimize database
        performance. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/getDatabaseMeasurements>_

        :param measurements: One or more types of measurement to request for this MongoDB process. If omitted, the
                             resource returns all measurements.
        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param database: Database name.
        :param granularity: Duration that specifies the interval at which Atlas reports the metrics. This parameter
                            expresses its value in the ISO 8601 duration format in UTC.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do not
                    set ``period``.
        """
        params = {"granularity": granularity}
        if measurements:
            params["m"] = [x.value for x in measurements]
        if any(x is not None for x in (period, start, end)):
            params.update(get_time_period_params(period=period, start=start, end=end))
        response = self.client.get(f"/groups/{project}/processes/{process}/databases/{database}/measurements", params=params)
        return [Measurement.from_api(x) for x in response["measurements"]]

    def list_disk_partitions(self, project: ProjectIdentifier, process: ProcessIdentifier) -> list[str]:
        """
        Returns the list of disks or partitions for the specified host for the specified project. To use this resource,
        the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/listDiskPartitions>_

        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :return: List of disk partition names.
        """
        response = self.client.get(f"/groups/{project}/processes/{process}/disks")
        return [x["partitionName"] for x in response["results"]]

    def list_disk_measurements(
        self,
        *measurements: DiskMeasurement,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        partition: str,
        granularity: str = "PT10M",
        period: str | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
    ) -> list[Measurement]:
        """
        Returns the measurements of one disk or partition for the specified host for the specified project. Returned
        value can be one of the following:

        - Throughput of I/O operations for the disk partition used for the MongoDB process
        - Percentage of time during which requests the partition issued and serviced
        - Latency per operation type of the disk partition used for the MongoDB process
        - Amount of free and used disk space on the disk partition used for the MongoDB process

        To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/listDiskMeasurements>_

        :param measurements: One or more types of measurement to request for this MongoDB process. If omitted, the
                             resource returns all measurements.
        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param partition: Disk partition name.
        :param granularity: Duration that specifies the interval at which Atlas reports the metrics. This parameter
                            expresses its value in the ISO 8601 duration format in UTC.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do not
                    set ``period``.
        """
        params = {"granularity": granularity}
        if measurements:
            params["m"] = [x.value for x in measurements]
        params.update(get_time_period_params(period=period, start=start, end=end))
        response = self.client.get(f"/groups/{project}/processes/{process}/disks/{partition}/measurements", params=params)
        return [Measurement.from_api(x) for x in response["measurements"]]

    def get_host_measurements(
        self,
        *measurements: HostMeasurement,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        granularity: str = "PT10M",
        period: str | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
    ) -> list[Measurement]:
        """
        Returns disk, partition, or host measurements per process for the specified host for the specified project.
        Returned value can be one of the following:

        - Throughput of I/O operations for the disk partition used for the MongoDB process
        - Percentage of time during which requests the partition issued and serviced
        - Latency per operation type of the disk partition used for the MongoDB process
        - Amount of free and used disk space on the disk partition used for the MongoDB process
        - Measurements for the host, such as CPU usage or number of I/O operations

        To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Monitoring-and-Logs/operation/getHostMeasurements>_

        :param measurements: One or more types of measurement to request for this MongoDB process. If omitted, the
                             resource returns all measurements.
        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param granularity: Duration that specifies the interval at which Atlas reports the metrics. This parameter
                            expresses its value in the ISO 8601 duration format in UTC.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do not
                    set ``period``.
        """
        params = {"granularity": granularity}
        if measurements:
            params["m"] = [x.value for x in measurements]
        params.update(get_time_period_params(period=period, start=start, end=end))
        response = self.client.get(f"/groups/{project}/processes/{process}/measurements", params=params)
        return [Measurement.from_api(x) for x in response["measurements"]]

    def disable_push_based_log_export(self, project: ProjectIdentifier) -> None:
        """
        Disables the push-based log export feature by resetting the project level settings to its default configuration.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Push-Based-Log-Export/operation/deletePushBasedLogConfiguration>_

        :param project:
        :return:
        """
        self.client.delete(f"/groups/{project}/pushBasedLogExport")

    def get_push_based_log_export(self, project: ProjectIdentifier) -> PushBasedLogExport:
        """
        Fetches the current project level settings for the push-based log export feature.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Push-Based-Log-Export/operation/getPushBasedLogConfiguration>_

        :param project:
        :return:
        """
        response = self.client.get(f"/groups/{project}/pushBasedLogExport")
        return PushBasedLogExport.from_api(response)

    def update_push_based_log_export(self, project: ProjectIdentifier, push_based_log_export: PushBasedLogExport) -> None:
        """
        Updates the project level settings for the push-based log export feature.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Push-Based-Log-Export/operation/updatePushBasedLogConfiguration>_

        :param project:
        :param push_based_log_export:
        :return:
        """
        self.client.patch(f"/groups/{project}/pushBasedLogExport", data=push_based_log_export.to_api())

    def enable_push_based_log_export(self, project: ProjectIdentifier, push_based_log_export: PushBasedLogExport) -> None:
        """
        Configures the project level settings for the push-based log export feature.

        :param project:
        :param push_based_log_export:
        :return:
        """
        self.client.post(f"/groups/{project}/pushBasedLogExport", data=push_based_log_export.to_api())

    def list_pinned_namespaces(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> list[str]:
        """
        Returns a list of given cluster's pinned namespaces, a set of namespaces manually selected by users to collect
        query latency metrics on.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getPinnedNamespaces>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :return: List of pinned namespaces.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/collStats/pinned")
        return response["pinnedNamespaces"]

    def add_pinned_namespace(self, project: ProjectIdentifier, cluster: ClusterIdentifier, namespaces: list[str]) -> list[str]:
        """
        Add provided list of namespaces to existing pinned namespaces list for collection-level latency metrics
        collection for the given Group and Cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getPinnedNamespaces>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :param namespaces: Dotted combination of ``database.collection`` namespaces to pin for query latency metric
                           collection.
        :return: List of pinned namespaces.
        """
        response = self.client.patch(f"/groups/{project}/clusters/{cluster}/collStats/pinned", json={"namespaces": namespaces})
        return response["pinnedNamespaces"]

    def pin_namespaces(self, project: ProjectIdentifier, cluster: ClusterIdentifier, namespaces: list[str]) -> list[str]:
        """
        Pin provided list of namespaces for collection-level latency metrics collection for the given Group and Cluster.
        This initializes a pinned namespaces list or replaces any existing pinned namespaces list for the Group and
        Cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/pinNamespacesPut>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :param namespaces: Dotted combination of ``database.collection`` namespaces to pin for query latency metric
                           collection.
        :return: List of pinned namespaces.
        """
        response = self.client.put(f"/groups/{project}/clusters/{cluster}/collStats/pinned", json={"namespaces": namespaces})
        return response["pinnedNamespaces"]

    def unpin_namespaces(self, project: ProjectIdentifier, cluster: ClusterIdentifier, namespaces: list[str]) -> list[str]:
        """
        Unpin provided list of namespaces for collection-level latency metrics collection for the given Group and
        Cluster.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/unpinNamespaces>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :param namespaces: Dotted combination of ``database.collection`` namespaces to unpin.
        :return: List of pinned namespaces.
        """
        response = self.client.patch(f"/groups/{project}/clusters/{cluster}/collStats/unpin", json={"namespaces": namespaces})
        return response["pinnedNamespaces"]

    def list_ranked_namespaces_by_cluster(
        self,
        project: ProjectIdentifier,
        cluster: ClusterIdentifier,
        cluster_view: ClusterView | str,
        start: datetime | None = None,
        end: datetime | None = None,
        period: str | None = None,
    ) -> list[str]:
        """
        Return the subset of namespaces from the given cluster sorted by highest total execution time (descending)
        within the given time window.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getCollStatsLatencyNamespacesForCluster>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :param cluster_view: Human-readable label that identifies the cluster topology to retrieve metrics for.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :return: Ordered list of the hottest namespaces, highest value first.
        """
        params = get_time_period_params(period=period, start=start, end=end)
        cluster_view = ClusterView(cluster_view)
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/{cluster_view.value}/collStats/namespaces", params=params)
        return response["rankedNamespaces"]

    def list_ranked_namespaces_by_process(
        self,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        start: datetime | None = None,
        end: datetime | None = None,
        period: str | None = None,
    ) -> list[str]:
        """
        Return the subset of namespaces from the given process ranked by highest total execution time (descending) within
        the given time window.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getCollStatsLatencyNamespaceMetrics>_

        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :return: Ordered list of the hottest namespaces, highest value first.
        """
        params = get_time_period_params(period=period, start=start, end=end)
        response = self.client.get(f"/groups/{project}/processes/{process}/collStats/namespaces", params=params)
        return response["rankedNamespaces"]

    def get_cluster_query_latency(
        self,
        project: ProjectIdentifier,
        cluster: ClusterIdentifier,
        cluster_view: ClusterView | str,
        database: str,
        collection: str,
        metrics: list[CollectionStatsMetric] | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
        period: str | None = None,
    ) -> QueryLatency:
        """
        Get a list of the Coll Stats Latency cluster-level measurements for the given namespace.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getCollStatsLatencyNamespacesForCluster>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its name.
        :param cluster_view: Human-readable label that identifies the cluster topology to retrieve metrics for.
        :param database: Database name.
        :param collection: Collection name.
        :param metrics: Optional list of ``CollectionStatsMetrics`` to retrieve. If omitted, returns all.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :return: ``QueryLatency`` entity.
        """
        params = get_time_period_params(period=period, start=start, end=end)
        if metrics is not None:
            params["metrics"] = [x.value for x in metrics]
        cluster_view = ClusterView(cluster_view)
        response = self.client.get(
            f"/groups/{project}/clusters/{cluster}/{cluster_view.value}/{database}/{collection}/collStats/measurements", params=params
        )
        return QueryLatency.from_api(response)

    def list_ranked_namespaces_by_host(
        self,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        start: datetime | None = None,
        end: datetime | None = None,
        period: str | None = None,
    ) -> list[str]:
        """
        Return the subset of namespaces from the given process ranked by highest total execution time (descending)
        within the given time window.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getCollStatsLatencyNamespacesForHost>_

        :param project: ``Project`` entity or its string id.
        :param process: ``Process`` entity or its string id.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :return: Ordered list of the hottest namespaces, highest value first.
        """
        params = get_time_period_params(period=period, start=start, end=end)
        response = self.client.get(f"/groups/{project}/processes/{process}/collStats/namespaces", params=params)
        return response["rankedNamespaces"]

    def get_host_query_latency(
        self,
        project: ProjectIdentifier,
        process: ProcessIdentifier,
        database: str,
        collection: str,
        metrics: list[CollectionStatsMetric] | None = None,
        start: datetime | None = None,
        end: datetime | None = None,
        period: str | None = None,
    ) -> QueryLatency:
        """
        Get a list of the Coll Stats Latency process-level measurements for the given namespace.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Collection-Level-Metrics/operation/getCollStatsLatencyNamespaceHostMeasurements>_

        :param project: ``Project`` entity or its string id.
        :param process: ``Process``  entity or its string id.
        :param database: Database name.
        :param collection: Collection name.
        :param metrics: Optional list of ``CollectionStatsMetrics`` to retrieve. If omitted, returns all.
        :param start: Date and time when MongoDB Cloud begins reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param end: Date and time when MongoDB Cloud stops reporting the metrics. Include this parameter when you do
                      not set ``period``.
        :param period: Duration over which Atlas reports the metrics. This parameter expresses its value in the ISO 8601
                       duration format in UTC. Include this parameter when you do not set ``start`` and ``end``.
        :return: ``QueryLatency`` entity.
        """
        params = get_time_period_params(period=period, start=start, end=end)
        if metrics is not None:
            params["metrics"] = [x.value for x in metrics]
        response = self.client.get(f"/groups/{project}/processes/{process}/{database}/{collection}/collStats/measurements", params=params)
        return QueryLatency.from_api(response)
