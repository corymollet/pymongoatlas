from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.ldap import LDAPConfig, LDAPConfigVerification, LDAPConfigVerificationIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier


class LDAPConfigClient(AtlasResourceClient):
    def get(self, project: ProjectIdentifier) -> LDAPConfig:
        """
        Returns the current LDAP configuration for the specified project. To use this resource, the requesting API Key
        must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/LDAP-Configuration/operation/getLDAPConfiguration>_

        :param project: ``Project`` entity or its string id.
        :return: ``LDAPConfig`` entity.
        """
        response = self.client.get(f"/groups/{project}/userSecurity")
        return LDAPConfig.from_api(response)

    def update(self, project: ProjectIdentifier, ldap_config: LDAPConfig) -> LDAPConfig:
        """
        Edits the LDAP configuration for the specified project. To use this resource, the requesting API Key must have
        the Project Owner role.

        Updating this configuration triggers a rolling restart of the database.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/LDAP-Configuration/operation/saveLDAPConfiguration>_

        :param project: ``Project`` entity or its string id.
        :param ldap_config: ``LDAPConfig`` entity with updated data.
        :return: Updated ``LDAPConfig`` entity.
        """
        response = self.client.patch(f"/groups/{project}/userSecurity", json=ldap_config.to_api())
        return LDAPConfig.from_api(response)

    def remove_current_user_mapping(self, project: ProjectIdentifier) -> LDAPConfig:
        """
        Removes the current LDAP Distinguished Name mapping captured in the userToDNMapping document from the LDAP
        configuration for the specified project. To use this resource, the requesting API Key must have the Project
        Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/LDAP-Configuration/operation/deleteLDAPConfiguration>_

        :param project: ``Project`` entity or its string id.
        :return: Updated ``LDAPConfig`` entity.
        """
        response = self.client.delete(f"/groups/{project}/userSecurity/ldap/userToDNMapping")
        return LDAPConfig.from_api(response)

    def verify(self, project: ProjectIdentifier, ldap_config: LDAPConfig) -> LDAPConfigVerification:
        """
        Verifies the LDAP configuration for the specified project. To use this resource, the requesting API Key must
        have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/LDAP-Configuration/operation/verifyLDAPConfiguration>_

        :param project: ``Project`` entity or its string id.
        :param ldap_config: ``LDAPConfig`` entity to verify.
        :return: ``LDAPConfigVerification`` entity.
        """
        response = self.client.post(f"/groups/{project}/userSecurity/ldap/verify", json=ldap_config.to_api())
        return LDAPConfigVerification.from_api(response)

    def get_verification_status(
        self, project: ProjectIdentifier, verification_request: LDAPConfigVerificationIdentifier
    ) -> LDAPConfigVerification:
        """
        Returns the status of one request to verify one LDAP configuration for the specified project. To use this
        resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/LDAP-Configuration/operation/getLDAPConfigurationStatus>_

        :param project: ``Project`` entity or its string id.
        :param verification_request: ``LDAPConfigVerificationIdentifier`` or its ``request_id`` string.
        :return: ``LDAPConfigVerification`` entity.
        """
        response = self.client.get(f"/groups/{project}/userSecurity/ldap/verify/{verification_request}")
        return LDAPConfigVerification.from_api(response)
