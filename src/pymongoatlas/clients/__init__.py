import logging
import os

from pymongoatlas.clients.access_log import AccessLogClient
from pymongoatlas.clients.alerts import AlertConfigurationClient, AlertsClient
from pymongoatlas.clients.auditing import AuditingClient
from pymongoatlas.clients.auth import (
    ApiKeysClient,
    AtlasUsersClient,
    CustomDatabaseRolesClient,
    DatabaseUsersClient,
    X509AuthenticationClient,
)
from pymongoatlas.clients.backup import BackupsClient
from pymongoatlas.clients.cloud_provider_access import CloudProviderAccessClient
from pymongoatlas.clients.clusters import ClustersClient
from pymongoatlas.clients.common import AtlasBaseClient
from pymongoatlas.clients.data_federation import DataFederationClient
from pymongoatlas.clients.data_lake_pipelines import DataLakePipelinesClient
from pymongoatlas.clients.encryption import EncryptionClient
from pymongoatlas.clients.events import EventsClient
from pymongoatlas.clients.federation import FederationClient
from pymongoatlas.clients.integrations import IntegrationsClient
from pymongoatlas.clients.invoices import InvoicesClient
from pymongoatlas.clients.ldap import LDAPConfigClient
from pymongoatlas.clients.monitoring import MonitoringClient
from pymongoatlas.clients.networking import NetworkClient
from pymongoatlas.clients.organization import OrganizationClient
from pymongoatlas.clients.performance_advisor import PerformanceAdvisorClient
from pymongoatlas.clients.projects import MaintenanceWindowClient, ProjectsClient
from pymongoatlas.clients.search import AtlasSearchClient
from pymongoatlas.clients.streams import StreamsClient
from pymongoatlas.clients.teams import TeamsClient


class AtlasClient:
    def __init__(self, public_key: str | None = None, private_key: str | None = None, log_level=logging.INFO) -> None:
        if public_key is None:
            public_key = os.environ["MONGODB_ATLAS_PUBLIC_KEY"]
        if private_key is None:
            private_key = os.environ["MONGODB_ATLAS_PRIVATE_KEY"]
        self.client = AtlasBaseClient(public_key=public_key, private_key=private_key, log_level=log_level)

        self.access_logs = AccessLogClient(self.client, "access_logs")
        self.alerts = AlertsClient(self.client, "alerts")
        self.alert_configurations = AlertConfigurationClient(self.client, "alert_configurations")
        self.api_keys = ApiKeysClient(self.client, "api_keys")
        self.atlas_search = AtlasSearchClient(self.client, "atlas_search")
        self.atlas_users = AtlasUsersClient(self.client, "atlas_users")
        self.auditing = AuditingClient(self.client, "auditing")
        self.backups = BackupsClient(self.client, "backups")
        self.clusters = ClustersClient(self.client, "clusters")
        self.cloud_provider_access = CloudProviderAccessClient(self.client, "cloud_provider_access")
        self.custom_database_roles = CustomDatabaseRolesClient(self.client, "custom_database_roles")
        self.database_users = DatabaseUsersClient(self.client, "database_users")
        self.data_federation = DataFederationClient(self.client, "data_federation")
        self.data_lake_pipelines = DataLakePipelinesClient(self.client, "data_lake_pipelines")
        self.encryption = EncryptionClient(self.client, "encryption")
        self.events = EventsClient(self.client, "events")
        self.federation = FederationClient(self.client, "federation")
        self.integrations = IntegrationsClient(self.client, "integrations")
        self.invoices = InvoicesClient(self.client, "invoices")
        self.ldap = LDAPConfigClient(self.client, "ldap")
        self.maintenance_windows = MaintenanceWindowClient(self.client, "maintenance_windows")
        self.monitoring = MonitoringClient(self.client, "monitoring")
        self.networking = NetworkClient(self.client, "networking")
        self.organizations = OrganizationClient(self.client, "organizations")
        self.performance_advisor = PerformanceAdvisorClient(self.client, "performance_advisor")
        self.projects = ProjectsClient(self.client, "projects")
        self.streams = StreamsClient(self.client, "streams")
        self.teams = TeamsClient(self.client, "teams")
        self.x509_auth = X509AuthenticationClient(self.client, "x509_auth")

    def get_system_status(self) -> dict[str, str | dict | list]:
        """
        This resource returns information about the MongoDB application along with API key metadata.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Root/operation/getSystemStatus>_

        :return: System status
        """
        return self.client.get("")

    def list_control_plane_ip_addresses(self):
        return self.client.get("/unauth/controlPlaneIPAddresses")
