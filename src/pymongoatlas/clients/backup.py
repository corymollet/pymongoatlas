from datetime import datetime
from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.backup import (
    BackupCompliancePolicy,
    BackupOnlineArchive,
    BackupOnlineArchiveIdentifier,
    BackupTenantSnapshot,
    BackupTenantSnapshotRestore,
    CloudBackupSchedule,
    DeliveryType,
    DiskBackupExportJob,
    DiskBackupSnapshotAWSExportBucket,
    DiskBackupSnapshotRestoreJob,
    DiskBackupSnapshotRestoreJobIdentifier,
    ReplicaSetSnapshot,
    ShardedClusterSnapshot,
)
from pymongoatlas.entities.cluster import ClusterIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier


class BackupsClient(AtlasResourceClient):
    def list_export_buckets(self, project: ProjectIdentifier) -> List[DiskBackupSnapshotAWSExportBucket]:
        """
        Returns all AWS S3 buckets associated with the specified project. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/listExportBuckets>_

        :param project: Project from which to retrieve the list of export buckets.
        :return: List of ``DiskBackupSnapshotAWSExportBucket`` entities.
        """
        response = self.client.get(f"/groups/{project}/backup/exportBuckets")
        return [DiskBackupSnapshotAWSExportBucket.from_api(x) for x in response["results"]]

    def create_export_bucket(
        self, project: ProjectIdentifier, export_bucket: DiskBackupSnapshotAWSExportBucket
    ) -> DiskBackupSnapshotAWSExportBucket:
        """
        Grants MongoDB Cloud access to the specified AWS S3 bucket. This enables this bucket to receive Atlas Cloud
        Backup snapshots. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/createExportBucket>_

        :param project: ``Project`` entity or its string id in which to create the export bucket.
        :param export_bucket: ``DiskBackupSnapshotAWSExportBucket`` entity to create.
        :return: Created ``DiskBackupSnapshotAWSExportBucket`` entity.
        """
        response = self.client.post(f"/groups/{project}/backup/exportBuckets", data=export_bucket.to_api())
        return DiskBackupSnapshotAWSExportBucket.from_api(response)

    def delete_export_bucket(self, project: ProjectIdentifier, export_bucket: DiskBackupSnapshotAWSExportBucket) -> None:
        """
        Revoke MongoDB Cloud access to the specified AWS S3 bucket. This prevents this bucket to receive Atlas Cloud
        Backup snapshots. Auto export must be disabled on all clusters in this project exporting to this bucket before
        revoking access. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/deleteExportBucket>_

        :param project: ``Project`` entity or its string id from which to revoke access to the export bucket
        :param export_bucket: ``DiskBackupSnapshotAWSExportBucket`` entity to remove from the project
        """
        self.client.delete(f"/groups/{project}/backup/exportBuckets/{export_bucket.id}")

    def get_export_bucket(self, project: ProjectIdentifier, export_bucket_id: str) -> DiskBackupSnapshotAWSExportBucket:
        """
        Returns one AWS S3 bucket associated with the specified project. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/getExportBucket>_

        :param project: ``Project`` entity or its string id from which to retrieve the export bucket.
        :param export_bucket_id: ID of the export bucket.
        :return: ``DiskBackupSnapshotAWSExportBucket`` entity.
        """
        response = self.client.get(f"/groups/{project}/backup/exportBuckets/{export_bucket_id}")
        return DiskBackupSnapshotAWSExportBucket.from_api(response)

    def get_backup_compliance_policy(self, project: ProjectIdentifier) -> BackupCompliancePolicy:
        """
        Returns the Backup Compliance Policy settings with the specified project. To use this resource, the requesting
        API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/getDataProtectionSettings>_

        :param project: Project from which to retrieve the backup compliance policy settings.
        :return: ``BackupCompliancePolicy`` entity.
        """
        response = self.client.get(f"/groups/{project}/backupCompliancePolicy")
        return BackupCompliancePolicy.from_api(response)

    def update_backup_compliance_policy(self, project: ProjectIdentifier, backup_policy: BackupCompliancePolicy) -> BackupCompliancePolicy:
        """
        Updates the Backup Compliance Policy settings for the specified project. To use this resource, the requesting
        API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/updateDataProtectionSettings>_

        :param project: ``Project`` entity or its string id in which to update the backup compliance policy settings.
        :param backup_policy: ``BackupCompliancePolicy`` entity with updated data.
        :return: Updated ``BackupCompliancePolicy`` entity.
        """
        response = self.client.put(f"/groups/{project}/backupCompliancePolicy", data=backup_policy.to_api())
        return BackupCompliancePolicy.from_api(response)

    def list_export_backup_jobs(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> List[DiskBackupExportJob]:
        """
        Returns all Cloud Backup snapshot export jobs associated with the specified Atlas cluster. To use this resource,
        the requesting API Key must have the Project Atlas Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/listBackupExportJobs>_

        :param project: Project from which to list the export backup jobs
        :param cluster: Cluster from which to list the export backup jobs
        :return: List of ``DiskBackupExportJob`` entities.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/exports")
        return [DiskBackupExportJob.from_api(x) for x in response["results"]]

    def create_export_backup_job(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, job: DiskBackupExportJob
    ) -> DiskBackupExportJob:
        """
        Exports one backup snapshot for dedicated Atlas cluster using Cloud Backups to an AWS bucket. To use this
        resource, the requesting API Key must have the Project Atlas Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/createBackupExportJob>_

        :param project: ``Project`` entity or its string id from which to create the export backup job.
        :param cluster: ``Cluster`` entity or its string name from which to create the export backup job.
        :param job: ``DiskBackupExportJob`` entity to create.
        :return: Created ``DiskBackupExportJob`` entity.
        """
        response = self.client.post(f"/groups/{project}/cluster/{cluster}/backup/exports", data=job.to_api())
        return DiskBackupExportJob.from_api(response)

    def get_export_backup_job(self, project: ProjectIdentifier, cluster: ClusterIdentifier, export_id: str) -> DiskBackupExportJob:
        """
        Returns one Cloud Backup snapshot export job associated with the specified Atlas cluster. To use this resource,
        the requesting API Key must have the Project Atlas Admin role.

        :param project: ``Project`` entity or its string id from which to retrieve the export backup job.
        :param cluster: ``Cluster`` entity or its string name from which to retrieve the export backup job.
        :param export_id: ID of the export job to retrieve.
        :return: ``DiskBackupExportJob`` entity.
        """
        response = self.client.get(f"/groups/{project}/cluster/{cluster.name}/backup/exports/{export_id}")
        return DiskBackupExportJob.from_api(response)

    def list_backup_restore_jobs(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> list[DiskBackupSnapshotRestoreJob]:
        """
        Returns all cloud backup restore jobs for one cluster from the specified project. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/listBackupRestoreJobs>_

        :param project: Project (id) for which to list the restore jobs.
        :param cluster: Cluster (name) for which to list the restore jobs.
        :return: List of ``DiskBackupSnapshotRestoreJob`` entities.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/restoreJobs")
        return [DiskBackupSnapshotRestoreJob.from_api(x) for x in response["results"]]

    def create_backup_restore_job(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, restore_job: DiskBackupSnapshotRestoreJob
    ) -> DiskBackupSnapshotRestoreJob:
        """
        Restores one snapshot of one cluster from the specified project. Atlas takes on-demand snapshots immediately and
        scheduled snapshots at regular intervals. If an on-demand snapshot with a status of queued or inProgress exists,
        before taking another snapshot, wait until Atlas completes processing the previously taken on-demand
        snapshot.

        To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/createBackupRestoreJob>_

        :param project: ``Project`` entity or its string id for which to create the backup restore job.
        :param cluster: ``Cluster`` entity or its string name for which to create the backup restore job.
        :param restore_job: ``DiskBackupSnapshotRestoreJob`` entity to create.
        :return: Created ``DiskBackupSnapshotRestoreJob`` entity.
        """
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/backup/restoreJobs", data=restore_job.to_api())
        return DiskBackupSnapshotRestoreJob.from_api(response)

    def cancel_backup_restore_job(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, restore_job: DiskBackupSnapshotRestoreJobIdentifier
    ) -> None:
        """
        Cancels one cloud backup restore job of one cluster from the specified project. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/cancelBackupRestoreJob>_

        :param project: ``Project`` entity or its string id of the restore job to cancel.
        :param cluster: ``Cluster`` entity or its string name of the restore job to cancel.
        :param restore_job: Restore job to cancel
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/clusters/{cluster}/backups/restoreJobs/{restore_job}")

    def get_backup_restore_job(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, restore_job_id: str
    ) -> DiskBackupSnapshotRestoreJob:
        """
        Returns one cloud backup restore job for one cluster from the specified project. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/getBackupRestoreJob>_

        :param project: ``Project`` entity or its string id of the restore job to retrieve.
        :param cluster: ``Cluster`` entity or its string name of the restore job to retrieve.
        :param restore_job_id: ID of the restore job.
        :return: ``DiskBackupSnapshotRestoreJob`` entity.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backups/restoreJobs/{restore_job_id}")
        return DiskBackupSnapshotRestoreJob.from_api(response)

    def delete_all_backup_schedules(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> None:
        """
        Removes all cloud backup schedules for the specified cluster. This schedule defines when MongoDB Cloud takes
        scheduled snapshots and how long it stores those snapshots. To use this resource, the requesting API Key must
        have the Project Atlas Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/deleteAllBackupSchedules>_

        :param project: ``Project`` entity or its string id from which to delete all backup schedules.
        :param cluster: ``Cluster`` entity or its string name from which to delete all backup schedules.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/clusters/{cluster}/backup/schedule")

    def get_backup_schedule(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> CloudBackupSchedule:
        """
        Returns the cloud backup schedule for the specified cluster within the specified project. This schedule defines
        when MongoDB Cloud takes scheduled snapshots and how long it stores those snapshots. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/getBackupSchedule>_

        :param project: ``Project`` entity or its string id from which to retrieve the backup schedule.
        :param cluster: ``Cluster`` entity or its string name from which to retrieve the backup schedule.
        :return: ``CloudBackupSchedule`` entity.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/schedule")
        return CloudBackupSchedule.from_api(response)

    def list_replica_set_backups(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> list[ReplicaSetSnapshot]:
        """
        Returns all snapshots of one cluster from the specified project. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/listReplicaSetBackups>_
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/snapshots")
        return [ReplicaSetSnapshot.from_api(x) for x in response["results"]]

    def list_sharded_cluster_backups(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> list[ShardedClusterSnapshot]:
        """
        Returns all snapshots of one sharded cluster from the specified project. To use this resource, the requesting
        API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/listShardedClusterBackups>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: List of``ShardedClusterSnapshot`` entities.
        """
        response = self.client.get(f"/groups/{project.id}/clusters/{cluster.name}/backup/snapshots/shardedClusters")
        return [ShardedClusterSnapshot.from_api(x) for x in response["results"]]

    def list_online_archives(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> list[BackupOnlineArchive]:
        """
        Returns details of all online archives. This archive stores data from one cluster within one project. To use
        this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Online-Archive/operation/listOnlineArchives>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :return: List of ``BackupOnlineArchive`` entities.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/onlineArchives")
        return [BackupOnlineArchive.from_api(x) for x in response["results"]]

    def create_online_archive(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, online_archive: BackupOnlineArchive
    ) -> BackupOnlineArchive:
        """
        Creates one online archive. This archive stores data from one cluster within one project. To use this resource,
        the requesting API Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Online-Archive/operation/createOnlineArchive>_

        :param project: ``Project`` entity or its string id.
        :param cluster:  ``Cluster`` entity or its string name.
        :param online_archive: ``BackupOnlineArchive`` entity to create.
        :return: Created ``BackupOnlineArchive`` entity.
        """
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/onlineArchives", data=online_archive.to_api())
        return BackupOnlineArchive.from_api(response)

    def delete_online_archive(self, project: ProjectIdentifier, cluster: ClusterIdentifier, archive: BackupOnlineArchiveIdentifier) -> None:
        """
        Removes one online archive. This archive stores data from one cluster within one project. To use this resource,
        the requesting API Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Online-Archive/operation/deleteOnlineArchive>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param archive: ``BackupOnlineArchive`` entity or its string id.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/clusters/{cluster}/onlineArchives/{archive}")

    def get_online_archive(self, project: ProjectIdentifier, cluster: ClusterIdentifier, archive_id: str) -> BackupOnlineArchive:
        """
        Returns one online archive for one cluster. This archive stores data from one cluster within one project. To use
        this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Online-Archive/operation/getOnlineArchive>_

        :param project: ``Project`` entity or its string id
        :param cluster: ``Cluster`` entity or its string name
        :param archive_id: ID of the online archive to fetch
        :return: ``BackupOnlineArchive`` entity.
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/onlineArchives/{archive_id}")
        return BackupOnlineArchive.from_api(response)

    def update_online_archive(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, archive: BackupOnlineArchive
    ) -> BackupOnlineArchive:
        """
        Updates, pauses, or resumes one online archive. This archive stores data from one cluster within one project. To
        use this resource, the requesting API Key must have the Project Data Access Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Online-Archive/operation/updateOnlineArchive>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param archive: ``BackupOnlineArchive`` entity with data to update.
        :return: Updated ``BackupOnlineArchive`` entity
        """
        response = self.client.patch(f"/groups/{project}/clusters/{cluster}/onlineArchives/{archive}", data=archive.to_api())
        return BackupOnlineArchive.from_api(response)

    def get_online_archive_query_logs(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, start_date: datetime, end_date: datetime, archive_only: bool = False
    ):
        """
        Downloads query logs for the specified online archive. To use this resource, the requesting API Key must have
        the Project Data Access Read Only or higher role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Online-Archive/operation/downloadOnlineArchiveQueryLogs>_

        :param project: ``Project`` entity or its string id.
        :param cluster: ``Cluster`` entity or its string name.
        :param start_date: Date and time that specifies the starting point for the range of log messages to return.
        :param end_date: Date and time that specifies the end point for the range of log messages to return.
        :param archive_only: Flag that indicates whether to download logs for queries against your online archive only
                             or both your online archive and cluster.
        :return: Generator of JSON lines of logs
        """
        params = {
            "startDate": start_date.timestamp() * 1000,
            "endDate": end_date.timestamp() * 1000,
            "archiveOnly": archive_only,
        }
        return self.client.stream_gzipped_logs(f"/groups/{project}/clusters/{cluster}/onlineArchives/queryLogs.gz", params=params)

    def restore_snapshot(
        self,
        project: ProjectIdentifier,
        cluster: ClusterIdentifier,
        snapshot: ReplicaSetSnapshot | str,
        delivery_type: DeliveryType = DeliveryType.AUTOMATED,
        **kwargs,
    ) -> DiskBackupSnapshotRestoreJob:
        """
        Restores one snapshot of one cluster from the specified project. Atlas takes on-demand snapshots immediately and
        scheduled snapshots at regular intervals. If an on-demand snapshot with a status of queued or inProgress exists,
        before taking another snapshot, wait until Atlas completes processing the previously taken on-demand snapshot.

        To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Backups/operation/listBackupRestoreJobs>_

        :param project: Project (id) containing the cluster snapshot
        :param cluster: Cluster (name) snapshot to restore
        :param snapshot: The snapshot (id) to restore
        :param delivery_type: Type of restore job to create
        :param kwargs: Additional request body key/value pairs (e.g. targetClusterName, targetGroupId, etc.)
        :return:
        """
        body = {"deliveryType": delivery_type.value, "snapshotId": str(snapshot), **kwargs}
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/backup/restoreJobs", data=body)
        return DiskBackupSnapshotRestoreJob.from_api(response)

    def list_shared_tier_restore_jobs(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> List[BackupTenantSnapshotRestore]:
        """
        Returns all restore jobs for the specified M2 or M5 cluster. Restore jobs restore a cluster using a snapshot. To
        use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Shared-Tier-Restore-Jobs/operation/createSharedClusterBackupRestoreJob>_

        :param project:
        :param cluster:
        :return:
        """
        response = self.client.get(f"/groups/{project}/cluster/{cluster}/backup/tenant/restores")
        return [BackupTenantSnapshotRestore.from_api(x) for x in response["results"]]

    def create_shared_tier_restore_job(
        self,
        project: ProjectIdentifier,
        cluster: ClusterIdentifier,
        snapshot: BackupTenantSnapshot | str,
        target_deployment_item_name: str,
        target_project: ProjectIdentifier | None = None,
    ) -> BackupTenantSnapshotRestore:
        """
        Restores the specified cluster. MongoDB Cloud limits which clusters can be the target clusters of a restore. The
        target cluster can't use encryption at rest, run a major release MongoDB version different than the snapshot,
        or receive client requests during restores. MongoDB Cloud deletes all existing data on the target cluster prior
        to the restore operation. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Shared-Tier-Restore-Jobs/operation/createSharedClusterBackupRestoreJob>_

        :param project:
        :param cluster:
        :param snapshot:
        :param target_deployment_item_name:
        :param target_project:
        :return:
        """
        payload = {"snapshotId": str(snapshot), "targetDeploymentItemName": target_deployment_item_name}
        if target_project is not None:
            payload["targetProjectId"] = str(target_project)
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/backup/tenant/restore", data=payload)
        return BackupTenantSnapshotRestore.from_api(response)

    def get_shared_tier_restore_job(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, restore_id: str
    ) -> BackupTenantSnapshotRestore:
        """
        Returns the specified restore job. To use this resource, the requesting API Key must have the Project Read Only
        role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Shared-Tier-Restore-Jobs/operation/getSharedClusterBackupRestoreJob>_

        :param project:
        :param cluster:
        :param restore_id:
        :return:
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/tenant/restores/{restore_id}")
        return BackupTenantSnapshotRestore.from_api(response)

    def list_shared_tier_snapshots(self, project: ProjectIdentifier, cluster: ClusterIdentifier) -> List[BackupTenantSnapshot]:
        """
        Returns details for all snapshots for the specified shared cluster. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Shared-Tier-Snapshots/operation/listSharedClusterBackups>_

        :param project:
        :param cluster:
        :return:
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/tenant/snapshots")
        return [BackupTenantSnapshot.from_api(x) for x in response["results"]]

    def get_shared_tier_snapshot(self, project: ProjectIdentifier, cluster: ClusterIdentifier, snapshot_id: str) -> BackupTenantSnapshot:
        """
        Returns details for one snapshot for the specified shared cluster. To use this resource, the requesting API Key
        must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Shared-Tier-Snapshots/operation/getSharedClusterBackup>_

        :param project:
        :param cluster:
        :param snapshot_id:
        :return:
        """
        response = self.client.get(f"/groups/{project}/clusters/{cluster}/backup/tenant/snapshots/{snapshot_id}")
        return BackupTenantSnapshot.from_api(response)

    def download_shared_tier_snapshot(
        self,
        project: ProjectIdentifier,
        cluster: ClusterIdentifier,
        snapshot: BackupTenantSnapshot | str,
        target_deployment_item_name: str,
        target_project: ProjectIdentifier | None = None,
    ) -> BackupTenantSnapshotRestore:
        """
        Requests one snapshot for the specified shared cluster (M2 or M5). This resource returns a snapshotURL that you
        can use to download the snapshot. This snapshotURL remains active for four hours after you make the request. To
        use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Shared-Tier-Snapshots>_

        :param project:
        :param cluster:
        :param snapshot:
        :param target_deployment_item_name: Human-readable label that identifies the cluster on the target project ot which you want to restore the snapshot. You can restore the snapshot to a cluster tier M2 or greater.
        :param target_project: Project (id) that identifies the project that contains the cluster to which you want to restore the snapshot.
        :return:
        """
        payload = {"snapshotId": str(snapshot), "targetDeploymentItemName": target_deployment_item_name}
        if target_project is not None:
            payload["targetProjectId"] = str(target_project)
        response = self.client.post(f"/groups/{project}/clusters/{cluster}/backup/tenant/download", data=payload)
        return BackupTenantSnapshotRestore.from_api(response)
