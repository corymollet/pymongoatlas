from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.backup import (
    DiskBackupSnapshot,
    DiskBackupSnapshotIdentifier,
    ReplicaSetSnapshot,
    ShardedClusterSnapshot,
    ShardedClusterSnapshotType,
)
from pymongoatlas.entities.data_lake_pipelines import (
    DataLakePipeline,
    DataLakePipelineIdentifier,
    DataLakePipelineRun,
    DataLakePipelineRunIdentifier,
    DatasetRetentionPolicy,
    IngestionSchedule,
)
from pymongoatlas.entities.projects import ProjectIdentifier


class DataLakePipelinesClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> List[DataLakePipeline]:
        """
        Returns a list of Data Lake Pipelines. To use this resource, the requesting API Key must have the Project Read
        Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/listPipelines>_

        :param project: ``Project`` entity or its string id.
        :return: List of ``DataLakePipeline`` entities.
        """
        response = self.client.get(f"/groups/{project}/pipelines")
        return [DataLakePipeline.from_api(x) for x in response]

    def create(self, project: ProjectIdentifier, pipeline: DataLakePipeline) -> DataLakePipeline:
        """
        Creates one Data Lake Pipeline.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/createPipeline>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity to create.
        :return: Created ``DataLakePipeline`` entity.
        """
        response = self.client.post(f"/groups/{project}/pipelines", json=pipeline.to_api())
        return DataLakePipeline.from_api(response)

    def delete(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier) -> None:
        """
        Removes one Data Lake Pipeline.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/deletePipeline>_

        :param project: ``Project`` entity or its string id.
        :param pipeline:  ``DataLakePipeline`` entity or its name to remove.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/pipelines/{pipeline}")

    def get(self, project: ProjectIdentifier, pipeline_name: str) -> DataLakePipeline:
        """
        Returns the details of one Data Lake Pipeline within the specified project. To use this resource, the requesting
        API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/getPipeline>_

        :param project: ``Project`` entity or its string id.
        :param pipeline_name: Name of the pipeline to fetch.
        :return: ``DataLakePipeline`` entity.
        """
        response = self.client.get(f"/groups/{project}/pipelines/{pipeline_name}")
        return DataLakePipeline.from_api(response)

    def update(self, project: ProjectIdentifier, pipeline: DataLakePipeline) -> DataLakePipeline:
        """
        Updates one Data Lake Pipeline.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/updatePipeline>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity with data to update.
        :return: Updated ``DataLakePipeline`` entity.
        """
        response = self.client.patch(f"/groups/{project}/pipelines/{pipeline}", json=pipeline.to_api())
        return DataLakePipeline.from_api(response)

    def list_ingestion_schedules(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier) -> List[IngestionSchedule]:
        """
        Returns a list of backup schedule policy items that you can use as a Data Lake Pipeline source. To use this
        resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/listPipelineSchedules>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :return: List of ``IngestionSchedule`` entities.
        """
        response = self.client.get(f"/groups/{project}/pipelines/{pipeline}/availableSchedules")
        return [IngestionSchedule.from_api(x) for x in response]

    def list_snapshots(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier) -> List[DiskBackupSnapshot]:
        """
        Returns a list of backup snapshots that you can use to trigger an on demand pipeline run. To use this resource,
        the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/listPipelineSnapshots>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :return: List of Disk Backup Snapshot entities.
        """
        snapshots = []
        response = self.client.get(f"/groups/{project}/pipelines/{pipeline}/availableSnapshots")
        for snapshot in response["results"]:
            snapshot_type = ShardedClusterSnapshotType(snapshot["type"])
            if snapshot_type == ShardedClusterSnapshotType.REPLICA_SET:
                snapshots.append(ReplicaSetSnapshot.from_api(snapshot))
            elif snapshot_type == ShardedClusterSnapshotType.SHARDED_CLUSTER:
                snapshots.append(ShardedClusterSnapshot.from_api(snapshot))
        return snapshots

    def pause(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier) -> DataLakePipeline:
        """
        Pauses ingestion for a Data Lake Pipeline within the specified project. To use this resource, the requesting API
        Key must have the Project Read Only role.

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :return: ``DataLakePipeline`` entity.
        """
        response = self.client.post(f"/groups/{project}/pipelines/{pipeline}/pause")
        return DataLakePipeline.from_api(response)

    def resume(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier) -> DataLakePipeline:
        """
        Resumes ingestion for a Data Lake Pipeline within the specified project. To use this resource, the requesting
        API Key must have the Project Read Only role.

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :return: ``DataLakePipeline`` entity.
        """
        response = self.client.post(f"/groups/{project}/pipelines/{pipeline}/resume")
        return DataLakePipeline.from_api(response)

    def list_runs(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier) -> List[DataLakePipelineRun]:
        """
        Returns a list of past Data Lake Pipeline runs. To use this resource, the requesting API Key must have the
        Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/listPipelineRuns>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :return: List of ``DataLakePipelineRun`` entities.
        """
        response = self.client.get(f"/groups/{project}/pipelines/{pipeline}/runs")
        return [DataLakePipelineRun.from_api(x) for x in response["results"]]

    def delete_run_dataset(
        self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier, run: DataLakePipelineRunIdentifier
    ) -> None:
        """
        Deletes the dataset that Atlas generated during the specified pipeline run.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/deletePipelineRunDataset>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :param run: ``DataLakePipelineRun`` entity or its string id.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/pipelines/{pipeline}/runs/{run}")

    def get_run(self, project: ProjectIdentifier, pipeline: DataLakePipelineIdentifier, run_id: str) -> DataLakePipelineRun:
        """
        Returns the details of one Data Lake Pipeline run within the specified project. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/getPipelineRun>_

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its name.
        :param run_id: ID of the Pipeline Run to fetch.
        :return: ``DataLakePipeline`` entity.
        """
        response = self.client.get(f"/groups/{project}/pipelines/{pipeline}/runs/{run_id}")
        return DataLakePipelineRun.from_api(response)

    def trigger_on_demand_snapshot_ingestion(
        self,
        project: ProjectIdentifier,
        pipeline: DataLakePipelineIdentifier,
        snapshot: DiskBackupSnapshotIdentifier,
        retention_policy: DatasetRetentionPolicy | None = None,
    ) -> DataLakePipelineRun:
        """
        Triggers a Data Lake Pipeline ingestion of a specified snapshot.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Lake-Pipelines/operation/getPipelineRun_>

        :param project: ``Project`` entity or its string id.
        :param pipeline: ``DataLakePipeline`` entity or its string id.
        :param snapshot: Snapshot entity or its string id.
        :param retention_policy: Optional retention policy.
        :return: ``DataLakePipelineRun`` entity.
        """
        payload = {"snapshotId": str(snapshot)}
        if retention_policy is not None:
            payload["datasetRetentionPolicy"] = retention_policy.to_api()
        response = self.client.post(f"/groups/{project}/pipelines/{pipeline}/trigger", json=payload)
        return DataLakePipelineRun.from_api(response)
