from typing import Generator

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.events import Event, EventFilter
from pymongoatlas.entities.organization import OrganizationIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier


class EventsClient(AtlasResourceClient):
    def list_project_events(
        self,
        project: ProjectIdentifier,
        event_filter: EventFilter | None = None,
    ) -> Generator[Event, None, None]:
        """
        Returns all events for the specified project. Events identify significant database, billing, or security
        activities or status changes. To use this resource, the requesting API Key must have the Project Read Only role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Events/operation/listProjectEvents>_

        :param project: ``Project`` entity or its string id
        :param event_filter: Optional filters
        :return: Generator of ``Event`` entities.
        """
        params = {}
        if event_filter is not None:
            params = event_filter.to_api()
        for page in self.client.paginate(f"/groups/{project}/events", params=params):
            yield from (Event.from_api(x) for x in page["results"])

    def get_project_event(self, project: ProjectIdentifier, event_id: str, include_raw: bool = False) -> Event:
        """
        Returns one event for the specified project. Events identify significant database, billing, or security
        activities or status changes. To use this resource, the requesting API Key must have the Project Read Only role.

        This resource remains under revision and may change.

        :param project: ``Project`` entity or its string id.
        :param event_id: ID of the event to fetch.
        :param include_raw: Flag that indicates whether to include the raw document in the output. The raw document
                            contains additional meta information about the event.
        :return: ``Event`` entity
        """
        response = self.client.get(f"/groups/{project}/events/{event_id}", params={"includeRaw": include_raw})
        return Event.from_api(response)

    def list_org_events(self, org: OrganizationIdentifier, event_filter: EventFilter | None = None) -> Generator[Event, None, None]:
        """
        Returns all events for the specified organization. Events identify significant database, billing, or security
        activities or status changes. To use this resource, the requesting API Key must have the Organization Member
        role.

        This resource remains under revision and may change.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Events/operation/listOrganizationEvents>_

        :param org: ``Organization`` entity or its string id.
        :param event_filter: Optional event filters.
        :return: Generator of ``Event`` entities.
        """
        params = {}
        if event_filter is not None:
            params = event_filter.to_api()
        for page in self.client.paginate(f"/orgs/{org}/events", params=params):
            yield from (Event.from_api(x) for x in page["results"])

    def get_org_event(self, org: OrganizationIdentifier, event_id: str, include_raw: bool = False) -> Event:
        """
        Returns one event for the specified project. Events identify significant database, billing, or security
        activities or status changes. To use this resource, the requesting API Key must have the Project Read Only role.

        This resource remains under revision and may change.

        :param org: ``Organization`` entity or its string id.
        :param event_id: ID of the event to fetch.
        :param include_raw: Flag that indicates whether to include the raw document in the output. The raw document
                            contains additional meta information about the event.
        :return: ``Event`` entity
        """
        response = self.client.get(f"/groups/{org}/events/{event_id}", params={"includeRaw": include_raw})
        return Event.from_api(response)
