from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.encryption import EncryptionAtRestConfig
from pymongoatlas.entities.projects import ProjectIdentifier


class EncryptionClient(AtlasResourceClient):
    def get_encryption_at_rest(self, project: ProjectIdentifier) -> EncryptionAtRestConfig:
        """
        Returns the configuration for encryption at rest using the keys you manage through your cloud provider. MongoDB
        Cloud encrypts all storage even if you don't use your own key management. This resource requires the requesting
        API Key to have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Encryption-at-Rest-using-Customer-Key-Management/operation/getEncryptionAtRest>_

        :param project: ``Project`` entity or its string id.
        :return: ``EncryptionAtRestConfig`` entity.
        """
        response = self.client.get(f"/groups/{project}/encryptionAtRest")
        return EncryptionAtRestConfig.from_api(response)

    def update_encryption_at_rest(self, project: ProjectIdentifier, encryption_config: EncryptionAtRestConfig) -> EncryptionAtRestConfig:
        """
        Updates the configuration for encryption at rest using the keys you manage through your cloud provider. MongoDB
        Cloud encrypts all storage even if you don't use your own key management. This resource requires the requesting
        API Key to have the Project Owner role. This feature isn't available for M0 free clusters, M2, M5, or
        serverless clusters.

        After you configure at least one Encryption at Rest using a Customer Key Management provider for the MongoDB Cloud
        project, Project Owners can enable Encryption at Rest using Customer Key Management for each MongoDB Cloud
        cluster for which they require encryption. The Encryption at Rest using Customer Key Management provider doesn't
        have to match the cluster cloud service provider. MongoDB Cloud doesn't automatically rotate user-managed
        encryption keys. Defer to your preferred Encryption at Rest using Customer Key Management provider's
        documentation and guidance for best practices on key rotation. MongoDB Cloud automatically creates a 90-day key
        rotation alert when you configure Encryption at Rest using Customer Key Management using your Key Management in
        an MongoDB Cloud project. MongoDB Cloud encrypts all storage whether or not you use your own key management.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Encryption-at-Rest-using-Customer-Key-Management/operation/updateEncryptionAtRest>_

        :param project: ``Project`` entity or its string id.
        :param encryption_config: ``EncryptionAtRestConfig`` entity with updated data.
        :return: Updated ``EncryptionAtRestConfig``
        """
        response = self.client.patch(f"/groups/{project}/encryptionAtRest", json=encryption_config.to_api())
        return EncryptionAtRestConfig.from_api(response)
