from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.federation import (
    ConnectedOrgConfig,
    FederatedRoleMapping,
    FederatedRoleMappingIdentifier,
    FederationSettings,
    FederationSettingsIdentifier,
    IdentityProvider,
    IdentityProviderIdentifier,
    IDPProtocol,
    IDPType,
    OIDCIdentityProvider,
    OIDCWorkforceIdentityProvider,
    OIDCWorkloadIdentityProvider,
    SAMLIdentityProvider,
)
from pymongoatlas.entities.organization import OrganizationIdentifier


def process_identity_providers(providers: list[dict]) -> list[IdentityProvider]:
    provider_entities = []
    for provider in providers:
        protocol = IDPProtocol(provider["protocol"])
        if protocol == IDPProtocol.SAML:
            provider_entities.append(SAMLIdentityProvider.from_api(provider))
        elif protocol == IDPProtocol.OIDC:
            idp_type = IDPType(provider["idpType"])
            if idp_type == IDPType.WORKFORCE:
                provider_entities.append(OIDCWorkforceIdentityProvider.from_api(provider))
            elif idp_type == IDPType.WORKLOAD:
                provider_entities.append(OIDCWorkloadIdentityProvider.from_api(provider))
    return provider_entities


class FederationClient(AtlasResourceClient):
    def delete_federation_settings(self, federation_settings: FederationSettingsIdentifier) -> None:
        """
        Deletes the federation settings instance and all associated data, including identity providers and domains. To
        use this resource, the requesting API Key must have the Organization Owner role in the last remaining connected
        organization. Note: requests to this resource will fail if there is more than one connected organization in the
        federation.

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :return: ``None``
        """
        self.client.delete(f"/federationSettings/{federation_settings}")

    def list_connected_org_configs(self, federation_settings: FederationSettingsIdentifier) -> list[ConnectedOrgConfig]:
        """
        Returns all connected org configs in the specified federation. To use this resource, the requesting API Key must
        have the Organization Owner role in one of the connected orgs.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/listConnectedOrgConfigs>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :return: List of ``ConnectedOrgConfig`` entities.
        """
        response = self.client.get(f"/federationSettings/{federation_settings}/connectedOrgConfigs")
        return [ConnectedOrgConfig.from_api(x) for x in response["results"]]

    def remove_connected_org_config(self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier) -> None:
        """
        Removes one connected organization configuration from the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role. Note: This request fails if only one connected
        organization exists in the federation.

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :return: ``None``
        """
        self.client.delete(f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}")

    def get_connected_org_config(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier
    ) -> ConnectedOrgConfig:
        """
        Returns the specified connected org config from the specified federation. To use this resource, the requesting
        API Key must have the Organization Owner role in the connected org.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/getConnectedOrgConfig_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :return: ``ConnectedOrgConfig`` entity.
        """
        response = self.client.get(f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}")
        return ConnectedOrgConfig.from_api(response)

    def update_connected_org_config(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier, config: ConnectedOrgConfig
    ) -> ConnectedOrgConfig:
        """
        Updates one connected organization configuration from the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role.

        **Note**: If the organization configuration has no associated identity provider, you can't use this resource to
        update role mappings or post authorization role grants.

        **Note**: The domainRestrictionEnabled field defaults to false if not provided in the request.

        **Note**: If the identityProviderId field is not provided, you will disconnect the organization and the identity
        provider.

        **Note**: Currently connected data access identity providers missing from the dataAccessIdentityProviderIds field
        will be disconnected.

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :param config: ``ConnectedOrgConfig`` entity with data to update.
        :return: Updated ``ConnectedOrgConfig`` entity.
        """
        response = self.client.patch(f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}", json=config.to_api())
        return ConnectedOrgConfig.from_api(response)

    def list_role_mappings(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier
    ) -> list[FederatedRoleMapping]:
        """
        Returns all role mappings from the specified organization in the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/listRoleMappings>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` or its string id.
        """
        response = self.client.get(f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}/roleMappings")
        return [FederatedRoleMapping.from_api(x) for x in response["results"]]

    def add_role_mapping(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier, role_mapping: FederatedRoleMapping
    ) -> FederatedRoleMapping:
        """
        Adds one role mapping to the specified organization in the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/createRoleMapping>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :param role_mapping: ``FederatedRoleMapping`` entity to add.
        :return: Added ``FederatedRoleMapping`` entity.
        """
        response = self.client.post(
            f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}/roleMappings", json=role_mapping.to_api()
        )
        return FederatedRoleMapping.from_api(response)

    def remove_role_mapping(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier, role_mapping: FederatedRoleMappingIdentifier
    ) -> None:
        """
        Removes one role mapping in the specified organization from the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/deleteRoleMapping>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :param role_mapping: ``RoleMapping`` entity or its string id to remove.
        :return: ``None``
        """
        self.client.delete(f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}/roleMappings/{role_mapping}")

    def get_role_mapping(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier, role_mapping_id: str
    ) -> FederatedRoleMapping:
        """
        Returns one role mapping from the specified organization in the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/getRoleMapping>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :param role_mapping_id: ID of the Role Mapping to fetch.
        :return: ``FederatedRoleMapping`` entity.
        """
        response = self.client.get(f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}/roleMappings/{role_mapping_id}")
        return FederatedRoleMapping.from_api(response)

    def update_role_mapping(
        self, federation_settings: FederationSettingsIdentifier, org: OrganizationIdentifier, role_mapping: FederatedRoleMapping
    ) -> FederatedRoleMapping:
        """
        Updates one role mapping in the specified organization in the specified federation. To use this resource, the
        requesting API Key must have the Organization Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/updateRoleMapping>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param org: ``Organization`` entity or its string id.
        :param role_mapping: ``FederatedRoleMapping`` entity with data to update.
        :return: Updated ``FederatedRoleMapping`` entity.
        """
        response = self.client.put(
            f"/federationSettings/{federation_settings}/connectedOrgConfigs/{org}/roleMappings/{role_mapping}", json=role_mapping.to_api()
        )
        return FederatedRoleMapping.from_api(response)

    def get_federation_settings(self, org: OrganizationIdentifier) -> FederationSettings:
        """
        Returns information about the federation settings for the specified organization. To use this resource, the
        requesting API Key must have the Organization Owner role in the connected org.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/getFederationSettings>_

        :param org: ``Organization`` entity or its string id.
        :return: ``FederationSettings`` entity.
        """
        response = self.client.get(f"/orgs/{org}/federationSettings")
        return FederationSettings.from_api(response)

    def list_identity_providers(
        self, federation_settings: FederationSettingsIdentifier, protocol: IDPProtocol | str | None = None
    ) -> list[IdentityProvider]:
        """
        Returns all identity providers with the provided protocol in the specified federation. If no protocol is
        specified, only SAML identity providers will be returned. To use this resource, the requesting API Key must have
        the Organization Owner role in one of the connected organizations.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/listIdentityProviders>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param protocol: Optional ``IdpProtocol`` type to filter results.
        :return: List of ``IdentityProvider`` entities.
        """
        params = {}
        if protocol is not None:
            if isinstance(protocol, str):
                protocol = IDPProtocol(protocol.upper().strip())
            params["protocol"] = protocol.value
        response = self.client.get(f"/federationSettings/{federation_settings}/identityProviders", params=params)
        return process_identity_providers(response["results"])

    def create_identity_provider(
        self, federation_settings: FederationSettingsIdentifier, provider: OIDCIdentityProvider
    ) -> OIDCIdentityProvider:
        """
        Creates one identity provider within the specified federation. To use this resource, the requesting API Key must
        have the Organization Owner role in one of the connected organizations.

        **Note**: This resource only supports the creation of OIDC identity providers.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/createIdentityProvider>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param provider: OIDC IDP entity to create
        :return: Created OIDC IDP
        """
        response = self.client.post(f"/federationSettings/{federation_settings}/identityProviders", json=provider.to_api())
        return process_identity_providers([response])[0]

    def delete_identity_provider(
        self, federation_settings: FederationSettingsIdentifier, identity_provider: IdentityProviderIdentifier
    ) -> None:
        """
        Deletes one identity provider in the specified federation. To use this resource, the requesting API Key must
        have the Organization Owner role in one of the connected organizations.

        **Note**: Requests to this resource will fail if the identity provider has any connected organizations. Before
        deleting an identity provider, disconnect all organizations and confirm that no organization in your account
        uses this identity provider. To learn more, see

        `Manage Organization Mapping for Federated Authentication <https://www.mongodb.com/docs/atlas/security/manage-org-mapping/>`_.

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param identity_provider: Identity Provider entity or its IDP id.
        :return: ``None``
        """
        self.client.delete(f"/federationSettings/{federation_settings}/identityProviders/{identity_provider}")

    def get_identity_provider(self, federation_settings: FederationSettingsIdentifier, identity_provider_id: str) -> IdentityProvider:
        """
        Returns one identity provider in the specified federation by the identity provider's id. To use this resource,
        the requesting API Key must have the Organization Owner role in one of the connected organizations.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/getIdentityProvider>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param identity_provider_id: ID of the IDP to fetch.
        :return: Identity Provider entity.
        """
        response = self.client.get(f"/federationSettings/{federation_settings}/identityProviders/{identity_provider_id}")
        return process_identity_providers([response])[0]

    def update_identity_provider(
        self, federation_setting: FederationSettingsIdentifier, identity_provider: IdentityProvider
    ) -> IdentityProvider:
        """
        Updates one identity provider in the specified federation. To use this resource, the requesting API Key must
        have the Organization Owner role in one of the connected organizations.

        **Note**: Changing authorization types and/or updating authorization claims can prevent current users and/or
        groups from accessing the database.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/updateIdentityProvider>_

        :param federation_setting: ``FederationSettings`` entity or its string id.
        :param identity_provider: Identity Provider entity with data to update.
        :return: Updated Identity Provider entity.
        """
        response = self.client.patch(
            f"/federationSettings/{federation_setting}/identityProviders/{identity_provider}", json=identity_provider.to_api()
        )
        return process_identity_providers([response])[0]

    def revoke_identity_provider_jwks(
        self, federation_settings: FederationSettingsIdentifier, identity_provider: IdentityProviderIdentifier
    ) -> None:
        """
        Revokes the JWKS tokens from the requested OIDC identity provider. To use this resource, the requesting API Key
        must have the Organization Owner role in one of the connected organizations.

        **Note**: Revoking your JWKS tokens immediately refreshes your IdP public keys from all your Atlas clusters,
        invalidating previously signed access tokens and logging out all users. You may need to restart your MongoDB
        clients. All organizations connected to the identity provider will be affected. To learn more, see
        `Configure OIDC Authorization <https://www.mongodb.com/docs/atlas/security-oidc/#revoke-jwks>`_.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/revokeJwksFromIdentityProvider>_

        :param federation_settings: ``FederationSettings`` entity or its string id
        :param identity_provider: Identity Provider entity or its IDP id.
        :return: ``None``
        """
        self.client.delete(f"/federationSettings/{federation_settings}/identityProviders/{identity_provider}/jwks")

    def get_identity_provider_metadata(
        self, federation_settings: FederationSettingsIdentifier, identity_provider: IdentityProviderIdentifier
    ) -> str:
        """
        Returns the metadata of one identity provider in the specified federation. To use this resource, the requesting
        API Key must have the Organization Owner role in one of the connected organizations.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Federated-Authentication/operation/getIdentityProviderMetadata>_

        :param federation_settings: ``FederationSettings`` entity or its string id.
        :param identity_provider: Identity Provider entity or its IDP id.
        """
        if isinstance(identity_provider, SAMLIdentityProvider):
            identity_provider = identity_provider.okta_idp_id
        response = self.client.get(f"/federationSettings/{federation_settings}/identityProviders/{identity_provider}/metadata.xml")
        return response.text
