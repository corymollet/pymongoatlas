from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.cloud_providers import AWSIAMRole, AzureIAMRole, IAMProvider, IAMRole, IAMRoleIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier


def process_iam_roles(roles: list[dict]) -> List[IAMRole]:
    processed_roles = []
    role_map = {
        IAMProvider.AWS: AWSIAMRole,
        IAMProvider.AZURE: AzureIAMRole,
    }
    for role in roles:
        provider = IAMProvider(role["providerName"])
        processed_roles.append(role_map[provider].from_api(role))
    return processed_roles


class CloudProviderAccessClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> List[IAMRole]:
        """
        Returns all cloud provider access roles with access to the specified project. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Provider-Access/operation/listCloudProviderAccessRoles>_

        :param project: ``Project`` entity or its string id.
        """
        response = self.client.get(f"/groups/{project}/cloudProviderAccess")
        return process_iam_roles(response["awsIamRoles"])

    def create(self, project: ProjectIdentifier, role: IAMRole) -> IAMRole:
        """
        Creates one access role for the specified cloud provider. Some MongoDB Cloud features use these cloud provider
        access roles for authentication. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Provider-Access/operation/createCloudProviderAccessRole>_

        :param project: ``Project`` entity or its string id.
        :param role: IAM role entity to create.
        :return: Created IAM role entity.
        """
        response = self.client.post(f"/groups/{project}/cloudProviderAccess", json=role.to_api())
        return process_iam_roles([response])[0]

    def revoke(self, project: ProjectIdentifier, provider: IAMProvider | str, role: IAMRoleIdentifier) -> None:
        """
        Revokes access to the specified project for the specified access role. To use this resource, the requesting API
        Key must have the Project Owner role.

        :param project: ``Project`` entity or its string id.
        :param provider: Provider type
        :param role: Role identifier to de-authorize.
        :return: ``None``
        """
        if isinstance(provider, str):
            provider = IAMProvider(provider.upper().strip())
        self.client.delete(f"/groups/{project}/cloudProviderAccess/{provider.value}/{role}")

    def get(self, project: ProjectIdentifier, role_id: str) -> IAMRole:
        """
        Returns the access role with the specified id and with access to the specified project. To use this resource,
        the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Provider-Access/operation/getCloudProviderAccessRole>_

        :param project: ``Project`` entity or its string id.
        :param role_id: ID of the IAM role to fetch.
        :return: IAMRole entity.
        """
        response = self.client.get(f"/groups/{project}/cloudProviderAccess/{role_id}")
        return process_iam_roles([response])[0]

    def authorize(self, project: ProjectIdentifier, role: IAMRoleIdentifier) -> IAMRole:
        """
        Grants access to the specified project for the specified access role. To use this resource, the requesting API
        Key must have the Project Owner role. This API endpoint is one step in a procedure to create unified access for
        MongoDB Cloud services.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Cloud-Provider-Access/operation/authorizeCloudProviderAccessRole>_

        :param project: ``Project`` entity or its string id.
        :param role: IAM Role to authorize
        :return: IAM role entity.
        """
        response = self.client.patch(f"/groups/{project}/cloudProviderAccess/{role}")
        return process_iam_roles([response])[0]
