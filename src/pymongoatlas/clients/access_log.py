from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.access_log import AccessLog, AccessLogQueryParams
from pymongoatlas.entities.cluster import ClusterIdentifier
from pymongoatlas.entities.monitoring import ProcessIdentifier
from pymongoatlas.entities.projects import ProjectIdentifier


class AccessLogClient(AtlasResourceClient):
    def list_by_cluster_name(
        self, project: ProjectIdentifier, cluster: ClusterIdentifier, params: AccessLogQueryParams | None = None
    ) -> list[AccessLog]:
        """
        Returns the access logs of one cluster identified by the cluster's name. Access logs contain a list of
        authentication requests made against your cluster. You can't use this feature on tenant-tier clusters
        (M0, M2, M5). To use this resource, the requesting API Key must have the Project Monitoring Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Access-Tracking/operation/listAccessLogsByClusterName>_
        """
        params = params or AccessLogQueryParams()
        response = self.client.get(
            f"/groups/{project}/dbAccessHistory/clusters/{cluster}", params=params.model_dump(exclude_none=True, by_alias=True)
        )
        return [AccessLog.from_api(x) for x in response["accessLogs"]]

    def list_by_hostname(
        self, project: ProjectIdentifier, process: ProcessIdentifier, params: AccessLogQueryParams | None = None
    ) -> list[AccessLog]:
        """
        Returns the access logs of one cluster identified by the cluster's hostname. Access logs contain a list of
        authentication requests made against your clusters. You can't use this feature on tenant-tier clusters
        (M0, M2, M5). To use this resource, the requesting API Key must have the Project Monitoring Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Access-Tracking/operation/listAccessLogsByHostname>_
        """
        params = params or AccessLogQueryParams()
        response = self.client.get(
            f"/groups/{project}/dbAccessHistory/processes/{process}", params=params.model_dump(exclude_none=True, by_alias=True)
        )
        return [AccessLog.from_api(x) for x in response["accessLogs"]]
