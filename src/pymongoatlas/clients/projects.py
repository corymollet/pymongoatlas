from typing import List

from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.auth import AtlasProjectRole, AtlasUser, AtlasUserTypeAlias
from pymongoatlas.entities.networking import ProjectIPAddress
from pymongoatlas.entities.projects import (
    MaintenanceWindow,
    Project,
    ProjectIdentifier,
    ProjectLimit,
    ProjectLimitIdentifier,
    ProjectLimitName,
    ProjectSettings,
)


class ProjectsClient(AtlasResourceClient):
    def get(self, project_id: str) -> Project:
        """
        Returns details about the specified project. Projects group clusters into logical collections that support an
        application environment, workload, or both. Each project can have its own users, teams, security, tags, and alert
        settings. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/getProject>_

        :param project_id: Unique 24-hexadecimal digit string that identifies your project.
        :return: ``Project`` entity
        """
        response = self.client.get(f"/groups/{project_id}")
        return Project.from_api(response)

    def get_by_name(self, name: str) -> Project:
        """
        Returns details about the specified project. Projects group clusters into logical collections that support an
        application environment, workload, or both. Each project can have its own users, teams, security, and alert
        settings. To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/getProjectByName>_

        :param name: Name of the project.
        :return: ``Project`` entity
        """
        response = self.client.get(f"/groups/byName/{name}")
        return Project.from_api(response)

    def create(self, project: Project, with_default_alert_settings: bool = True) -> Project:
        """
        Creates one project. Projects group clusters into logical collections that support an application environment,
        workload, or both. Each project can have its own users, teams, security, tags, and alert settings. To use this
        resource, the requesting API Key must have the Read Write role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/createProject>_

        :param project: Project entity to create
        :param with_default_alert_settings: Whether to create the project with default alert settings
        :return: ``Project`` entity
        """
        response = self.client.post("/groups", data=project.to_api(with_default_alert_settings=with_default_alert_settings))
        return Project.from_api(response)

    def delete(self, project: ProjectIdentifier) -> None:
        """
        Removes the specified project. Projects group clusters into logical collections that support an application
        environment, workload, or both. Each project can have its own users, teams, security, tags, and alert settings.
        You can delete a project only if there are no Online Archives for the clusters in the project. To use this
        resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/deleteProject>_

        :param project: Project (entity object or id) to delete.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}")

    def list(self) -> List[Project]:
        """
        Returns details about all projects. Projects group clusters into logical collections that support an application
        environment, workload, or both. Each project can have its own users, teams, security, and alert settings. To use
        this resource, the requesting API Key must have the Read Write role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/listProjects>_

        :return: List of ``Project`` entities
        """
        response = self.client.get("/groups")
        return [Project.from_api(x) for x in response.get("results")]

    def update(self, project: Project) -> Project:
        """
        Updates the human-readable label that identifies the specified project, or the tags associated with the project.
        To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/updateProject>_

        :param project: Project entity with data to update
        :return: Updated ``Project`` entity
        """
        response = self.client.patch(f"/groups/{project}", data=project.to_api())
        return Project.from_api(response)

    def list_ip_addresses(self, project: ProjectIdentifier) -> ProjectIPAddress:
        """
        Returns all IP addresses for this project. To use this resource, the requesting API Key must have the Project
        Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/returnAllIPAddresses>_

        :param project: ``Project`` (entity object or id)
        :return: List of ``ProjectIPAddress`` entities
        """
        response = self.client.get(f"/groups/{project}/ipAddresses")
        return ProjectIPAddress.from_api(response)

    def list_limits(self, project: ProjectIdentifier) -> List[ProjectLimit]:
        """
        Returns all the limits for the specified project. To use this resource, the requesting API Key must have the
        Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/listProjectLimits>_

        :param project: ``Project`` (entity object or id)
        :return: List of ``ProjectLimit`` entities
        """
        response = self.client.get(f"/groups/{project}/limits")
        return [ProjectLimit.from_api(x) for x in response]

    def delete_limit(self, project: ProjectIdentifier, limit: ProjectLimitIdentifier) -> None:
        """
        Removes the specified project limit. Depending on the limit, Atlas either resets the limit to its default value
        or removes the limit entirely. To use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/deleteProjectLimit>_

        :param project: ``Project`` (entity object or id)
        :param limit: ``ProjectLimit`` (entity object or name)
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/limits/{limit}")

    def get_limit(self, project: ProjectLimit, limit_name: ProjectLimitName | str) -> ProjectLimit:
        """
        Returns the specified limit for the specified project. To use this resource, the requesting API Key must have
        the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/getProjectLimit>_

        :param project: ``Project`` (entity or id)
        :param limit_name: name of the limit to retrieve
        :return: ``ProjectLimit`` entity
        """
        if isinstance(limit_name, str):
            limit_name = ProjectLimitName(limit_name)
        response = self.client.get(f"/groups/{project}/limits/{limit_name.value}")
        return ProjectLimit.from_api(response)

    def update_limit(self, project: ProjectIdentifier, limit: ProjectLimitIdentifier) -> ProjectLimit:
        """
        Sets the specified project limit. To use this resource, the requesting API Key must have the Project Owner role.

        NOTE: Increasing the following configuration limits might lead to slower response times in the MongoDB Cloud UI
        or increased user management overhead leading to authentication or authorization re-architecture. If possible,
        we recommend that you create additional projects to gain access to more of these resources for a more
        sustainable growth pattern.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/setProjectLimit>_

        :param project: ``Project`` (entity or id)
        :param limit: ``ProjectLimit`` with updated data
        :return: Updated ``ProjectLimit`` entity
        """
        response = self.client.patch(f"/groups/{project}/limits/{limit}", json={"value": limit.value})
        return ProjectLimit.from_api(response)

    def get_settings(self, project: ProjectIdentifier) -> ProjectSettings:
        """
        Returns details about the specified project's settings. To use this resource, the requesting API Key must have
        the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/getProjectSettings>_

        :param project: ``Project`` (entity or id)
        :return: ``ProjectSettings`` entity
        """
        response = self.client.get(f"/groups/{project}/settings")
        return ProjectSettings.from_api(response)

    def update_settings(self, project: ProjectIdentifier, settings: ProjectSettings) -> ProjectSettings:
        """
        Updates the settings of the specified project. You can update any of the options available. MongoDB cloud only
        updates the options provided in the request. To use this resource, the requesting API Key must have the Project
        Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/updateProjectSettings>_

        :param project: ``Project`` (entity or id)
        :param settings: ``ProjectSettings`` with updated data
        :return: Updated ``ProjectSettings`` entity
        """
        response = self.client.patch(f"/groups/{project}/settings", json=settings.to_api())
        return ProjectSettings.from_api(response)

    def list_users(self, project: ProjectIdentifier) -> List[AtlasUser]:
        """
        Returns details about all users in the specified project. Users belong to an organization. To use this resource,
        the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/listProjectUsers>_
        :param project: ``Project`` (entity or id)
        :return: List of ``AtlasUser`` entities
        """
        response = self.client.get(f"/groups/{project}/users")
        return [AtlasUser.from_api(x) for x in response["results"]]

    def add_user(self, project: ProjectIdentifier, user: AtlasUser) -> AtlasUser:
        """
        Adds one MongoDB Cloud user to the specified project. If the MongoDB Cloud user is not a member of the project's
        organization, then the user must accept their invitation to the organization to access information within the
        specified project. If the MongoDB Cloud User is already a member of the project's organization, then they will
        be added to the project immediately and an invitation will not be returned by this resource. To use this
        resource, the requesting API Key must have the Group User Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/addUserToProject>_

        :param project: ``Project`` (entity or id)
        :param user: ``AtlasUser`` entity to add to the project.
        :return: ``AtlasUser`` entity
        """
        response = self.client.post(
            f"/groups/{project}/access", json={"username": user.username, "roles": [x.value for x in user.get_project_roles(project)]}
        )
        return AtlasUser.from_api(response)

    def remove_user(self, project: ProjectIdentifier, user: AtlasUserTypeAlias) -> None:
        """
        Removes the specified user from the specified project. To use this resource, the requesting API Key must have
        the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/removeProjectUser>_

        :param project: ``Project`` (entity or id)
        :param user: ``AtlasUser`` (entity or id)
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/users/{user}")

    def update_user_project_roles(
        self, project: ProjectIdentifier, user: AtlasUserTypeAlias, roles: List[AtlasProjectRole | str]
    ) -> List[AtlasProjectRole]:
        """
        Updates the roles of the specified user in the specified project. To specify the user to update, provide the
        unique 24-hexadecimal digit string that identifies the user in the specified project. To use this resource, the
        requesting API Key must have the Group User Admin role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Projects/operation/updateProjectRoles>_

        :param project: ``Project`` (entity or id)
        :param user: ``AtlasUser`` (entity or id)
        :param roles: List of ``AtlasProjectRole`` to set for this user.
        :return: List of updated ``AtlasProjectRole`` attached to this user.
        """
        roles = [AtlasProjectRole(x) for x in roles]
        response = self.client.patch(f"/groups/{project}/users/{user}/roles", json={"groupRoles": [x.value for x in roles]})
        return [AtlasProjectRole(x) for x in response["groupRoles"]]


class MaintenanceWindowClient(AtlasResourceClient):
    def get(self, project: ProjectIdentifier) -> MaintenanceWindow:
        """
        Returns the maintenance window for the specified project. MongoDB Cloud starts those maintenance activities when
        needed. You can't change your maintenance window until the current maintenance efforts complete. The maintenance
        procedure that MongoDB Cloud performs requires at least one replica set election during the maintenance window
        per replica set. Maintenance always begins as close to the scheduled hour as possible, but in-progress cluster
        updates or unexpected system issues could delay the start time. To use this resource, the requesting API Key
        must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Maintenance-Windows/operation/getMaintenanceWindow>_

        :param project: ``Project`` entity or its string id
        :return: ``MaintenanceWindow`` entity.
        """
        response = self.client.get(f"/groups/{project}/maintenanceWindow")
        return MaintenanceWindow.from_api(response)

    def reset(self, project: ProjectIdentifier) -> None:
        """
        Resets the maintenance window for the specified project. To use this resource, the requesting API Key must have
        the Project Owner role. Urgent maintenance activities such as security patches can't wait for your chosen
        window. MongoDB Cloud starts those maintenance activities when needed. After you schedule maintenance for your
        cluster, you can't change your maintenance window until the current maintenance efforts complete. The
        maintenance procedure that MongoDB Cloud performs requires at least one replica set election during the
        maintenance window per replica set. Maintenance always begins as close to the scheduled hour as possible, but
        in-progress cluster updates or unexpected system issues could delay the start time. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Maintenance-Windows/operation/resetMaintenanceWindow>_

        :param project: ``Project`` entity or its string id.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/maintenanceWindow")

    def update(self, project: ProjectIdentifier, maintenance_window: MaintenanceWindow) -> None:
        """
        Updates the maintenance window for the specified project. Urgent maintenance activities such as security patches
        can't wait for your chosen window. MongoDB Cloud starts those maintenance activities when needed. After you
        schedule maintenance for your cluster, you can't change your maintenance window until the current maintenance
        efforts complete. The maintenance procedure that MongoDB Cloud performs requires at least one replica set
        election during the maintenance window per replica set. Maintenance always begins as close to the scheduled hour
        as possible, but in-progress cluster updates or unexpected system issues could delay the start time. To use this
        resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Maintenance-Windows/operation/updateMaintenanceWindow>_

        :param project: ``Project`` entity or its string id.
        :param maintenance_window: ``MaintenanceWindow`` entity with updated data.
        :return: ``None``
        """
        self.client.patch(f"/groups/{project}/maintenanceWindow", json=maintenance_window.to_api())

    def toggle_automatic_deferral(self, project: ProjectIdentifier) -> None:
        """
        Toggles automatic deferral of the maintenance window for the specified project. To use this resource, the
        requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Maintenance-Windows/operation/toggleMaintenanceAutoDefer>_

        :param project: ``Project`` entity or its string id.
        :return: ``None``
        """
        self.client.post(f"/groups/{project}/maintenanceWindow/autoDefer")

    def defer(self, project: ProjectIdentifier) -> None:
        """
        Defers the maintenance window for the specified project. Urgent maintenance activities such as security patches
        can't wait for your chosen window. MongoDB Cloud starts those maintenance activities when needed. After you
        schedule maintenance for your cluster, you can't change your maintenance window until the current maintenance
        efforts complete. The maintenance procedure that MongoDB Cloud performs requires at least one replica set
        election during the maintenance window per replica set. Maintenance always begins as close to the scheduled hour
        as possible, but in-progress cluster updates or unexpected system issues could delay the start time. To use this
        resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Maintenance-Windows/operation/deferMaintenanceWindow>_

        :param project: ``Project`` entity or its string id.
        :return: ``None``
        """
        self.client.post(f"/groups/{project}/maintenanceWindow/defer")
