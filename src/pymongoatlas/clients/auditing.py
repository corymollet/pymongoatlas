from pymongoatlas.clients.common import AtlasResourceClient
from pymongoatlas.entities.auditing import AuditingConfig
from pymongoatlas.entities.projects import Project


class AuditingClient(AtlasResourceClient):
    def get(self, project: Project) -> AuditingConfig:
        """
        Returns the auditing configuration for the specified project. The auditing configuration defines the events that
        MongoDB Cloud records in the audit log. To use this resource, the requesting API Key must have the Project Owner
        role. This feature isn't available for M0, M2, M5, or serverless clusters.

        :param project: The project from which to retrieve the auditing config
        :return: AuditingConfig entity

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Auditing/operation/getAuditingConfiguration>_
        """
        response = self.client.get(f"/groups/{project.id}/auditLog")
        return AuditingConfig.from_api(response)

    def update(self, project: Project, auditing_config: AuditingConfig) -> AuditingConfig:
        """
        Updates the auditing configuration for the specified project. The auditing configuration defines the events that
        MongoDB Cloud records in the audit log. To use this resource, the requesting API Key must have the Project
        Owner role. This feature isn't available for M0, M2, M5, or serverless clusters.

        :param project: The project associated with the auditing config
        :param auditing_config: The updated auditing config
        :return: Updated AuditingConfig entity
        """
        response = self.client.patch(f"?groups/{project.id}/auditLog", json=auditing_config.to_api())
        return AuditingConfig.from_api(response)
