import math
from datetime import datetime
from os import PathLike
from pathlib import Path
from typing import Generator, List

from pymongoatlas.clients.common import AcceptHeader, AtlasResourceClient, get_headers
from pymongoatlas.entities.data_federation import (
    FederatedDatabaseInstance,
    FederatedDatabaseInstanceIdentifier,
    OverrunPolicy,
    PrivateNetworkEndpoint,
    PrivateNetworkEndpointIdentifier,
    QueryLimitName,
    QueryLimits,
)
from pymongoatlas.entities.projects import ProjectIdentifier


class DataFederationClient(AtlasResourceClient):
    def list(self, project: ProjectIdentifier) -> List[FederatedDatabaseInstance]:
        """
        Returns the details of all federated database instances in the specified project. To use this resource, the
        requesting API Key must have the Project Read Only or higher role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/listFederatedDatabases>_

        :param project: ``Project`` entity or its string id
        :return: List of ``FederatedDatabaseInstance`` entities.
        """
        response = self.client.get(f"/groups/{project}/dataFederation")
        return [FederatedDatabaseInstance.from_api(x) for x in response]

    def create(
        self, project: ProjectIdentifier, instance: FederatedDatabaseInstance, test_s3_bucket: str | None = None
    ) -> FederatedDatabaseInstance:
        """
        Creates one federated database instance in the specified project. To use this resource, the requesting API Key
        must have the Project Owner or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/createFederatedDatabase>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity to create.
        :param test_s3_bucket: (Optional) name of a bucket to test the AWS Cloud Provider, if configured.
        :return: Created ``FederatedDatabaseInstance`` entity.
        """
        response = self.client.post(f"/groups/{project}/dataFederation", json=instance.to_api(test_s3_bucket))
        return FederatedDatabaseInstance.from_api(response)

    def delete(self, project: ProjectIdentifier, instance: FederatedDatabaseInstanceIdentifier) -> None:
        """
        Removes one federated database instance from the specified project. To use this resource, the requesting API Key
        must have the Project Owner or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/deleteFederatedDatabase>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity or its name to delete.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/dataFederation/{instance}")

    def get(self, project: ProjectIdentifier, instance_name: str) -> FederatedDatabaseInstance:
        """
        Returns the details of one federated database instance within the specified project. To use this resource, the
        requesting API Key must have the Project Read Only or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/getFederatedDatabase>_

        :param project: ``Project`` entity or its string id.
        :param instance_name: Name of the federated instance to fetch.
        :return: ``FederatedDatabaseInstance`` entity.
        """
        response = self.client.get(f"/groups/{project}/dataFederation/{instance_name}")
        return FederatedDatabaseInstance.from_api(response)

    def update(
        self, project: ProjectIdentifier, instance: FederatedDatabaseInstance, test_s3_bucket: str | None = None
    ) -> FederatedDatabaseInstance:
        """
        Updates the details of one federated database instance in the specified project. To use this resource, the
        requesting API Key must have the Project Owner or higher role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/updateFederatedDatabase>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity with updated data.
        :param test_s3_bucket: (Optional) name of a bucket to test the AWS Cloud Provider, if configured.
        :return: Updated ``FederatedDatabaseInstance`` entity.
        """
        response = self.client.patch(f"/groups/{project}/dataFederation/{instance}", json=instance.to_api())
        return FederatedDatabaseInstance.from_api(response)

    def list_query_limits(self, project: ProjectIdentifier, instance: FederatedDatabaseInstanceIdentifier) -> List[QueryLimits]:
        """
        Returns query limits for a federated databases instance in the specified project. To use this resource, the
        requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/returnFederatedDatabaseQueryLimits>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity or its name.
        :return: List of ``QueryLimits`` entities.
        """
        response = self.client.get(f"/groups/{project}/dataFederation/{instance}/limits")
        return [QueryLimits.from_api(x) for x in response]

    def delete_query_limit(self, project: ProjectIdentifier, instance: FederatedDatabaseInstanceIdentifier, limit: QueryLimitName) -> None:
        """
        Deletes one query limit for one federated database instance. To use this resource, the requesting API Key must
        have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/deleteOneDataFederationInstanceQueryLimit>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity or its name.
        :param limit: ``QueryLimitName`` to delete.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/dataFederation/{instance}/limits/{limit.value}")

    def get_query_limit(
        self, project: ProjectIdentifier, instance: FederatedDatabaseInstanceIdentifier, limit: QueryLimitName
    ) -> QueryLimits:
        """
        Returns the details of one query limit for the specified federated database instance in the specified project.
        To use this resource, the requesting API Key must have the Project Read Only role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/returnFederatedDatabaseQueryLimit>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity or its name.
        :param limit: ``QueryLimitName`` to fetch.
        :return: ``QueryLimits`` entity.
        """
        response = self.client.get(f"/groups/{project}/dataFederation/{instance}/limits/{limit.value}")
        return QueryLimits.from_api(response)

    def configure_query_limit(
        self,
        project: ProjectIdentifier,
        instance: FederatedDatabaseInstanceIdentifier,
        limit: QueryLimitName,
        value: int,
        overrun_policy: OverrunPolicy | None = None,
    ) -> QueryLimits:
        """
        Creates or updates one query limit for one federated database instance. To use this resource, the requesting API
        Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/createOneDataFederationQueryLimit>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity or its name.
        :param limit: ``QueryLimitName`` to configure.
        :param value: Amount to set the limit to.
        :param overrun_policy: Optional Overrun Policy to set.
        :return: ``QueryLimits`` entity.
        """
        payload = {"value": value}
        if overrun_policy is not None:
            payload["overrunPolicy"] = overrun_policy.value
        response = self.client.patch(f"/groups/{project}/dataFederation/{instance}/limits/{limit.value}", json=payload)
        return QueryLimits.from_api(response)

    def download_query_logs(
        self,
        project: ProjectIdentifier,
        instance: FederatedDatabaseInstanceIdentifier,
        start_date: datetime,
        end_date: datetime,
        path: PathLike,
    ) -> Path:
        """
        Downloads the query logs for the specified federated database instance. To use this resource, the requesting API
        Key must have the Project Owner or Project Data Access Read Write roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/downloadFederatedDatabaseQueryLogs>_

        :param project: ``Project`` entity or its string id.
        :param instance: ``FederatedDatabaseInstance`` entity or its name.
        :param start_date: Starting point for the range of log messages to download.
        :param end_date: End point for the range of log messages to download.
        :param path: Local path to download the compressed log archive.
        :return: ``Path`` instance where the logs have been downloaded.
        """
        if start_date > end_date:
            raise ValueError("End date must be after the start date.")
        params = {
            "startDate": math.floor(start_date.timestamp()),
            "endDate": math.ceil(end_date.timestamp()),
        }
        path = Path(path)
        with self.client.get_stream(
            f"/groups/{project}/dataFederation/{instance}/queryLogs.gz", headers=get_headers(AcceptHeader.GZIP), params=params
        ) as req:
            req.raise_for_status()
            with path.open("rb") as outfile:
                for chunk in req.iter_content(chunk_size=2048):
                    if chunk:
                        outfile.write(chunk)
        return path

    def list_private_endpoints(self, project: ProjectIdentifier) -> Generator[PrivateNetworkEndpoint, None, None]:
        """
        Returns all private endpoints for Federated Database Instances and Online Archives in the specified project. To
        use this resource, the requesting API Key must have the Project Read Only or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/listDataFederationPrivateEndpoints>_

        :param project: ``Project`` entity or its string id.
        :return: Generator of ``PrivateNetworkEndpointIdEntry`` entities
        """
        for page in self.client.paginate(f"/groups/{project}/privateNetworkSettings/endpointIds"):
            yield from (PrivateNetworkEndpoint.from_api(x) for x in page["results"])

    def create_private_endpoint(self, project: ProjectIdentifier, private_endpoint: PrivateNetworkEndpoint) -> List[PrivateNetworkEndpoint]:
        """
        Adds one private endpoint for Federated Database Instances and Online Archives to the specified projects. If the
        endpoint ID already exists and the associated comment is unchanged, Atlas Data Federation makes no change to the
        endpoint ID list. If the endpoint ID already exists and the associated comment is changed, Atlas Data Federation
        updates the comment value only in the endpoint ID list. If the endpoint ID doesn't exist, Atlas Data Federation
        appends the new endpoint to the list of endpoints in the endpoint ID list. Each region has an associated service
        name for the various endpoints in each region.

        us-east-1 is com.amazonaws.vpce.us-east-1.vpce-svc-00e311695874992b4.

        us-west-1 is com.amazonaws.vpce.us-west-2.vpce-svc-09d86b19e59d1b4bb.

        eu-west-1 is com.amazonaws.vpce.eu-west-1.vpce-svc-0824460b72e1a420e.

        eu-west-2 is com.amazonaws.vpce.eu-west-2.vpce-svc-052f1840aa0c4f1f9.

        eu-central-1 is com.amazonaws.vpce.eu-central-1.vpce-svc-0ac8ce91871138c0d.

        sa-east-1 is com.amazonaws.vpce.sa-east-1.vpce-svc-0b56e75e8cdf50044.

        ap-southeast-2 is com.amazonaws.vpce.ap-southeast-2.vpce-svc-036f1de74d761706e.

        ap-south-1 is com.amazonaws.vpce.ap-south-1.vpce-svc-03eb8a541f96d356d.

        To use this resource, the requesting API Key must have the Project Owner or Project Charts Admin roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/createDataFederationPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param private_endpoint: ``PrivateNetworkEndpoint`` entity to create.
        :return: List of created ``PrivateNetworkEndpoint`` entities.
        """
        response = self.client.post(f"/groups/{project}/privateNetworkSettings/endpointIds", json=private_endpoint.to_api())
        return [PrivateNetworkEndpoint.from_api(x) for x in response["results"]]

    def delete_private_endpoint(self, project: ProjectIdentifier, endpoint: PrivateNetworkEndpointIdentifier) -> None:
        """
        Removes one private endpoint for Federated Database Instances and Online Archives in the specified project. To
        use this resource, the requesting API Key must have the Project Owner role.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/deleteDataFederationPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param endpoint: ``PrivateNetworkEndpoint`` entity or its ``endpoint_id``.
        :return: ``None``
        """
        self.client.delete(f"/groups/{project}/privateNetworkSettings/endpointIds/{endpoint}")

    def get_private_endpoint(self, project: ProjectIdentifier, endpoint_id: str) -> PrivateNetworkEndpoint:
        """
        Returns the specified private endpoint for Federated Database Instances or Online Archives in the specified
        project. To use this resource, the requesting API Key must have the Project Read Only or Project Charts Admin
        roles.

        <https://www.mongodb.com/docs/atlas/reference/api-resources-spec/v2/#tag/Data-Federation/operation/getDataFederationPrivateEndpoint>_

        :param project: ``Project`` entity or its string id.
        :param endpoint_id: Unique 22-character alphanumeric string that identifies the private endpoint to return.
        :return: ``PrivateNetworkEndpoint`` entity.
        """
        response = self.client.get(f"/groups/{project}/privateNetworkSettings/endpointIds/{endpoint_id}")
        return PrivateNetworkEndpoint.from_api(response)
