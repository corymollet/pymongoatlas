PYMONGOATLAS_VENV_DIR ?= ~/.envs/pymongoatlas

all: init install

init:
	uv venv --quiet --seed $(PYMONGOATLAS_VENV_DIR)

activate:
	source $(PYMONGOATLAS_VENV_DIR)/bin/activate

install:
	uv pip install --editable '.[dev,test]'

destroy:
	rm -rf $(PYMONGOATLAS_VENV_DIR)

build:
	python -m build

clean: destroy init

refresh: clean activate install
